package sd.pagos.persistencia;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;

import sd.pagos.Config;

public class ConfigDAO {
	
	private static final String path = System.getProperty("user.dir") + "/data/";
	private String archivo;
	
	public ConfigDAO() {
		this( "config.json" );
	}
	
	public ConfigDAO( String archivo ) {
		this.setArchivo(archivo);
	}
	
	public Config obtenerConfiguracion() {
		ObjectMapper mapper = new ObjectMapper();
		try {
			if (!existeArchivo())
				guardarConfiguracion( new Config( "automatico", 70,2 ) );
			return (Config)mapper.readValue( new File( getArchivo() ), Config.class );
		} catch (IOException e) {
			System.out.println( "[Config] Exception " + e.getMessage() );
			return null;
		}
	}
	

	public int guardarConfiguracion( Config cfg ) {
		if (!existeArchivo() && !crearArchivo())
			return -1;
		ObjectMapper mapper = new ObjectMapper();
		try {
			mapper.writeValue(  new FileOutputStream( getArchivo() ), cfg );
			return 1;
		} catch (IOException e) {
			System.out.println( "[Config] Exception: " + e.getMessage() );
			return -1;
		}
	}
	

	private boolean crearArchivo() {
		File file = new File( getArchivo() );
		try {
			file.createNewFile();
			return true;
		} catch (IOException e) {
			System.out.println("[Config] IOEXception: " + e.getMessage() );
			return false;
		}
	}
	
	private boolean existeArchivo() {
		File file = new File( getArchivo() );
		return file.exists();
	}
	
	
	public static String getPath() {
		return path;
	}

	public String getArchivo() {
		return path+archivo;
	}

	public void setArchivo(String archivo) {
		this.archivo = archivo;
	}
	
	

}
