package sd.infracciones;

public class Infraccion {

	public enum Estado {SIN_INFRACCION, INFRACCION, ESPERA};
	private int numero;
	private String detalle;
	private int compra;
	private Estado estado;
	
	public Infraccion() {}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public String getDetalle() {
		return detalle;
	}

	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}

	public int getCompra() {
		return compra;
	}

	public void setCompra(int compra) {
		this.compra = compra;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}
	
}
