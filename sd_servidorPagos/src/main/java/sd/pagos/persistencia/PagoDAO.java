package sd.pagos.persistencia;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;

import sd.pagos.Pago;

public class PagoDAO {

	private static final String path =  System.getProperty("user.dir") + "/data/";
	private String archivo;
	
	public PagoDAO() {
		this("data.json");
	}
	
	public PagoDAO( String archivo ) {
		this.setArchivo(archivo);
	}
	
	public Pago obtenerPago( int numero ) {
		List<Pago> pagos = obtenerTodosPagos();
		for (Pago pago: pagos) {
			if (pago.getNumero() == numero)
				return pago;
		}
		return null;
	}
	
	public List<Pago> obtenerTodosPagos() {
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			if (!existeArchivo())
				return null;
			Pago[] pgs =  mapper.readValue( new File( path+getArchivo() ), Pago[].class );
			return new ArrayList<Pago>( Arrays.asList( pgs ));
		} catch (IOException e) {
			System.out.println("[*][PAGOS] IOExcepction: " + e.getMessage() );
			return null;
		}			
	}

	
	public int guardarPago( Pago pago ) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			if (!existeArchivo() && !crearArchivo())
				return -1;
			FileOutputStream output = new FileOutputStream( path+getArchivo() );
			JsonGenerator g = mapper.getFactory().createGenerator( output );
			mapper.writeValue( g,pago );
			return 1;
		} catch (IOException e) {
			System.out.println( "[*][PAGOS] IOException: " + e.getMessage() );
			return -1;
		}
	}
	
	public int guardarPagos( List<Pago> pagos ) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			if (!existeArchivo() && !crearArchivo())
				return -1;
			FileOutputStream output = new FileOutputStream( path+getArchivo() );
			JsonGenerator g = mapper.getFactory().createGenerator( output );
			mapper.writeValue( g, pagos );
			return 1;
		} catch (IOException e) {
			System.out.println( "[*][PAGOS] IOException: " + e.getMessage() );
			return -1;
		}
	}
	
	private boolean existeArchivo() {
		File file = new File( path+getArchivo() );
		return file.exists();
	}
	
	private boolean crearArchivo() {
		File file = new File( path+getArchivo() );
		try {
			file.createNewFile();
			return true;
		} catch (IOException e) {
			System.out.println("[*][PAGOS] IOException : " + e.getMessage() );
			return false;
		}
	}
	
	private String getArchivo() {
		return archivo;
	}

	private void setArchivo(String archivo) {
		this.archivo = archivo;
	}
	
}
