package sd.infracciones.negocio;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import sd.infracciones.Infraccion;
import sd.infracciones.Infraccion.Estado;
import sd.infracciones.mensajeria.ManejadorMensajesInfraccion;
import sd.infracciones.persistencia.ConfigDAO;
import sd.infracciones.persistencia.InfraccionDAO;
import sd.infracciones.persistencia.RelojDAO;
import servidor.Mensaje.ID;
import servidor.Mensaje.Operacion;
import servidor.Canal;
import servidor.Mensaje;
import servidor.RelojVectorial;

public class ServidorInfracciones  {

	private List<Infraccion> infracciones;
	private ManejadorMensajesInfraccion manejadorMensajes;
	private RelojVectorial relojes;
	private RelojDAO relojDAO;
	private boolean automatico;
	
	private boolean snapshot;
	 
	Canal canalProductos;
	Canal canalCompras;
	List<Infraccion> infraccionesCorte;
	 
	public void iniciarAutomatico() {
		iniciar();
		manejadorMensajes.recibir();
		automatico = true;
	}
	
	public void iniciarPap() {
		iniciar();
		automatico = false;
	}
	
	private void iniciar() {
		relojDAO = new RelojDAO();
		relojes = new RelojVectorial( relojDAO.obtenerRelojes() );
		infracciones = new InfraccionDAO().obtenerTodasInfracciones();
		if (infracciones == null)
			infracciones = new ArrayList<Infraccion>();
		manejadorMensajes = new ManejadorMensajesInfraccion( this );
		manejadorMensajes.conectar();
		

		// Inicializar los canales
		canalProductos = new Canal( ID.S_PRODUCTOS.toString(), new ArrayList<Mensaje>(), servidor.Canal.Estado.DESABILITADO );
		canalCompras = new Canal( ID.S_COMPRAS.toString(), new ArrayList<Mensaje>(), servidor.Canal.Estado.DESABILITADO );

	}

	
	public void detener() {
		new InfraccionDAO().guardarInfracciones( infracciones );
		if (automatico)
			manejadorMensajes.detener();
	}

	public void recibirSiguienteMensaje() {
		manejadorMensajes.recibirSingle();
	}
	

	public void comenzarCorteConsistente() {
		canalProductos.setEstado( servidor.Canal.Estado.LOGGING );
		canalCompras.setEstado( servidor.Canal.Estado.LOGGING );
		
		almacenarEstadoCorte();
		
		Mensaje mensaje = new Mensaje();
		mensaje.setIdentificador( ID.S_INFRACCIONES );
		mensaje.setOper( Operacion.SNAPSHOT );
		
		relojes.tick( RelojVectorial.INDEX_INFRACCIONES );
		mensaje.setTiempos( relojes.toLongArray() );
		manejadorMensajes.enviar( mensaje, "infracciones.compras" );
		
		relojes.tick( RelojVectorial.INDEX_INFRACCIONES );
		mensaje.setTiempos( relojes.toLongArray() );
		manejadorMensajes.enviar( mensaje, "infracciones.productos" );
		
		setSnapshot( true );
	}

	
	public void procesarMensaje( ID origen, Operacion operacion, String mensaje, long[] tiempos) {
		procesarCorte( origen , operacion, mensaje );
		// Incrementa tiempo reloj propio, y ajusta los tiempos de los otros relojes
		relojes.ajustarTiempos( RelojVectorial.INDEX_INFRACCIONES, tiempos );
		if (operacion == Operacion.DETECTAR_INFRACCION) {
        	JsonNode node;
			try {
				ObjectMapper mapper = new ObjectMapper();
				node = mapper.readTree( mensaje );
				int compra = node.get("compra").asInt();
	        	
	        	boolean inf = detectarInfraccion( compra );
	        	
	        	Mensaje respuesta = new Mensaje();
	        	respuesta.setIdentificador( ID.S_INFRACCIONES );
				respuesta.setOper( Mensaje.Operacion.INFORMAR_INFRACCION );
				
				respuesta.setMensaje( "{\"compra\":" + compra + ",\"infraccion\":" + inf + "}" );
				/*
				ObjectNode node1 = mapper.createObjectNode();
				node1.put("compra", compra );
				node1.put("infraccion", inf );
				respuesta.setMensaje( node1.toString() );
				*/
				relojes.tick( RelojVectorial.INDEX_INFRACCIONES );
				respuesta.setTiempos( relojes.toLongArray() );
				manejadorMensajes.enviar( respuesta, "infracciones.compras");
				
				relojes.tick( RelojVectorial.INDEX_INFRACCIONES );
				respuesta.setTiempos( relojes.toLongArray() );
				manejadorMensajes.enviar( respuesta,"infracciones.productos");
			} catch (IOException e) {
				System.out.println("[*][INFRACCIONES] IOException: " + e.getMessage() );
			}
        }
        else if (operacion == Operacion.SNAPSHOT) {
   		 	if (origen == ID.S_PRODUCTOS) {
   		 		if (canalProductos.getEstado() == servidor.Canal.Estado.DESABILITADO) {
   		 			
   		 			canalProductos.setEstado( servidor.Canal.Estado.COMPLETO );
   		 			canalCompras.setEstado( servidor.Canal.Estado.LOGGING );
   				
   		 			almacenarEstadoCorte();
   		
   		 			Mensaje msj = new Mensaje();
   		 			msj.setIdentificador( ID.S_INFRACCIONES );
   		 			msj.setOper( Operacion.SNAPSHOT );
	   		 			
	   		 		relojes.tick( RelojVectorial.INDEX_INFRACCIONES );
					msj.setTiempos( relojes.toLongArray() );
   		 			manejadorMensajes.enviar( msj, "infracciones.compras" );
	   		 			
	   		 		relojes.tick( RelojVectorial.INDEX_INFRACCIONES );
					msj.setTiempos( relojes.toLongArray() );
			 		manejadorMensajes.enviar( msj, "infracciones.productos" );
   		 		}
   		 		else if (canalProductos.getEstado() == servidor.Canal.Estado.LOGGING) {
	   		 		canalProductos.setEstado( servidor.Canal.Estado.COMPLETO );
   		 			cerrarCorte();
   		 		}
   		 	}
   		 	else if (origen == ID.S_COMPRAS) {
   		 		if (canalCompras.getEstado() == servidor.Canal.Estado.DESABILITADO) {
   		 			
	   		 		canalCompras.setEstado( servidor.Canal.Estado.COMPLETO );
   		 			canalProductos.setEstado( servidor.Canal.Estado.LOGGING );
   		 			
   		 			almacenarEstadoCorte();
   		
   		 			Mensaje msj = new Mensaje();
   		 			msj.setIdentificador( ID.S_INFRACCIONES );
   		 			msj.setOper( Operacion.SNAPSHOT );
	   		 			
	   		 		relojes.tick( RelojVectorial.INDEX_INFRACCIONES );
					msj.setTiempos( relojes.toLongArray() );
   		 			manejadorMensajes.enviar( msj, "infracciones.productos" );
   		 			
	   		 		relojes.tick( RelojVectorial.INDEX_INFRACCIONES );
					msj.setTiempos( relojes.toLongArray() );
			 		manejadorMensajes.enviar( msj, "infracciones.compras" );
	   		 		
   		 		}
   		 		else if (canalCompras.getEstado() == servidor.Canal.Estado.LOGGING) {
   		 			canalCompras.setEstado( servidor.Canal.Estado.COMPLETO );
   		 			cerrarCorte();
   		 		}
		 	}
        }
		// Guarda el reloj actual en archivo.
	 	relojDAO.guardarRelojes( relojes.toLongArray() );

	}

	public List<Infraccion> listaInfracciones() {
		return infracciones;
	}
	
	public boolean detectarInfraccion( int compra ) {
		Infraccion inf = new Infraccion();
		inf.setCompra( compra );
		inf.setNumero( infracciones.size() + 1 );
		inf.setEstado( Estado.ESPERA );
		
		boolean enInfraccion = calcularInfraccion();
		inf.setEstado( enInfraccion ? Estado.INFRACCION : Estado.SIN_INFRACCION );
		infracciones.add( inf );
		return enInfraccion;
	}
	
	public void setearInfraccion( int compra, boolean inf ) {
		for (Infraccion infraccion: infracciones) {
			if (infraccion.getCompra() == compra) {
				infraccion.setEstado( inf ? Estado.INFRACCION: Estado.SIN_INFRACCION );
			}
		}
	}
	
	private boolean calcularInfraccion() {
		double randomValue = Math.random()*100;  //0.0 to 99.9
	    return randomValue <= new ConfigDAO().obtenerConfiguracion().getProbabilidadInfraccion();
	}
	
	public String infraccionesEstadoString() {
		return infraccionesToString( infracciones );
	}
	
	public String infraccionesPersistidasString() {
		InfraccionDAO idao = new InfraccionDAO();
		List<Infraccion> infs = idao.obtenerTodasInfracciones();
		return infraccionesToString(infs);
	}
	
	private String infraccionesToString( List<Infraccion> listaInf ) {
		ObjectMapper mapper = new ObjectMapper();
		ArrayNode arrayNode = mapper.createArrayNode();
		if (listaInf != null)
			for (Infraccion inf: listaInf) {
				ObjectNode node = mapper.createObjectNode();
				node.put("numero", inf.getNumero() );
				node.put("compra", inf.getCompra() );
				node.put("estado", inf.getEstado().toString() );
				arrayNode.add( node );
			}
		return arrayNode.toString();
	}
	
	public void almacenarEstado() {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		InfraccionDAO edao = new InfraccionDAO();
		System.out.println("[*][INFRACCIONES] almacenando estado de envios...  " + infracciones.size() + " registros, " + dateFormat.format( new Date() ) );
		if (infracciones.size() > 0)
			edao.guardarInfracciones(infracciones);
	}
	
	
	private void procesarCorte( ID origen, Operacion operacion, String mensaje ) {
		if (operacion == Operacion.SNAPSHOT)
			return;
		if ((canalCompras.getNombre().compareToIgnoreCase( origen.toString())  == 0) && canalCompras.getEstado() == servidor.Canal.Estado.LOGGING) {
			Mensaje m = new Mensaje();
			m.setIdentificador( origen );
			m.setOper( operacion );
			m.setMensaje(mensaje);
			canalCompras.agregarMensaje( m );
		}
		else if ((canalProductos.getNombre().compareToIgnoreCase( origen.toString()) == 0) && canalProductos.getEstado() == servidor.Canal.Estado.LOGGING) {
			Mensaje m = new Mensaje();
			m.setIdentificador( origen );
			m.setOper( operacion );
			m.setMensaje(mensaje);
			canalProductos.agregarMensaje( m );
		}
		
	}
	
	private void cerrarCorte() {
		if (canalCompras.getEstado() == servidor.Canal.Estado.COMPLETO && canalProductos.getEstado() == servidor.Canal.Estado.COMPLETO ) {
			
			almacenarCanalesCorte();
			
			canalCompras.setEstado( servidor.Canal.Estado.DESABILITADO );
			canalCompras.setMensajes( new ArrayList<Mensaje>() );
			
			canalProductos.setEstado( servidor.Canal.Estado.DESABILITADO );
			canalProductos.setMensajes( new ArrayList<Mensaje>() );
		}
	}

	private void almacenarEstadoCorte() {
		infraccionesCorte = new ArrayList<Infraccion>( infracciones );
	}
	
	private void almacenarCanalesCorte() {
		ObjectMapper mapper = new ObjectMapper();
	
		// Nodo raiz
		ObjectNode nodeCorte = mapper.createObjectNode();
		
		// Nodo estado; array de infracciones
		ArrayNode nodeInfracciones = mapper.createArrayNode();
		for (Infraccion inf: infraccionesCorte) {
			ObjectNode nodeInf = mapper.createObjectNode();
			nodeInf.put("numero", inf.getNumero() );
			nodeInf.put("compra", inf.getCompra() );
			nodeInf.put("detalle", inf.getDetalle() );
			nodeInf.put("estado", inf.getEstado().toString() );
			nodeInfracciones.add( nodeInf );
		}
		nodeCorte.put("estado", nodeInfracciones.toString() );
		
		// Nodo canales: array de mensajes
		ArrayNode nodeCanales = mapper.createArrayNode();
		for (Mensaje m: canalCompras.getMensajes()) {
			ObjectNode nodeCanal = mapper.createObjectNode();
			nodeCanal.put("operacion", m.getOper().toString() );
			nodeCanal.put("mensaje", m.getMensaje().toString() );
			nodeCanales.add( nodeCanal );
		}
		for (Mensaje m: canalProductos.getMensajes()) {
			ObjectNode nodeCanal = mapper.createObjectNode();
			nodeCanal.put("operacion", m.getOper().toString() );
			nodeCanal.put("mensaje", m.getMensaje().toString() );
			nodeCanales.add( nodeCanal );
		}
		nodeCorte.put( "canales", nodeCanales.toString() );
		
		try {
			String archivo = System.getProperty("user.dir") + "/data/" + "corte-" + relojes.toString() + ".json";
			File file = new File( archivo );
			file.createNewFile();
			FileOutputStream output = new FileOutputStream( archivo );
			JsonGenerator g = mapper.getFactory().createGenerator( output );
			mapper.writeValue( g, nodeCorte );
		} catch (IOException e) {
			System.out.println( "[*][COMPRAS] IOException guardando Corte: " + e.getMessage() );
		}
		
	}
	
	public boolean isSnapshot() {
		return snapshot;
	}

	public void setSnapshot(boolean snapshot) {
		this.snapshot = snapshot;
	}


}
