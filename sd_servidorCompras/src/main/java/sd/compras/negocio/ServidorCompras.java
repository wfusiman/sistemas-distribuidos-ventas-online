package sd.compras.negocio;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import sd.compras.Compra;
import sd.compras.mensajeria.ManejadorMensajesCompras;
import sd.compras.persistencia.ComprasDAO;
import sd.compras.persistencia.RelojDAO;
import servidor.Canal;
import servidor.Canal.Estado;
import servidor.Mensaje;
import servidor.Mensaje.ID;
import servidor.Mensaje.Operacion;
import servidor.RelojVectorial;

public class ServidorCompras {
	
	private List<Compra> compras;	// Estado del servidor Compras
	private RelojVectorial tiempos; // relojes vectoriales del servidor Compras
	private ManejadorMensajesCompras manejadorMensajes; 
	private RelojDAO relojPersist;  
	 
	private boolean snapshot;
	 
	Canal canalEnvios;
	Canal canalProductos;
	Canal canalInfracciones;
	Canal canalPagos;
	List<Compra> comprasCorte;
	 
	public ServidorCompras() {}

	/**
	 * Inicializa el servidor de Compras en modo automatico
	 */
	public void iniciarAutomatico() {
		iniciar();
		manejadorMensajes.recibir();
	}
	
	/**
	 * Inicializa el servidor de Compras en modo Paso a Paso
	 */
	public void iniciarPap() {
		iniciar();
	}
	
	/**
	 * Inicializacion comun en cualquier modo del servidor
	 */
	private void iniciar() {
		setSnapshot( false );
		relojPersist = new RelojDAO();
		tiempos = new RelojVectorial( relojPersist.obtenerRelojes() );
		
		// Obtiene las lista de compras
		compras = new ComprasDAO().obtenerTodasCompras();
		if (compras == null)
			compras = new ArrayList<Compra>();
		manejadorMensajes = new ManejadorMensajesCompras( this );
		manejadorMensajes.conectar();
		
		// Inicializar los canales
		canalEnvios = new Canal( ID.S_ENVIOS.toString(), new ArrayList<Mensaje>(), Estado.DESABILITADO );
		canalProductos = new Canal( ID.S_PRODUCTOS.toString(), new ArrayList<Mensaje>(), Estado.DESABILITADO );
		canalInfracciones = new Canal( ID.S_INFRACCIONES.toString(), new ArrayList<Mensaje>(), Estado.DESABILITADO );
		canalPagos = new Canal( ID.S_PAGOS.toString(), new ArrayList<Mensaje>(), Estado.DESABILITADO );
	}

	/**
	 * Detiene el servidor de Compras
	 */
	public void detener() {
		new ComprasDAO().guardarCompras( compras );
		manejadorMensajes.detener();
	}
	
	/**
	 * Realiza la lectura del sisguiente Mensaje (modo Paso a Paso)
	 */
	public void recibirSiguienteMensaje() {
		manejadorMensajes.recibirSingle();
	}
	
	/**
	 * Comienza un corte consistente.
	 */
	public void comenzarCorteConsistente() {
		canalEnvios.setEstado( Estado.LOGGING );
		canalProductos.setEstado( Estado.LOGGING );
		canalInfracciones.setEstado( Estado.LOGGING );
		canalPagos.setEstado( Estado.LOGGING );
		
		almacenarEstadoCorte();
		
		Mensaje mensaje = new Mensaje();
		mensaje.setIdentificador( ID.S_COMPRAS );
		mensaje.setOper( Operacion.SNAPSHOT );
		
		tiempos.tick( RelojVectorial.INDEX_COMPRAS ); // incrementa reloj
    	mensaje.setTiempos( tiempos.toLongArray() );
		manejadorMensajes.enviar( mensaje, "compras.envios" );
		
		tiempos.tick( RelojVectorial.INDEX_COMPRAS ); // incrementa reloj
    	mensaje.setTiempos( tiempos.toLongArray() );
		manejadorMensajes.enviar( mensaje, "compras.productos" );
		
		tiempos.tick( RelojVectorial.INDEX_COMPRAS ); // incrementa reloj
    	mensaje.setTiempos( tiempos.toLongArray() );
		manejadorMensajes.enviar( mensaje, "compras.infracciones" );
		
		tiempos.tick( RelojVectorial.INDEX_COMPRAS ); // incrementa reloj
    	mensaje.setTiempos( tiempos.toLongArray() );
		manejadorMensajes.enviar( mensaje, "compras.pagos" );
		
		setSnapshot( true );
	}

	/**
	 * Realiza el procesamiento de un mensaje
	 * @param origen
	 * @param operacion
	 * @param mensaje
	 * @param relojes
	 */
	public void procesarMensaje( ID origen, Operacion operacion, String mensaje, long[] relojes )  {
		procesarCorte( origen , operacion, mensaje );
		// Incrementa tiempo reloj propio, y ajusta los tiempos de los otros relojes
	 	tiempos.ajustarTiempos( RelojVectorial.INDEX_COMPRAS, relojes );
		if (operacion == Operacion.NUEVA_COMPRA) {
	            ObjectMapper mapper = new ObjectMapper();
	            try {
	                JsonNode node = mapper.readTree( mensaje );
	                Compra compra = new Compra();
	                compra.setNumero( node.get("numero").asInt() );
	                compra.setComprador( node.get("comprador").asText() );
	                compra.setProducto( node.get("producto").asInt() );
	                compra.setEnvio( node.get("envio").asBoolean() );
	                compra.setConfirmada( node.get("confirma").asBoolean() );
	                compra.setFecha( new Date() );
	            	agregarCompra(compra);

	            	/*
	            	ObjectNode node1 = mapper.createObjectNode();
	            	node1.put("compra", compra.getNumero() );
	            	node1.put("producto", compra.getProducto() );
	                */
	            	String compraJson = "{\"compra\":" + compra.getNumero() + ",\"producto\":" + compra.getProducto() + "}";
	            	
	            	Mensaje msj1 = new Mensaje();
	            	msj1.setIdentificador( ID.S_COMPRAS );
	            	msj1.setOper( Operacion.RESERVAR_PRODUCTO );
	            	msj1.setMensaje( compraJson );
	            	//msj1.setMensaje( node1.toString() );
	            	tiempos.tick( RelojVectorial.INDEX_COMPRAS ); // incrementa reloj
	            	msj1.setTiempos( tiempos.toLongArray() );
	            	manejadorMensajes.enviar( msj1, "compras.productos" );
	            	
	            	Mensaje msj2 = new Mensaje();
	            	msj2.setIdentificador( ID.S_COMPRAS );
	            	msj2.setOper( Operacion.DETECTAR_INFRACCION );
	            	msj2.setMensaje( compraJson );
	            	//msj2.setMensaje( node1.toString() );
	            	tiempos.tick( RelojVectorial.INDEX_COMPRAS ); // incrementa reloj
	            	msj2.setTiempos( tiempos.toLongArray() );
	            	manejadorMensajes.enviar( msj2, "compras.infracciones" );
	            	
	            	if (compra.isEnvio()) { // Envio Correo
	            	 	Mensaje msj3 = new Mensaje();
	            	 	msj3.setIdentificador( ID.S_COMPRAS );
	            	 	msj3.setOper( Operacion.CALCULAR_ENVIO);
	            	 	msj3.setMensaje( compraJson );
	            	 	//msj3.setMensaje( node1.toString() );
	            	 	tiempos.tick( RelojVectorial.INDEX_COMPRAS ); // incrementa reloj
	            	 	msj3.setTiempos( tiempos.toLongArray() );
	            	 	manejadorMensajes.enviar( msj3, "compras.envios" );
	            	}
	            } catch (IOException e) {
	                System.out.println("[*][COMPRAS] Error leuendo mensaje ");
	            }  
	        }
	        else if (operacion == Operacion.INFORMAR_INFRACCION) {
	            JsonNode node;
	            try {
	            	ObjectMapper mapper = new ObjectMapper();
	                node = mapper.readTree(mensaje);
	                int compra = node.get("compra").asInt();
	                boolean inf = node.get("infraccion").asBoolean();
	                setearInfraccion(compra, inf);
	                
	                if (!inf) { // si no hay infraccion
		                 Mensaje msj = new Mensaje();
		                 msj.setIdentificador( ID.S_COMPRAS );
		                 msj.setOper( Operacion.AUTORIZAR_PAGO );
		                 /*
		                 ObjectNode node1 = mapper.createObjectNode();
		                 node1.put("compra", compra );
		                 msj.setMensaje( node1.toString() );
		                 */
		                 msj.setMensaje( "{\"compra\":" + compra + "}" );
		                 
		                 tiempos.tick( RelojVectorial.INDEX_COMPRAS ); // incrementa reloj
		                 msj.setTiempos( tiempos.toLongArray() );
		                 manejadorMensajes.enviar( msj, "compras.pagos" );
		            }
	            } catch (IOException e) {
	                System.out.println("[*][COMPRAS] Error leuendo mensaje ");
	            }
	        }
	        else if (operacion == Operacion.CALCULAR_ENVIO) {	
	   		 	JsonNode node;
	            try {
	                node = new ObjectMapper().readTree(mensaje);
	                int compra = node.get("compra").asInt();
	                float costo = node.get("costo").floatValue();
	                setearCostoEnvio(compra, costo);
	            } catch (IOException e) {
	                System.out.println("[*][COMPRAS] Error leuendo mensaje ");
	            }
	        }
	        else if (operacion == Operacion.AUTORIZAR_PAGO) {
	        	JsonNode node;
	            try {
	            	ObjectMapper mapper = new ObjectMapper();
	                node = mapper.readTree(mensaje);
	                int compra = node.get("compra").asInt();
	                boolean aut = node.get("autorizado").asBoolean();
	                setearAutorizacion(compra, aut );
	                
		            if (aut) {
		                 confirmarCompra( compra );
		                 Mensaje msj = new Mensaje();
		                 msj.setIdentificador( ID.S_COMPRAS );
		                 msj.setOper(Operacion.AGENDAR_ENVIO );
		                 msj.setMensaje( "{\"compra\":" + compra + "}" );
		                 /*
		                 ObjectNode node1 = mapper.createObjectNode();
		                 node1.put("compra", compra );
		                 msj.setMensaje( node1.toString() );
		                 */
		                 tiempos.tick( RelojVectorial.INDEX_COMPRAS ); // incrementa reloj
		                 msj.setTiempos( tiempos.toLongArray() );
		                 manejadorMensajes.enviar( msj, "compras.envios" );
	            	}
	            } catch (IOException e) {
	                System.out.println("[*][COMPRAS] Error leyendo mensaje ");
	            }   
	        }
	        else if (operacion == Operacion.SNAPSHOT) {
	   		 	if (origen == ID.S_ENVIOS) {
	   		 		if (canalEnvios.getEstado() == Estado.DESABILITADO) {
	   		 			
	   		 			canalEnvios.setEstado( Estado.COMPLETO );
	   		 			canalProductos.setEstado( Estado.LOGGING );
	   		 			canalInfracciones.setEstado( Estado.LOGGING );
	   		 			canalPagos.setEstado( Estado.LOGGING );
	   				
	   		 			almacenarEstadoCorte();
	   		
	   		 			Mensaje msj = new Mensaje();
	   		 			msj.setIdentificador( ID.S_COMPRAS );
	   		 			msj.setOper( Operacion.SNAPSHOT );
	   		 			
		   		 		tiempos.tick( RelojVectorial.INDEX_COMPRAS ); // incrementa reloj
	   		 			msj.setTiempos( tiempos.toLongArray() );
	   		 			manejadorMensajes.enviar( msj, "compras.envios" );
	   		 			
	   		 			tiempos.tick( RelojVectorial.INDEX_COMPRAS ); // incrementa reloj
	   		 			msj.setTiempos( tiempos.toLongArray() );
	   		 			manejadorMensajes.enviar( msj, "compras.productos" );
	   		 			
	   		 			tiempos.tick( RelojVectorial.INDEX_COMPRAS ); // incrementa reloj
	   		 			msj.setTiempos( tiempos.toLongArray() );
	   		 			manejadorMensajes.enviar( msj, "compras.infracciones" );
	   		 			
	   		 			tiempos.tick( RelojVectorial.INDEX_COMPRAS ); // incrementa reloj
	   		 			msj.setTiempos( tiempos.toLongArray() );
	   		 			manejadorMensajes.enviar( msj, "compras.pagos" );
	   		 		}
	   		 		else if (canalEnvios.getEstado() == Estado.LOGGING) {
		   		 		canalEnvios.setEstado( Estado.COMPLETO );
	   		 			cerrarCorte();
	   		 		}
	   		 	}
	   		 	else if (origen == ID.S_PRODUCTOS) {
	   		 		if (canalProductos.getEstado() == Estado.DESABILITADO) {
	   		 			
	   		 			canalProductos.setEstado( Estado.COMPLETO );
	   		 			canalEnvios.setEstado( Estado.LOGGING );
	   		 			canalInfracciones.setEstado( Estado.LOGGING );
	   		 			canalPagos.setEstado( Estado.LOGGING );
	   				
	   		 			almacenarEstadoCorte();
	   		
	   		 			Mensaje msj = new Mensaje();
	   		 			msj.setIdentificador( ID.S_COMPRAS );
	   		 			msj.setOper( Operacion.SNAPSHOT );
			   		 		
		   		 		tiempos.tick( RelojVectorial.INDEX_COMPRAS ); // incrementa reloj
	   		 			msj.setTiempos( tiempos.toLongArray() );
	   		 			manejadorMensajes.enviar( msj, "compras.productos" );
	   		 			
	   		 			tiempos.tick( RelojVectorial.INDEX_COMPRAS ); // incrementa reloj
	   		 			msj.setTiempos( tiempos.toLongArray() );
	   		 			manejadorMensajes.enviar( msj, "compras.envios" );
	   		 			
		   		 		tiempos.tick( RelojVectorial.INDEX_COMPRAS ); // incrementa reloj
	   		 			msj.setTiempos( tiempos.toLongArray() );
	   		 			manejadorMensajes.enviar( msj, "compras.infracciones" );
	   					
		   		 		tiempos.tick( RelojVectorial.INDEX_COMPRAS ); // incrementa reloj
	   		 			msj.setTiempos( tiempos.toLongArray() );
	   		 			manejadorMensajes.enviar( msj, "compras.pagos" );
	   		 		}
	   		 		else if (canalProductos.getEstado() == Estado.LOGGING) {
		   		 		canalProductos.setEstado( Estado.COMPLETO );
	   		 			cerrarCorte();
	   		 		}
	   		 	}
	   		 	else if (origen == ID.S_INFRACCIONES) {
	   		 		if (canalInfracciones.getEstado() == Estado.DESABILITADO) {
		   		 		
	   		 			canalInfracciones.setEstado( Estado.COMPLETO );
	   		 			canalEnvios.setEstado( Estado.LOGGING );
	   		 			canalProductos.setEstado( Estado.LOGGING );
	   		 			canalPagos.setEstado( Estado.LOGGING );
	   				
	   		 			almacenarEstadoCorte();
	   		
	   		 			Mensaje msj = new Mensaje();
	   		 			msj.setIdentificador( ID.S_COMPRAS );
	   		 			msj.setOper( Operacion.SNAPSHOT );
	   		 			
		   		 		tiempos.tick( RelojVectorial.INDEX_COMPRAS ); // incrementa reloj
	   		 			msj.setTiempos( tiempos.toLongArray() );
	   		 			manejadorMensajes.enviar( msj, "compras.infracciones" );
   		 			
		   		 		tiempos.tick( RelojVectorial.INDEX_COMPRAS ); // incrementa reloj
	   		 			msj.setTiempos( tiempos.toLongArray() );
	   		 			manejadorMensajes.enviar( msj, "compras.envios" );
	   		 			
		   		 		tiempos.tick( RelojVectorial.INDEX_COMPRAS ); // incrementa reloj
	   		 			msj.setTiempos( tiempos.toLongArray() );
	   		 			manejadorMensajes.enviar( msj, "compras.productos" );
		   		 		
	   		 			tiempos.tick( RelojVectorial.INDEX_COMPRAS ); // incrementa reloj
	   		 			msj.setTiempos( tiempos.toLongArray() );
	   		 			manejadorMensajes.enviar( msj, "compras.pagos" );
	   		 		}
	   		 		else if (canalInfracciones.getEstado() == Estado.LOGGING) {
		   		 		canalInfracciones.setEstado( Estado.COMPLETO );
	   		 			cerrarCorte();
	   		 		}
	   		 	}
	   		 	else if (origen == ID.S_PAGOS) {
	   		 		if (canalPagos.getEstado() == Estado.DESABILITADO) {
	   		 			
		   		 		canalPagos.setEstado( Estado.COMPLETO );
	   		 			canalEnvios.setEstado( Estado.LOGGING );
	   		 			canalProductos.setEstado( Estado.LOGGING );
	   		 			canalInfracciones.setEstado( Estado.LOGGING );
	   				
	   		 			almacenarEstadoCorte();
	   		
	   		 			Mensaje msj = new Mensaje();
	   		 			msj.setIdentificador( ID.S_COMPRAS );
	   		 			msj.setOper( Operacion.SNAPSHOT );
		   		 			
		   		 		tiempos.tick( RelojVectorial.INDEX_COMPRAS ); // incrementa reloj
	   		 			msj.setTiempos( tiempos.toLongArray() );
	   		 			manejadorMensajes.enviar( msj, "compras.pagos" );
	   		 			
		   		 		tiempos.tick( RelojVectorial.INDEX_COMPRAS ); // incrementa reloj
	   		 			msj.setTiempos( tiempos.toLongArray() );
		   		 		manejadorMensajes.enviar( msj, "compras.envios" );
		   		 			
		   		 		tiempos.tick( RelojVectorial.INDEX_COMPRAS ); // incrementa reloj
	   		 			msj.setTiempos( tiempos.toLongArray() );
	   		 			manejadorMensajes.enviar( msj, "compras.productos" );
		   		 			
		   		 		tiempos.tick( RelojVectorial.INDEX_COMPRAS ); // incrementa reloj
	   		 			msj.setTiempos( tiempos.toLongArray() );
	   					manejadorMensajes.enviar( msj, "compras.infracciones" );
	   		 		}
	   		 		else if (canalPagos.getEstado() == Estado.LOGGING) {
	   		 			canalPagos.setEstado( Estado.COMPLETO );
	   		 			cerrarCorte();
	   		 		}
   		 		}	
	        }
		 	// Guarda el reloj actual en archivo.
		 	relojPersist.guardarRelojes( tiempos.toLongArray() );
	    }

	 /**
	  * Agrega una nueva compra a la lista de compras en memoria
	  * @param compra
	  */
	    public void agregarCompra( Compra compra ) {
			if (compra != null) {
				compra.setEstado( "ESPERA ENVIO CONFIRMACION INFRACCION PAGO");
				compras.add( compra );
			}
				
		}
		
	    /**
	     * Asigna al estado de la compra el valor de si tiene infraccion o no
	     * @param compra
	     * @param inf
	     */
		public void setearInfraccion( int compra, boolean inf ) {
			for (Compra c: compras) {
				if (c.getNumero() == compra) {
					if (inf) 
						c.setEstado("EN INFRACCION");
					else 
						c.setEstado( c.getEstado().replaceAll("INFRACCION", "") );
					return;
				}
			}
		}
		
		
		/**
		 * Asigna al estado de la compra el valor de si fue confirmada o no.
		 * @param compra
		 * @param conf
		 */
		public void setearConfirmacion( int compra, boolean conf ) {
			for (Compra c: compras) {
				if (c.getNumero() == compra) {
					if (!conf) 
						c.setEstado("RECHAZADA");
					else 
						c.setEstado( c.getEstado().replaceAll("CONFIRMACION", "") );
					return;
				}
			}
		}
		
		/**
		 * Asigna a una compra el costo de envio
		 * @param compra
		 * @param costo
		 */
		public void setearCostoEnvio( int compra, float costo ) {
			for (Compra c: compras) {
				if (c.getNumero() == compra) {
					c.setCostoEnvio( costo );
					c.setEstado( c.getEstado().replaceAll("ENVIO", "" ));
					return;
				}
			}
		}
		
		/**
		 * Asigna a una compra el estado de si el pago fue autorizado o no.
		 * @param compra
		 * @param autorizado
		 */
		public void setearAutorizacion( int compra, boolean autorizado ) {
			for (Compra c: compras) {
				if (c.getNumero() == compra) {
					if (autorizado)
						c.setEstado("PAGO AUTORIZADO");
					else
						c.setEstado( "PAGO NO AUTORIZADO");
					return;
				}
			}
		}
		
		/**
		 * Asigna a una compra el estado de si fue confirmada o no por el cliente.
		 * @param compra
		 */
		public void confirmarCompra( int compra ) {
			for (Compra c: compras) {
				if (c.getNumero() == compra) {
					if (c.isEnvio())
						c.setEstado("CONFIRMADA ENVIAR");
					else
						c.setEstado("CONFIRMADA RETIRA");
					return;
				}
			}
		}
		
		/**
		 * Retorna la lista de compras en memoria en formato cadena Json
		 */
		public String comprasEstadoString() {
			return comprasToString( compras ); 
		}

		/**
		 * Retorna la lista de compras persistida en formato cadena Json
		 */
	
		public String comprasPerisitidasString() {
			List<Compra> result = new ComprasDAO().obtenerTodasCompras();
			return comprasToString( result );
		}
		
		/**
		 * Tranforma una lista de compras en una cadena con formato json.
		 * @param listCompras
		 * @return
		 */
		private String comprasToString( List<Compra> listCompras ) {
			ObjectMapper mapper = new ObjectMapper();
			ArrayNode arrayNode = mapper.createArrayNode();
			if (listCompras != null)
				for (Compra comp: listCompras) {
					ObjectNode node = mapper.createObjectNode();
					node.put("numero", comp.getNumero());
					node.put("producto", comp.getProducto() );
					node.put("comprador", comp.getComprador() );
					node.put("estado", comp.getEstado());
					node.put("envio",  comp.isEnvio() );
					node.put("confirmada", comp.isConfirmada() );
					arrayNode.add( node );
				}
			return arrayNode.toString();
		}

		public boolean isSnapshot() {
			return snapshot;
		}

		public void setSnapshot(boolean isSnapshot) {
			this.snapshot = isSnapshot;
		}
		
		/**
		 * Determina si el servidor esta en proceso de un corte consistente y realiza las operaciones necesarias
		 * @param origen
		 * @param operacion
		 * @param mensaje
		 */
		private void procesarCorte( ID origen, Operacion operacion, String mensaje ) {
			if (operacion == Operacion.SNAPSHOT)
				return;
			if ((canalEnvios.getNombre().compareToIgnoreCase( origen.toString())  == 0) && canalEnvios.getEstado() == Estado.LOGGING) {
				Mensaje m = new Mensaje();
				m.setIdentificador( origen );
				m.setOper( operacion );
				m.setMensaje(mensaje);
				canalEnvios.agregarMensaje( m );
			}
			else if ((canalProductos.getNombre().compareToIgnoreCase( origen.toString()) == 0) && canalProductos.getEstado() == Estado.LOGGING) {
				Mensaje m = new Mensaje();
				m.setIdentificador( origen );
				m.setOper( operacion );
				m.setMensaje(mensaje);
				canalProductos.agregarMensaje( m );
			}
			else if ((canalInfracciones.getNombre().compareToIgnoreCase( origen.toString()) == 0) && canalInfracciones.getEstado() == Estado.LOGGING) {
				Mensaje m = new Mensaje();
				m.setIdentificador( origen );
				m.setOper( operacion );
				m.setMensaje(mensaje);
				canalInfracciones.agregarMensaje( m );
			}
			else if ((canalPagos.getNombre().compareToIgnoreCase( origen.toString()) == 0) && canalPagos.getEstado() == Estado.LOGGING) {
				Mensaje m = new Mensaje();
				m.setIdentificador( origen );
				m.setOper( operacion );
				m.setMensaje(mensaje);
				canalPagos.agregarMensaje( m );
			}
			
		}
		
		/**
		 * Determina si el una operacion de corte consistente ha sido completada
		 */
		private void cerrarCorte() {
			if (canalEnvios.getEstado() == Estado.COMPLETO && canalProductos.getEstado() == Estado.COMPLETO && canalInfracciones.getEstado() == Estado.COMPLETO && canalPagos.getEstado() == Estado.COMPLETO) {
				
				almacenarCanalesCorte();
				canalEnvios.setEstado( Estado.DESABILITADO );
				canalEnvios.setMensajes( new ArrayList<Mensaje>() );
				
				canalProductos.setEstado( Estado.DESABILITADO );
				canalProductos.setMensajes( new ArrayList<Mensaje>() );
				
				canalInfracciones.setEstado( Estado.DESABILITADO );
				canalInfracciones.setMensajes( new ArrayList<Mensaje>() );
				
				canalPagos.setEstado( Estado.DESABILITADO );
				canalPagos.setMensajes( new ArrayList<Mensaje>() );
			}
		}
		
		/**
		 * Reliza el almacenamiento persistente del estado del servidor Compras
		 */
		public synchronized void almacenarEstado() {
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			ComprasDAO cdao = new ComprasDAO();
			System.out.println("[*][COMPRAS] Almacenando estado...  " + compras.size() + " registro: " + dateFormat.format( new Date() ) );
			if (compras.size() > 0)
				cdao.guardarCompras( compras );
		}
		
		private void almacenarEstadoCorte() {
			comprasCorte = new ArrayList<Compra>( compras );
		}
		
		private void almacenarCanalesCorte() {
			ObjectMapper mapper = new ObjectMapper();
		
			// Nodo raiz
			ObjectNode nodeCorte = mapper.createObjectNode();
			
			// Nodo estado; array de compras
			ArrayNode nodeCompras = mapper.createArrayNode();
			for (Compra c: comprasCorte) {
				ObjectNode nodeCompra = mapper.createObjectNode();
				nodeCompra.put("numero", c.getNumero() );
				nodeCompra.put("producto", c.getProducto() );
				nodeCompra.put("comprador", c.getComprador() );
				nodeCompra.put("estado", c.getEstado() );
				nodeCompra.put("costoEnvio", c.getCostoEnvio() );
				nodeCompra.put("fecha",c.getFecha().toString() );
				nodeCompras.add( nodeCompra );
			}
			nodeCorte.put("estado", nodeCompras.toString() );
			
			// Nodo canales: array de mensajes
			ArrayNode nodeCanales = mapper.createArrayNode();
			for (Mensaje m: canalEnvios.getMensajes()) {
				ObjectNode nodeCanal = mapper.createObjectNode();
				nodeCanal.put("operacion", m.getOper().toString() );
				nodeCanal.put("mensaje", m.getMensaje().toString() );
				nodeCanales.add( nodeCanal );
			}
			for (Mensaje m: canalProductos.getMensajes()) {
				ObjectNode nodeCanal = mapper.createObjectNode();
				nodeCanal.put("operacion", m.getOper().toString() );
				nodeCanal.put("mensaje", m.getMensaje().toString() );
				nodeCanales.add( nodeCanal );
			}
			for (Mensaje m: canalInfracciones.getMensajes()) {
				ObjectNode nodeCanal = mapper.createObjectNode();
				nodeCanal.put("operacion", m.getOper().toString() );
				nodeCanal.put("mensaje", m.getMensaje().toString() );
				nodeCanales.add( nodeCanal );
			}
			for (Mensaje m: canalPagos.getMensajes()) {
				ObjectNode nodeCanal = mapper.createObjectNode();
				nodeCanal.put("operacion", m.getOper().toString() );
				nodeCanal.put("mensaje", m.getMensaje().toString() );
				nodeCanales.add( nodeCanal );
			}
			
			nodeCorte.put( "canales", nodeCanales.toString() );
			
			try {
				String archivo = System.getProperty("user.dir") + "/data/" + "corte" + tiempos.toString() + ".json";
				File file = new File( archivo );
				file.createNewFile();
				FileOutputStream output = new FileOutputStream( archivo );
				JsonGenerator g = mapper.getFactory().createGenerator( output );
				mapper.writeValue( g, nodeCorte );
			} catch (IOException e) {
				System.out.println( "[*][COMPRAS] IOException guardando Corte: " + e.getMessage() );
			}
			
		}

}
