package sd.productos;

public class CompraProducto {
	
	public enum Estado {RESERVAR, RESERVADO, SIN_STOCK, LIBERADO, SIN_INFRACCION, INFRACCION, PAGO_OK, PAGO_RECHAZADO, ENVIADO};
	
	private int compra;
	private int producto;
	private Estado estado;
	
	public CompraProducto() {}
	
	public CompraProducto( int compra, int producto ) {
		this.setCompra(compra);
		this.setProducto(producto);
	}

	public int getCompra() {
		return compra;
	}

	public void setCompra(int compra) {
		this.compra = compra;
	}

	public int getProducto() {
		return producto;
	}

	public void setProducto(int producto) {
		this.producto = producto;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}
	
}
