package sd.productos.negocio;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import sd.productos.CompraProducto;
import sd.productos.Producto;
import sd.productos.CompraProducto.Estado;
import sd.productos.mensajeria.ManejadorMensajesProductos;
import sd.productos.persistencia.ProductoDAO;
import sd.productos.persistencia.RelojDAO;
import servidor.Mensaje.ID;
import servidor.Mensaje.Operacion;
import servidor.Canal;
import servidor.Mensaje;
import servidor.RelojVectorial;

public class ServidorProductos   {

	private List<Producto> productos;
	private List<CompraProducto> comprasProductos;
	private ManejadorMensajesProductos manejadorMensajes;
	private RelojVectorial relojes;
	private RelojDAO relojDAO;
	private boolean automatico;
	
	private boolean snapshot;
	 
	Canal canalEnvios;
	Canal canalCompras;
	Canal canalInfracciones;
	Canal canalPagos;
	List<CompraProducto> comprasProductosCorte;
	
	public void iniciarAutomatico() {
		iniciar();
		manejadorMensajes.recibir();
		automatico = true;
	}
	
	public void iniciarPap() {
		iniciar();
		automatico = false;
	}
	
	public void iniciar() {
		relojDAO = new RelojDAO();
		relojes = new RelojVectorial( relojDAO.obtenerRelojes() );
		productos = new ProductoDAO().obtenerTodosProductos();
		comprasProductos = new ProductoDAO().obtenerComprasProductos();
		if (comprasProductos == null)
			comprasProductos = new ArrayList<CompraProducto>();
		manejadorMensajes = new ManejadorMensajesProductos( this );
		manejadorMensajes.conectar();
		
		// Inicializar los canales
		canalEnvios = new Canal( ID.S_ENVIOS.toString(), new ArrayList<Mensaje>(), servidor.Canal.Estado.DESABILITADO );
		canalCompras = new Canal( ID.S_COMPRAS.toString(), new ArrayList<Mensaje>(), servidor.Canal.Estado.DESABILITADO );
		canalInfracciones = new Canal( ID.S_INFRACCIONES.toString(), new ArrayList<Mensaje>(), servidor.Canal.Estado.DESABILITADO );
		canalPagos = new Canal( ID.S_PAGOS.toString(), new ArrayList<Mensaje>(), servidor.Canal.Estado.DESABILITADO );
	}

	
	public void detener() {
		ProductoDAO pdao = new ProductoDAO();
		pdao.guardarComprasProductos(comprasProductos);
		pdao.guardarProductos(productos);
		if (automatico)
			manejadorMensajes.detener();
	}

	public void recibirSiguienteMensaje() {
		manejadorMensajes.recibirSingle();
	}
	
	public void comenzarCorteConsistente() {
		
		canalEnvios.setEstado( servidor.Canal.Estado.LOGGING );
		canalCompras.setEstado( servidor.Canal.Estado.LOGGING );
		canalInfracciones.setEstado( servidor.Canal.Estado.LOGGING );
		canalPagos.setEstado( servidor.Canal.Estado.LOGGING );
		
		almacenarEstadoCorte();
		
		Mensaje mensaje = new Mensaje();
		mensaje.setIdentificador( ID.S_PRODUCTOS );
		mensaje.setOper( Operacion.SNAPSHOT );
		
		relojes.tick( RelojVectorial.INDEX_PRODUCTOS );
		mensaje.setTiempos( relojes.toLongArray() );
		manejadorMensajes.enviar( mensaje, "productos.compras" );
		
		relojes.tick( RelojVectorial.INDEX_PRODUCTOS );
		mensaje.setTiempos( relojes.toLongArray() );
		manejadorMensajes.enviar( mensaje, "productos.envios" );
		
		relojes.tick( RelojVectorial.INDEX_PRODUCTOS );
		mensaje.setTiempos( relojes.toLongArray() );
		manejadorMensajes.enviar( mensaje, "productos.infracciones" );
		
		relojes.tick( RelojVectorial.INDEX_PRODUCTOS );
		mensaje.setTiempos( relojes.toLongArray() );
		manejadorMensajes.enviar( mensaje, "productos.pagos" );
		setSnapshot( true );
	}
	
	public void procesarMensaje( ID origen, Operacion operacion, String mensaje, long[] tiempos) {
		procesarCorte( origen , operacion, mensaje );
		// Incrementa tiempo reloj propio, y ajusta los tiempos de los otros relojes
	 	relojes.ajustarTiempos( RelojVectorial.INDEX_PRODUCTOS, tiempos );
		if (operacion ==  Operacion.RESERVAR_PRODUCTO) { 	
        	JsonNode node;
			try {
				ObjectMapper mapper = new ObjectMapper();
				node = mapper.readTree( mensaje );
				int compra = node.get("compra").asInt();
	        	int producto = node.get("producto").asInt(); 
	        	
	        	reservarProducto( compra,producto );
			} catch (IOException e) {
				System.out.println("[*][PRODCUTOS] IOException: " + e.getMessage() );
			}
        }
        else if (operacion == Operacion.INFORMAR_INFRACCION) {
		 	JsonNode node;
			try {
				ObjectMapper mapper = new ObjectMapper();
				node = mapper.readTree( mensaje );
				int compra =  node.get( "compra" ).asInt();
	        	boolean infraccion = node.get("infraccion").asBoolean();
	        	
	        	actualizarInfraccion(compra, infraccion);
			} catch (IOException e) {
				System.out.println("[*][PRODCUTOS] IOException: " + e.getMessage() );
			}
        }
        else if (operacion == Operacion.AUTORIZAR_PAGO) {
        	JsonNode node;
			try {
				ObjectMapper mapper = new ObjectMapper();
				node = mapper.readTree( mensaje );
				int compra =  node.get( "compra" ).asInt();
	        	boolean aut = node.get("autorizado").asBoolean();
	        	
	        	informarPago( compra, aut );
			} catch (IOException e) {
				System.out.println("[*][PRODCUTOS] IOException: " + e.getMessage() );
			}
        	
        }
        else if (operacion== Operacion.ENVIAR) {
        	JsonNode node;
			try {
				ObjectMapper mapper = new ObjectMapper();
				node = mapper.readTree( mensaje );
				int compra =  node.get( "compra" ).asInt();
	        	
	        	enviarProducto( compra );
			} catch (IOException e) {
				System.out.println("[*][PRODCUTOS] IOException: " + e.getMessage() );
			}
        }	
        else if (operacion == Operacion.SNAPSHOT) {
   		 	if (origen == ID.S_ENVIOS) {
   		 		if (canalEnvios.getEstado() == servidor.Canal.Estado.DESABILITADO) {
   		 			
   		 			canalEnvios.setEstado( servidor.Canal.Estado.COMPLETO );
   		 			canalCompras.setEstado( servidor.Canal.Estado.LOGGING );
   		 			canalInfracciones.setEstado( servidor.Canal.Estado.LOGGING );
   		 			canalPagos.setEstado(servidor.Canal.Estado.LOGGING  );
   				
   		 			almacenarEstadoCorte();
   		
   		 			Mensaje msj = new Mensaje();
   		 			msj.setIdentificador( ID.S_PRODUCTOS );
   		 			msj.setOper( Operacion.SNAPSHOT );
		   		 			
	   		 		relojes.tick( RelojVectorial.INDEX_PRODUCTOS );
	   				msj.setTiempos( relojes.toLongArray() );
			 		manejadorMensajes.enviar( msj, "productos.envios" );
	   		 		
			 		relojes.tick( RelojVectorial.INDEX_PRODUCTOS );
	   				msj.setTiempos( relojes.toLongArray() );
   		 			manejadorMensajes.enviar( msj, "productos.compras" );
	   					
	   		 		relojes.tick( RelojVectorial.INDEX_PRODUCTOS );
	   				msj.setTiempos( relojes.toLongArray() );
   		 			manejadorMensajes.enviar( msj, "productos.infracciones" );
	   		 			
	   		 		relojes.tick( RelojVectorial.INDEX_PRODUCTOS );
	   				msj.setTiempos( relojes.toLongArray() );
		 			manejadorMensajes.enviar( msj, "productos.pagos" );
   		 		}
   		 		else if (canalEnvios.getEstado() == servidor.Canal.Estado.LOGGING) {
	   		 		canalEnvios.setEstado( servidor.Canal.Estado.COMPLETO );
   		 			cerrarCorte();
   		 		}
   		 	}
   		 	else if (origen == ID.S_COMPRAS) {
   		 		if (canalCompras.getEstado() == servidor.Canal.Estado.DESABILITADO) {
   		 			
	   		 		canalCompras.setEstado( servidor.Canal.Estado.COMPLETO );
	   		 		canalEnvios.setEstado( servidor.Canal.Estado.LOGGING );
   		 			canalInfracciones.setEstado( servidor.Canal.Estado.LOGGING );
   		 			canalPagos.setEstado(servidor.Canal.Estado.LOGGING  );
   				
   		 			almacenarEstadoCorte();
   		
   		 			Mensaje msj = new Mensaje();
   		 			msj.setIdentificador( ID.S_PRODUCTOS );
   		 			msj.setOper( Operacion.SNAPSHOT );
	   		 			
	   		 		relojes.tick( RelojVectorial.INDEX_PRODUCTOS );
	   				msj.setTiempos( relojes.toLongArray() );
			 		manejadorMensajes.enviar( msj, "productos.compras" );
			 			
	   		 		relojes.tick( RelojVectorial.INDEX_PRODUCTOS );
	   				msj.setTiempos( relojes.toLongArray() );
   		 			manejadorMensajes.enviar( msj, "productos.envios" );
	   		 			
	   		 		relojes.tick( RelojVectorial.INDEX_PRODUCTOS );
	   				msj.setTiempos( relojes.toLongArray() );
   		 			manejadorMensajes.enviar( msj, "productos.infracciones" );
   		 			
	   		 		relojes.tick( RelojVectorial.INDEX_PRODUCTOS );
	   				msj.setTiempos( relojes.toLongArray() );
		 			manejadorMensajes.enviar( msj, "productos.pagos" );
   		 		}
   		 		else if (canalCompras.getEstado() == servidor.Canal.Estado.LOGGING) {
   		 			canalCompras.setEstado( servidor.Canal.Estado.COMPLETO );
   		 			cerrarCorte();
   		 		}
		 	}	
	   		 else if (origen == ID.S_INFRACCIONES) {
			 		if (canalInfracciones.getEstado() == servidor.Canal.Estado.DESABILITADO) {
			 			
		   		 		canalInfracciones.setEstado( servidor.Canal.Estado.COMPLETO );
			 			canalCompras.setEstado( servidor.Canal.Estado.LOGGING );
			 			canalEnvios.setEstado( servidor.Canal.Estado.LOGGING );
			 			canalPagos.setEstado(servidor.Canal.Estado.LOGGING  );
					
			 			almacenarEstadoCorte();
			
			 			Mensaje msj = new Mensaje();
			 			msj.setIdentificador( ID.S_PRODUCTOS );
			 			msj.setOper( Operacion.SNAPSHOT );
			 			
			 			relojes.tick( RelojVectorial.INDEX_PRODUCTOS );
		   				msj.setTiempos( relojes.toLongArray() );
	   		 			manejadorMensajes.enviar( msj, "productos.infracciones" );
	   		 			
			 			relojes.tick( RelojVectorial.INDEX_PRODUCTOS );
		   				msj.setTiempos( relojes.toLongArray() );
			 			manejadorMensajes.enviar( msj, "productos.envios" );
			 			
			 			relojes.tick( RelojVectorial.INDEX_PRODUCTOS );
		   				msj.setTiempos( relojes.toLongArray() );
			 			manejadorMensajes.enviar( msj, "productos.compras" );
			 			
			 			relojes.tick( RelojVectorial.INDEX_PRODUCTOS );
		   				msj.setTiempos( relojes.toLongArray() );
			 			manejadorMensajes.enviar( msj, "productos.pagos" );
			 		}
			 		else if (canalInfracciones.getEstado() == servidor.Canal.Estado.LOGGING) {
			 			canalInfracciones.setEstado( servidor.Canal.Estado.COMPLETO );
			 			cerrarCorte();
			 		}
			 }
	   		 else if (origen == ID.S_PAGOS) {
		 		if (canalPagos.getEstado() == servidor.Canal.Estado.DESABILITADO) {
		 			
		 			canalPagos.setEstado(servidor.Canal.Estado.COMPLETO  );
	   		 		canalInfracciones.setEstado( servidor.Canal.Estado.LOGGING );
		 			canalCompras.setEstado( servidor.Canal.Estado.LOGGING );
		 			canalEnvios.setEstado( servidor.Canal.Estado.LOGGING );
				
		 			almacenarEstadoCorte();
		
		 			Mensaje msj = new Mensaje();
		 			msj.setIdentificador( ID.S_PRODUCTOS );
		 			msj.setOper( Operacion.SNAPSHOT );
		 			
		 			relojes.tick( RelojVectorial.INDEX_PRODUCTOS );
	   				msj.setTiempos( relojes.toLongArray() );
		 			manejadorMensajes.enviar( msj, "productos.pagos" );
		 			
		 			relojes.tick( RelojVectorial.INDEX_PRODUCTOS );
	   				msj.setTiempos( relojes.toLongArray() );
		 			manejadorMensajes.enviar( msj, "productos.envios" );
		 			
		 			relojes.tick( RelojVectorial.INDEX_PRODUCTOS );
	   				msj.setTiempos( relojes.toLongArray() );
		 			manejadorMensajes.enviar( msj, "productos.compras" );
		 			
		 			relojes.tick( RelojVectorial.INDEX_PRODUCTOS );
	   				msj.setTiempos( relojes.toLongArray() );
		 			manejadorMensajes.enviar( msj, "productos.infracciones" );
		 		}
		 		else if (canalPagos.getEstado() == servidor.Canal.Estado.LOGGING) {
		 			canalPagos.setEstado( servidor.Canal.Estado.COMPLETO );
		 			cerrarCorte();
		 		}
		 	}
        }
		// Guarda el reloj actual en archivo.
	 	relojDAO.guardarRelojes( relojes.toLongArray() );
	}

	
	public List<CompraProducto> listaComprasProductos() {
		return comprasProductos;
	}
	
	public List<Producto> listaProductos() {
    	return productos;
	}
	
	public void reservarProducto( int compra, int producto ) {
		CompraProducto cp = new CompraProducto( compra,producto );
		cp.setEstado( Estado.RESERVAR );
		for (Producto prod: productos) {
			if (prod.getCodigo() == producto) {
				if (prod.getStock() > 0) {
					prod.setStock( prod.getStock() - 1 );
					cp.setEstado( Estado.RESERVADO );
				}
				else {
					cp.setEstado( Estado.SIN_STOCK );
				}
				comprasProductos.add( cp );
				break;
			}
		}	
	}
	
	public void actualizarInfraccion( int compra, boolean inf ) {
		for (CompraProducto cp: comprasProductos) {
			if (cp.getCompra() == compra) {
				if (inf) {
					liberarProducto( cp.getProducto() );
					cp.setEstado( Estado.INFRACCION );
				}
				else 
					cp.setEstado( Estado.SIN_INFRACCION );
				break;
			}
		}	
	}
	
	public void informarPago( int compra, boolean pago ) {
		for (CompraProducto cp: comprasProductos) {
			if (cp.getCompra() == compra) {
				cp.setEstado( (pago) ? Estado.PAGO_OK: Estado.PAGO_RECHAZADO );
				break;
			}
		}	
	}
	
	public void enviarProducto( int compra ) {
		for (CompraProducto cp: comprasProductos) {
			if (cp.getCompra() == compra) {
				cp.setEstado( Estado.ENVIADO );
				break;
			}
		}
	}
	
	private void liberarProducto( int producto ) {
		for (Producto p: productos) {
			if (p.getCodigo() == producto) {
				p.setStock( p.getStock() + 1 );
				break;
			}
		}
	}
	
	public String comprasProductoEstadoString() {
		return comprasProductoToString( comprasProductos );
	}
	
	public String comprasProductoPersistidoString() {
		ProductoDAO pdao = new ProductoDAO();
		List<CompraProducto> list = pdao.obtenerComprasProductos();
		return comprasProductoToString(list);
	}
	
	private String comprasProductoToString( List<CompraProducto> list) {
		ObjectMapper mapper = new ObjectMapper();
		ArrayNode arrayNode = mapper.createArrayNode();
		if (list != null)
			for (CompraProducto cp: list) {
				ObjectNode node = mapper.createObjectNode();
				node.put("compra", cp.getCompra() );
				node.put("producto", cp.getProducto() );
				node.put("estado", cp.getEstado().toString() );
				arrayNode.add( node );
			}
		return arrayNode.toString();
	}
	
	public String productosToString() {
		ObjectMapper mapper = new ObjectMapper();
		ArrayNode arrayNode = mapper.createArrayNode();
		if (productos != null)
			for (Producto prod: productos) {
				ObjectNode node = mapper.createObjectNode();
				node.put("codigo", prod.getCodigo() );
				node.put("descripcion", prod.getDescripcion() );
				node.put("precio", prod.getPrecio() );
				node.put("proveedor",prod.getProveedorDenominacion() );
				arrayNode.add( node );
			}
		return arrayNode.toString();
	}
	
	public void almacenarEstado() {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		ProductoDAO edao = new ProductoDAO();
		System.out.println("[*][PRODUCTOS] almacenando estado de envios...  " + comprasProductos.size() + " registros, " + dateFormat.format( new Date() ) );
		if (comprasProductos.size() > 0)
			edao.guardarComprasProductos( comprasProductos);
	}
	

	private void procesarCorte( ID origen, Operacion operacion, String mensaje ) {
		if (operacion == Operacion.SNAPSHOT)
			return;
		if ((canalCompras.getNombre().compareToIgnoreCase( origen.toString())  == 0) && canalCompras.getEstado() == servidor.Canal.Estado.LOGGING) {
			Mensaje m = new Mensaje();
			m.setIdentificador( origen );
			m.setOper( operacion );
			m.setMensaje(mensaje);
			canalCompras.agregarMensaje( m );
		}
		else if ((canalEnvios.getNombre().compareToIgnoreCase( origen.toString()) == 0) && canalEnvios.getEstado() == servidor.Canal.Estado.LOGGING) {
			Mensaje m = new Mensaje();
			m.setIdentificador( origen );
			m.setOper( operacion );
			m.setMensaje(mensaje);
			canalEnvios.agregarMensaje( m );
		}
		else if ((canalInfracciones.getNombre().compareToIgnoreCase( origen.toString()) == 0) && canalInfracciones.getEstado() == servidor.Canal.Estado.LOGGING) {
			Mensaje m = new Mensaje();
			m.setIdentificador( origen );
			m.setOper( operacion );
			m.setMensaje(mensaje);
			canalInfracciones.agregarMensaje( m );
		}
		
	}
	
	private void cerrarCorte() {
		if (canalCompras.getEstado() == servidor.Canal.Estado.COMPLETO && canalEnvios.getEstado() == servidor.Canal.Estado.COMPLETO 
				&& canalInfracciones.getEstado() == servidor.Canal.Estado.COMPLETO && canalPagos.getEstado() == servidor.Canal.Estado.COMPLETO) {
			
			almacenarCanalesCorte();
			
			canalCompras.setEstado( servidor.Canal.Estado.DESABILITADO );
			canalCompras.setMensajes( new ArrayList<Mensaje>() );
			
			canalEnvios.setEstado( servidor.Canal.Estado.DESABILITADO );
			canalEnvios.setMensajes( new ArrayList<Mensaje>() );
			
			canalInfracciones.setEstado( servidor.Canal.Estado.DESABILITADO );
			canalInfracciones.setMensajes( new ArrayList<Mensaje>() );
			
			canalPagos.setEstado( servidor.Canal.Estado.DESABILITADO );
			canalPagos.setMensajes( new ArrayList<Mensaje>() );
			
		}
	}
	
	private void almacenarEstadoCorte() {
		comprasProductosCorte = new ArrayList<CompraProducto>( comprasProductos );
	}
	
	private void almacenarCanalesCorte() {
		ObjectMapper mapper = new ObjectMapper();
	
		// Nodo raiz
		ObjectNode nodeCorte = mapper.createObjectNode();
		
		// Nodo estado; array de comprasProductos
		ArrayNode nodeCompras = mapper.createArrayNode();
		for (CompraProducto cp: comprasProductosCorte) {
			ObjectNode nodeCompraProducto = mapper.createObjectNode();
			nodeCompraProducto.put("numero", cp.getCompra() );
			nodeCompraProducto.put("producto", cp.getProducto() );
			nodeCompraProducto.put("estado", cp.getEstado().toString() );
			nodeCompras.add( nodeCompraProducto );
		}
		nodeCorte.put("estado", nodeCompras.toString() );
		
		// Nodo canales: array de mensajes
		ArrayNode nodeCanales = mapper.createArrayNode();
		for (Mensaje m: canalEnvios.getMensajes()) {
			ObjectNode nodeCanal = mapper.createObjectNode();
			nodeCanal.put("operacion", m.getOper().toString() );
			nodeCanal.put("mensaje", m.getMensaje().toString() );
			nodeCanales.add( nodeCanal );
		}
		for (Mensaje m: canalCompras.getMensajes()) {
			ObjectNode nodeCanal = mapper.createObjectNode();
			nodeCanal.put("operacion", m.getOper().toString() );
			nodeCanal.put("mensaje", m.getMensaje().toString() );
			nodeCanales.add( nodeCanal );
		}
		for (Mensaje m: canalInfracciones.getMensajes()) {
			ObjectNode nodeCanal = mapper.createObjectNode();
			nodeCanal.put("operacion", m.getOper().toString() );
			nodeCanal.put("mensaje", m.getMensaje().toString() );
			nodeCanales.add( nodeCanal );
		}
		for (Mensaje m: canalPagos.getMensajes()) {
			ObjectNode nodeCanal = mapper.createObjectNode();
			nodeCanal.put("operacion", m.getOper().toString() );
			nodeCanal.put("mensaje", m.getMensaje().toString() );
			nodeCanales.add( nodeCanal );
		}
		nodeCorte.put( "canales", nodeCanales.toString() );
		
		try {
			String archivo = System.getProperty("user.dir") + "/data/" + "corte" + relojes.toString() + ".json";
			File file = new File( archivo );
			file.createNewFile();
			FileOutputStream output = new FileOutputStream( archivo );
			JsonGenerator g = mapper.getFactory().createGenerator( output );
			mapper.writeValue( g, nodeCorte );
		} catch (IOException e) {
			System.out.println( "[*][COMPRAS] IOException guardando Corte: " + e.getMessage() );
		}
		
	}
	
	public boolean isSnapshot() {
		return snapshot;
	}

	public void setSnapshot(boolean snapshot) {
		this.snapshot = snapshot;
	}
}
