package servidor;

public interface Servidor {
	
	public static final String INICIAR_AUTOMATICO = "INICIAR_AUTOMATICO";
	public static final String INICIAR_PASOPASO = "INICIAR_PASOPASO";
	public static final String DETENER = "DETENER";
	public static final String REINICIAR = "REINICIAR";
	public static final String DERRIBAR = "DERRIBAR";
	public static final String PERSISTIR = "PERSISTIR";
	public static final String ESTADO_ACTUAL = "ESTADO_ACTUAL";
	public static final String ESTADO_ULTIMO = "ESTADO_ULTIMO";
	public static final String SIGUIENTE = "SIGUIENTE";
	public static final String CONFIG = "CONFIG";
	public static final String SERVERSTATUS = "SERVERSTATUS";
	public static final String LISTA_PRODUCTOS = "LISTA_PRODUCTOS";
	public static final String CORTE = "CORTE";
	
	public enum Modo {AUTOMATICO,PASO_PASO};
	
	public Modo modo = Modo.AUTOMATICO;
	
	public int iniciar();
	public void detener();
	public int reiniciar();
	public void derribar();
	public int persistirEstado();
	public String estadoActual();
	public String estadoUltimo();
	public int siguientePaso();
	public int setearConfig( int[] params );
}
