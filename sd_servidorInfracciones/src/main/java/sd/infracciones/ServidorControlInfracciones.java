/**
 * ServidorInfracciones.java
 */

package sd.infracciones;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import sd.infracciones.negocio.ServidorInfracciones;
import sd.infracciones.persistencia.ConfigDAO;
import sd.infracciones.persistencia.ManejadorPersistencia;

import servidor.Servidor;

public class ServidorControlInfracciones implements Servidor {
	
	private final int PUERTO = 50002; //Puerto para la conexión
	private ServerSocket serverSocket; 	//Socket del servidor
	private Socket socket; 		//Socket 
	private DataInputStream dis;
	private DataOutputStream dos;
	
	private boolean activo;
	private boolean automatico;
	
	ManejadorPersistencia manejadorPersistencia;
	ServidorInfracciones servidorInfracciones;
	
	Config config;
	
	public ServidorControlInfracciones() { 
		setActivo(false); 
		automatico = true;
	}
	
	public static void main( String[] args ) {
		ServidorControlInfracciones si = new ServidorControlInfracciones();
		si.setup();
		si.atender();
	}
	
	private void setup() {
		try {
			serverSocket = new ServerSocket( PUERTO );
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public int iniciar() {
		System.out.println(	"[*][INFRACCIONES] Iniciando Servidor ..." );
		servidorInfracciones = new ServidorInfracciones();
		if (automatico) {
			System.out.println(	"[*][INFRACCIONES] Modo automatico ..." );
			config = abrirConfig();
			servidorInfracciones.iniciarAutomatico();
			manejadorPersistencia = new ManejadorPersistencia( config.getMinutosPersistencia(), servidorInfracciones );
			manejadorPersistencia.start();
		}
		else { 
			System.out.println(	"[*][INFRACCIONES] Iniciando Paso a paso ..." );
			config = null;
			servidorInfracciones.iniciarPap();
		}

		setActivo( true );
		return 0;
	}

	public void detener() {
		System.out.println("[*][INFRACCIONES] Deteniendo servidor ... ");
        if (automatico)
        	manejadorPersistencia.stop();
        servidorInfracciones.detener();
		setActivo( false );
	}

	public int reiniciar() {
		detener();
		iniciar();
		return 0;
	}

	public void derribar() {
		if (automatico)
			manejadorPersistencia.stop();
		setActivo( false );
	}

	public String estadoActual() {
		return servidorInfracciones.infraccionesEstadoString();
	}

	public String estadoUltimo() {
		return servidorInfracciones.infraccionesPersistidasString();
	}

	public int siguientePaso() {
		servidorInfracciones.recibirSiguienteMensaje();
		return 0;
	}
	
	public int iniciarCorteConsistente() {
		servidorInfracciones.comenzarCorteConsistente();
		return 0;
	}
	
	public int persistirEstado() {
		servidorInfracciones.almacenarEstado();
		return 0;
	}
	
	
	public int setearConfig( int[] params ) {
		ConfigDAO configdao = new ConfigDAO();
		return configdao.guardarConfiguracion( new Config( (params[0] == 0) ? "automatico":"manual",params[1],params[2] ) );
	}
	
	private Config abrirConfig() {
		ConfigDAO configdao = new ConfigDAO();
		config = configdao.obtenerConfiguracion();
		return config;
	}

	public void atender() {
		while(true){
            System.out.println("[*][INFRACCIONES]  ejecutando....");
            try {
				socket = serverSocket.accept();
				dis = new DataInputStream( socket.getInputStream() );
	            
	            String msjRcv = (String) dis.readUTF();
	            System.out.println("[*][INFRACCIONES]  Mensaje recibido: " + msjRcv );
	           
	            String msjSnd = procesar( msjRcv );
	            
	            dos = new DataOutputStream( socket.getOutputStream());
	            dos.writeUTF( msjSnd );
	            System.out.println("[*][INFRACCIONES]  Enviando respuesta: " + msjSnd );
	            
	            dis.close();
	            dos.close();
	            socket.close();
			} catch (IOException e) {
				System.out.println("[*][INFRACCIONES]  Exception atender ... " + e.getMessage() );
				e.printStackTrace();
			}
        }
	}

	/* procesar: procesa comando del que se envian mediante sockets */
	private String procesar( String msj ) {
		String []params = msj.split(" ");
		System.out.println( "[*][INFRACCIONES] , procesando pedido: " + params[0] ); 
		if (params[0].compareTo(INICIAR_AUTOMATICO) == 0) {
			automatico = true;
			iniciar();
			return "EJECUTANDO AUTOMATICO";
		}
		if (params[0].compareTo(INICIAR_PASOPASO) == 0) {
			automatico = false;
			iniciar();
			return "EJECUTANDO PAP";
		}
		else if (params[0].compareTo(DETENER) == 0) {
			detener();
			return "DETENIDO";
		}
		else if (params[0].compareTo(REINICIAR) == 0) {
			reiniciar();
			return "REINICIADO";
		}
		else if (params[0].compareTo(DERRIBAR) == 0) {
			derribar();
			return "CAIDO";
		}
		else if (params[0].compareTo(ESTADO_ACTUAL) == 0) {
			return estadoActual();
		}
		else if (params[0].compareTo(ESTADO_ULTIMO) == 0) {
			return estadoUltimo();
		}
		else if (params[0].compareTo(SIGUIENTE) == 0) {
			siguientePaso();
			return "OK";
		}
		else if (params[0].compareTo(PERSISTIR) == 0) {
			persistirEstado();
			return "OK";
		}
		else if (params[0].compareTo(CORTE) == 0) {
			iniciarCorteConsistente();
			return "OK";
		}
		else if (params[0].compareTo( SERVERSTATUS ) == 0) {
			return (activo) ? "EJECUTANDO " + (automatico ? "AUTOMATICO":"PASO PASO"):"DETENIDO";
		}
		else {
			// comando invalido
			return "COMANDO INVALIDO";
		}
	}
	
	
	
	public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}


	public boolean isAutomatico() {
		return automatico;
	}

	public void setAutomatico(boolean automatico) {
		this.automatico = automatico;
	}

}
