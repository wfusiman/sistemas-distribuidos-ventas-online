/**
 * ManejadorPersistencia.java
 */

package sd.infracciones.persistencia;

import sd.infracciones.negocio.ServidorInfracciones;

public class ManejadorPersistencia implements Runnable {

	private volatile Thread blinker;
	int minutos;
	ServidorInfracciones si;
	
	public ManejadorPersistencia( int min, ServidorInfracciones si ) {
		this.minutos = min;
		this.si = si;
	}
	
	public void start() {
		blinker = new Thread( this );
		System.out.println("[*][INFRACCIONES] Iniciando Manejador de persistencia..." );
		blinker.start();
	}
	
	@Override
	public void run() {
		System.out.println("[*][INFRACCIONES]  Manejador de persistencia, actualizando estado cada " + minutos + " minutos" );
		Thread thisThread = Thread.currentThread();
		while (blinker == thisThread) {
			try {
				Thread.sleep( minutos * 60000 );
				si.almacenarEstado();
			} catch (InterruptedException e) {
				System.out.println("[*][INFRACCIONES] , InterruptedException " + e.getMessage() );
			}
		}
	}
	
	public void stop() {
		System.out.println("[*][INFRACCIONES] Deteniendo Manejador de persistencia..." );
		blinker = null;
	}
}
