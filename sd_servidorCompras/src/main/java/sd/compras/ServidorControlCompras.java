package sd.compras;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import sd.compras.negocio.ServidorCompras;
import sd.compras.persistencia.ConfigDAO;
import sd.compras.persistencia.ManejadorPersistencia;

import servidor.Servidor;

public class ServidorControlCompras implements Servidor {
	
	private final int PUERTO = 50001; //Puerto para la conexión
	private ServerSocket serverSocket; 	//Socket del servidor
	private Socket socket; 		//Socket 
	private DataInputStream dis;
	private DataOutputStream dos;
	
	private boolean activo;
	private boolean automatico;
	
	private Config config;
	
	ManejadorPersistencia manejadorPersistencia;
	ServidorCompras servidorCompras;

	public ServidorControlCompras() { 
		setActivo(false); 
		automatico = true;
	}
	
	public static void main( String[] args ) {
		ServidorControlCompras sc = new ServidorControlCompras();
		sc.setup();
		sc.atender();
	}
	
	private void setup() {
		try {
			serverSocket = new ServerSocket( PUERTO );
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Inicia el servidor de compras 
	 */
	public int iniciar() {
		System.out.println("[*][COMPRAS] Iniciando Servidor... ");
		servidorCompras = new ServidorCompras();
		if (automatico) {
			System.out.println("[*][COMPRAS] Modo automatico... " );
			servidorCompras.iniciarAutomatico();
			config = abrirConfig();
			manejadorPersistencia = new ManejadorPersistencia( config.getMinutosPersistencia(), servidorCompras );
			manejadorPersistencia.start();
		}
		else  {
			System.out.println("[*][]COMPRAS Modo paso a paso ... " );
			config = null;
			servidorCompras.iniciarPap();
		}
		
		setActivo( true );
		return 0;
	}

	/**
	 * Detiene el servidor de compras
	 */
	public void detener() {
		System.out.println("[*][COMPRAS] Deteniendo Servidor... ");
        if (automatico) 
        	manejadorPersistencia.stop();
        servidorCompras.detener();
		setActivo( false );
	}

	/**
	 * Detiene e inicia el servidor de compras
	 */
	public int reiniciar() {
		detener();
		iniciar();
		return 0;
	}

	/**
	 * Derriba el servidor de compras
	 */
	public void derribar() {
		
	}

	/**
	 * Retorna el estado actual del servidor Compras
	 */
	public String estadoActual() {
		return servidorCompras.comprasEstadoString();
	}

	/**
	 * Retorna el ultimo estado persistido del servidor Compras
	 */
	public String estadoUltimo() {
		return servidorCompras.comprasPerisitidasString();
	}

	/**
	 * Realiza la lectura del siguiente mensaje (si hay) si el servidor Compras se esta ejecutando en modo Paso a paso
	 */
	public int siguientePaso() {
		servidorCompras.recibirSiguienteMensaje();
		return 0;
	}
	
	/**
	 * Realiza la persistencia del estado actual del servidor de Compras, si se ejecuta en modo Paso a paso
	 */
	public int persistirEstado() {
		servidorCompras.almacenarEstado();
		return 0;
	}
	
	/**
	 * Comienza la ejecucion de un corte consistente
	 * @return
	 */
	public int iniciarCorteConsistente() {
		servidorCompras.comenzarCorteConsistente();
		return 0;
	}
	
	/**
	 * Setea los parametros de configuracion.
	 */
	public int setearConfig( int[] params ) {
		ConfigDAO configdao = new ConfigDAO();
		
		return configdao.guardarConfiguracion( new Config( (params[0] == 0) ? "automatico":"manual" ,params[1] ) );
	}
	
	/**
	 * Obtiene los parametros de configuracion desde el archivo
	 * @return
	 */
	private Config abrirConfig() {
		ConfigDAO configdao = new ConfigDAO();
		config = configdao.obtenerConfiguracion();
		return config;
	}

	/**
	 * Atiende peticiones realizas al servidor mediante sockets
	 */
	public void atender() {
		while(true){
            System.out.println("[*][COMPRAS] Servidor ejecutando....");
            try {
				socket = serverSocket.accept();
				dis = new DataInputStream( socket.getInputStream() );
	            
	            String msjRcv = (String) dis.readUTF();
	            System.out.println("[*][COMPRAS] Recibido: " + msjRcv );
	           
	            String msjSnd = procesar( msjRcv );
	            
	            dos = new DataOutputStream( socket.getOutputStream());
	            System.out.println("[*][COMPRAS] Respuesta: " + msjSnd );
	            dos.writeUTF( msjSnd );
	            
	            
	            dis.close();
	            dos.close();
	            socket.close();
			} catch (IOException e) {
				System.out.println("[COMPRAS] Exception Compras atender ... " + e.getMessage() );
				e.printStackTrace();
			}
        }
	}

	/**
	 *  procesar: procesa peticiones que llegan mediante sockets 
	 */
	private String procesar( String msj ) {
		String []params = msj.split(" ");
		System.out.println( "[*][COMPRAS] Procesando pedido: " + params[0] ); 
		if (params[0].compareTo(INICIAR_AUTOMATICO) == 0) {
			setAutomatico(true);
			iniciar();
			return "EJECUTANDO AUTOMATICO";
		}
		else if (params[0].compareTo(INICIAR_PASOPASO) == 0) {
			setAutomatico(false);
			iniciar();
			return "EJECUTANDO PAP";
		}
		else if (params[0].compareTo(DETENER) == 0) {
			detener();
			return "DETENIDO";
		}
		else if (params[0].compareTo(REINICIAR) == 0) {
			reiniciar();
			return "REINICIADO";
		}
		else if (params[0].compareTo(DERRIBAR) == 0) {
			derribar();
			return "REINICIADO";
		}
		else if (params[0].compareTo(ESTADO_ACTUAL) == 0) {
			return estadoActual();
		}
		else if (params[0].compareTo(ESTADO_ULTIMO) == 0) {
			return estadoUltimo();
		}
		else if (params[0].compareTo(SIGUIENTE) == 0) {
			siguientePaso();
			return "OK";
		}
		else if (params[0].compareTo(PERSISTIR) == 0) {
			persistirEstado();
			return "OK";
		}
		else if (params[0].compareTo(CORTE) == 0) {
			iniciarCorteConsistente();
			return "OK";
		}
		else if (params[0].compareTo( SERVERSTATUS ) == 0) {
			return (activo) ? "EJECUTANDO " + (automatico ? "AUTOMATICO":"PAP"):"DETENIDO";
		}
		else {
			// comando invalido
			return "COMANDO INVALIDO";
		}
	}
	
	/**
	 * retorna si el servidor de negocio de compras esta en ejecution
	 * @return
	 */
	public boolean isActivo() {
		return activo;
	}

	/**
	 * 
	 * @param activo
	 */
	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	public boolean isAutomatico() {
		return automatico;
	}

	public void setAutomatico(boolean automatico) {
		this.automatico = automatico;
	}

}
