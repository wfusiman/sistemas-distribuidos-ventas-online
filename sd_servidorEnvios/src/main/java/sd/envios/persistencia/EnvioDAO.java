package sd.envios.persistencia;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;

import sd.envios.Envio;

public class EnvioDAO {

	private static final String path = System.getProperty("user.dir") + "/data/";
	private String archivo;
	
	public EnvioDAO() {
		this("data.json");
	}
	
	public EnvioDAO( String archivo ) {
		this.setArchivo(archivo);
	}
	
	public Envio obtenerEnvio( int number ) {
		List<Envio> envios = obtenerTodosEnvios();
		for (Envio env: envios) {
			if (env.getNumero() == number)
				return env;
		}
		return null;
	}
	
	public List<Envio> obtenerTodosEnvios() {
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			if (!existeArchivo())
				return null;
			Envio[] envs = mapper.readValue( new File( path+getArchivo() ), Envio[].class );
			return new ArrayList<Envio>( Arrays.asList( envs ));
		} catch (IOException e) {
			System.out.println("[*][ENVIOS] IO Excepction: " + e.getMessage() );
			return null;
		}			
	}

	
	public int guardarEnvio( Envio envio ) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			if (!existeArchivo() && !crearArchivo())
				return -1;
			FileOutputStream output = new FileOutputStream( path+getArchivo() );
			JsonGenerator g = mapper.getFactory().createGenerator( output );
			mapper.writeValue( g,envio );
			return 1;
		} catch (IOException e) {
			System.out.println( "[*][ENVIOS] IOException: " + e.getMessage() );
			return -1;
		}
	}
	
	public int guardarEnvios( List<Envio> envios ) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			if (!existeArchivo() && !crearArchivo())
				return -1;
			FileOutputStream output = new FileOutputStream( path+getArchivo() );
			JsonGenerator g = mapper.getFactory().createGenerator( output );
			mapper.writeValue( g, envios );
			return 1;
		} catch (IOException e) {
			System.out.println( "[*][ENVIOS] IOException: " + e.getMessage() );
			return -1;
		}
	}
	
	private boolean existeArchivo() {
		File file = new File( path+getArchivo() );
		return file.exists();
	}
	
	private boolean crearArchivo() {
		File file = new File( path+getArchivo() );
		try {
			file.createNewFile();
			return true;
		} catch (IOException e) {
			return false;
		}
	}
	
	private String getArchivo() {
		return archivo;
	}

	private void setArchivo(String archivo) {
		this.archivo = archivo;
	}
	
}
