package sd.control.gui;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import sd.control.Config;
import sd.control.mensajeria.ManejadorMensajesControl;
import sd.control.persistencia.ConfigDAO;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;

import java.awt.FlowLayout;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import servidor.Mensaje;
import servidor.Mensaje.ID;
import servidor.Mensaje.Operacion;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.IOException;

public class GenerarCompra extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textFieldProducto;
	private JFrame frameListaProductos;
	private JTable tablaProductos;

	private ManejadorMensajesControl mensajeria;
	
	private JDialog frameSeleccionProducto;
	private JTextField txtComprador;
	private JTextField txtNumero;
	private JRadioButton rdbtnRetira;
	private JRadioButton rdbtnEnvio;
	private JRadioButton rdbtnAutomatico1;
	private JRadioButton rdbtnConfirma;
	private JRadioButton rdbtnRechazar;
	private JRadioButton rdbtnAutomatico2;
	private JTextField txtPrecio;
	
	private int codigoProducto;
	
	private String listaProductos;
	
	private Config config;
	
	/**
	 * Create the frame.
	 */
	public GenerarCompra( ManejadorMensajesControl mensajeria, String listaProductos ) {
		this.listaProductos = listaProductos;
		setMensajeria( mensajeria );
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 712, 462);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JLabel lblIniciarCompra = new JLabel("Iniciar Compra");
		lblIniciarCompra.setHorizontalAlignment(SwingConstants.CENTER);
		lblIniciarCompra.setFont(new Font("Garuda", Font.BOLD, 24));
		contentPane.add(lblIniciarCompra, BorderLayout.NORTH);
		
		JPanel panelCentral = new JPanel();
		contentPane.add(panelCentral, BorderLayout.CENTER);
		GridBagLayout gbl_panelCentral = new GridBagLayout();
		gbl_panelCentral.columnWidths = new int[] {150, 150, 250, 150};
		gbl_panelCentral.rowHeights = new int[] {51, 51, 51, 51, 51, 0, 51};
		gbl_panelCentral.columnWeights = new double[]{0.0, 1.0, 1.0};
		gbl_panelCentral.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
		panelCentral.setLayout(gbl_panelCentral);
		
		txtPrecio = new JTextField();
		txtPrecio.setFont(new Font("Garuda", Font.PLAIN, 16));
		txtPrecio.setEditable(false);
		GridBagConstraints gbc_txtPrecio = new GridBagConstraints();
		gbc_txtPrecio.insets = new Insets(5, 5, 5, 5);
		gbc_txtPrecio.fill = GridBagConstraints.BOTH;
		gbc_txtPrecio.gridx = 1;
		gbc_txtPrecio.gridy = 3;
		panelCentral.add(txtPrecio, gbc_txtPrecio);
		txtPrecio.setColumns(10);
		
		JLabel lblPrecio = new JLabel("Precio:");
		lblPrecio.setHorizontalAlignment(SwingConstants.RIGHT);
		GridBagConstraints gbc_lblPrecio = new GridBagConstraints();
		gbc_lblPrecio.fill = GridBagConstraints.BOTH;
		gbc_lblPrecio.insets = new Insets(5, 5, 5, 5);
		gbc_lblPrecio.gridx = 0;
		gbc_lblPrecio.gridy = 3;
		panelCentral.add(lblPrecio, gbc_lblPrecio);
		
		JLabel lblComprador = new JLabel("Comprador:");
		lblComprador.setHorizontalAlignment(SwingConstants.RIGHT);
		GridBagConstraints gbc_lblComprador = new GridBagConstraints();
		gbc_lblComprador.fill = GridBagConstraints.BOTH;
		gbc_lblComprador.insets = new Insets(5, 5, 5, 5);
		gbc_lblComprador.gridx = 0;
		gbc_lblComprador.gridy = 1;
		panelCentral.add(lblComprador, gbc_lblComprador);
		
		txtComprador = new JTextField();
		txtComprador.setHorizontalAlignment(SwingConstants.LEFT);
		txtComprador.setFont(new Font("Garuda", Font.PLAIN, 16));
		GridBagConstraints gbc_txtComprador = new GridBagConstraints();
		gbc_txtComprador.gridwidth = 3;
		gbc_txtComprador.fill = GridBagConstraints.BOTH;
		gbc_txtComprador.insets = new Insets(5, 5, 5, 0);
		gbc_txtComprador.gridx = 1;
		gbc_txtComprador.gridy = 1;
		panelCentral.add(txtComprador, gbc_txtComprador);
		txtComprador.setColumns(10);
		
		JLabel lblProductoprovedor = new JLabel("Producto/Provedor:");
		lblProductoprovedor.setHorizontalAlignment(SwingConstants.RIGHT);
		GridBagConstraints gbc_lblProductoprovedor = new GridBagConstraints();
		gbc_lblProductoprovedor.fill = GridBagConstraints.BOTH;
		gbc_lblProductoprovedor.insets = new Insets(5, 5, 5, 5);
		gbc_lblProductoprovedor.gridx = 0;
		gbc_lblProductoprovedor.gridy = 2;
		panelCentral.add(lblProductoprovedor, gbc_lblProductoprovedor);
		
		textFieldProducto = new JTextField();
		textFieldProducto.setEditable(false);
		textFieldProducto.setHorizontalAlignment(SwingConstants.LEFT);
		textFieldProducto.setFont(new Font("Garuda", Font.PLAIN, 16));
		textFieldProducto.setText( "" );
		GridBagConstraints gbc_textFieldProducto = new GridBagConstraints();
		gbc_textFieldProducto.gridwidth = 2;
		gbc_textFieldProducto.fill = GridBagConstraints.BOTH;
		gbc_textFieldProducto.insets = new Insets(5, 5, 5, 5);
		gbc_textFieldProducto.gridx = 1;
		gbc_textFieldProducto.gridy = 2;
		panelCentral.add(textFieldProducto, gbc_textFieldProducto);
		textFieldProducto.setColumns(10);
		
		JButton btnSeleccionar = new JButton("seleccionar");
		btnSeleccionar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//recuperarListaProductos();
				mostrarFrameListaProductos();
				//mostrarFrameSeleccion();
			}
		});
		GridBagConstraints gbc_btnSeleccionar = new GridBagConstraints();
		gbc_btnSeleccionar.fill = GridBagConstraints.BOTH;
		gbc_btnSeleccionar.insets = new Insets(5, 5, 5, 0);
		gbc_btnSeleccionar.gridx = 3;
		gbc_btnSeleccionar.gridy = 2;
		panelCentral.add(btnSeleccionar, gbc_btnSeleccionar);
		
		JLabel lblEntrega = new JLabel("Entrega:");
		lblEntrega.setHorizontalAlignment(SwingConstants.RIGHT);
		GridBagConstraints gbc_lblEntrega = new GridBagConstraints();
		gbc_lblEntrega.fill = GridBagConstraints.BOTH;
		gbc_lblEntrega.insets = new Insets(5, 5, 5, 5);
		gbc_lblEntrega.gridx = 0;
		gbc_lblEntrega.gridy = 4;
		panelCentral.add(lblEntrega, gbc_lblEntrega);
		
		ButtonGroup groupEntrega = new ButtonGroup();
				
		rdbtnRetira = new JRadioButton("Retira");
		rdbtnRetira.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_rdbtnRetira = new GridBagConstraints();
		gbc_rdbtnRetira.fill = GridBagConstraints.BOTH;
		gbc_rdbtnRetira.insets = new Insets(5, 50, 5, 5);
		gbc_rdbtnRetira.gridx = 1;
		gbc_rdbtnRetira.gridy = 4;
		panelCentral.add(rdbtnRetira, gbc_rdbtnRetira);
		groupEntrega.add( rdbtnRetira );
		
		rdbtnEnvio = new JRadioButton("Envio");
		rdbtnEnvio.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_rdbtnEnvio = new GridBagConstraints();
		gbc_rdbtnEnvio.fill = GridBagConstraints.BOTH;
		gbc_rdbtnEnvio.insets = new Insets(5, 5, 5, 5);
		gbc_rdbtnEnvio.gridx = 2;
		gbc_rdbtnEnvio.gridy = 4;
		panelCentral.add(rdbtnEnvio, gbc_rdbtnEnvio);
		groupEntrega.add( rdbtnEnvio );
		
		rdbtnAutomatico1 = new JRadioButton("automatico");
		rdbtnAutomatico1.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_rdbtnAutomatico1 = new GridBagConstraints();
		gbc_rdbtnAutomatico1.fill = GridBagConstraints.HORIZONTAL;
		gbc_rdbtnAutomatico1.insets = new Insets(5, 5, 5, 5);
		gbc_rdbtnAutomatico1.gridx = 3;
		gbc_rdbtnAutomatico1.gridy = 4;
		panelCentral.add(rdbtnAutomatico1, gbc_rdbtnAutomatico1);
		groupEntrega.add( rdbtnAutomatico1 );
	
		
		JLabel lblCompra = new JLabel("Compra:");
		lblCompra.setHorizontalAlignment(SwingConstants.RIGHT);
		GridBagConstraints gbc_lblCompra = new GridBagConstraints();
		gbc_lblCompra.fill = GridBagConstraints.BOTH;
		gbc_lblCompra.insets = new Insets(5, 5, 5, 5);
		gbc_lblCompra.gridx = 0;
		gbc_lblCompra.gridy = 5;
		panelCentral.add(lblCompra, gbc_lblCompra);
		
		ButtonGroup groupCompra = new ButtonGroup();
		
		rdbtnConfirma = new JRadioButton("Confirmar");
		rdbtnConfirma.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_rdbtnConfirma = new GridBagConstraints();
		gbc_rdbtnConfirma.fill = GridBagConstraints.BOTH;
		gbc_rdbtnConfirma.insets = new Insets(5, 50, 5, 5);
		gbc_rdbtnConfirma.gridx = 1;
		gbc_rdbtnConfirma.gridy = 5;
		panelCentral.add(rdbtnConfirma, gbc_rdbtnConfirma);
		groupCompra.add( rdbtnConfirma );
		
		rdbtnRechazar = new JRadioButton("Rechazar");
		rdbtnRechazar.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_rdbtnRechazar = new GridBagConstraints();
		gbc_rdbtnRechazar.insets = new Insets(5, 5, 5, 5);
		gbc_rdbtnRechazar.fill = GridBagConstraints.BOTH;
		gbc_rdbtnRechazar.gridx = 2;
		gbc_rdbtnRechazar.gridy = 5;
		panelCentral.add(rdbtnRechazar, gbc_rdbtnRechazar);
		groupCompra.add( rdbtnRechazar );
		
		rdbtnAutomatico2 = new JRadioButton("automatico");
		GridBagConstraints gbc_rdbtnAutomatico2 = new GridBagConstraints();
		gbc_rdbtnAutomatico2.fill = GridBagConstraints.HORIZONTAL;
		gbc_rdbtnAutomatico2.insets = new Insets(0, 5, 5, 5);
		gbc_rdbtnAutomatico2.gridx = 3;
		gbc_rdbtnAutomatico2.gridy = 5;
		panelCentral.add(rdbtnAutomatico2, gbc_rdbtnAutomatico2);
		groupCompra.add( rdbtnAutomatico2 );
		
		JLabel lblNumero = new JLabel("Numero:");
		GridBagConstraints gbc_lblNumero = new GridBagConstraints();
		gbc_lblNumero.anchor = GridBagConstraints.EAST;
		gbc_lblNumero.insets = new Insets(5, 5, 5, 5);
		gbc_lblNumero.gridx = 0;
		gbc_lblNumero.gridy = 0;
		panelCentral.add(lblNumero, gbc_lblNumero);
		
		
		txtNumero = new JTextField();
		txtNumero.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped( KeyEvent e ) {
				char c = e.getKeyChar();
				if((!(Character.isDigit(c))) &&  (c != '\b')) {
					e.consume(); 
				}
			}
		});
		txtNumero.setFont(new Font("Garuda", Font.PLAIN, 16));
		GridBagConstraints gbc_txtNumero = new GridBagConstraints();
		gbc_txtNumero.insets = new Insets(5, 5, 5, 5);
		gbc_txtNumero.fill = GridBagConstraints.BOTH;
		gbc_txtNumero.gridx = 1;
		gbc_txtNumero.gridy = 0;
		panelCentral.add(txtNumero, gbc_txtNumero);
		txtNumero.setColumns(10);
		
		
		JPanel panelButtons = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panelButtons.getLayout();
		flowLayout.setAlignment(FlowLayout.RIGHT);
		contentPane.add(panelButtons, BorderLayout.SOUTH);
		
		JButton btnIniciar = new JButton("Iniciar");
		btnIniciar.addActionListener( new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (txtNumero.getText().length() == 0) {
					JOptionPane.showMessageDialog( null, "Debe especificar numero de compra","Error", JOptionPane.ERROR_MESSAGE );
					return;
				}
				if (txtComprador.getText().length() == 0) {
					JOptionPane.showMessageDialog( null, "Debe Especificar nombre de comprador","Error", JOptionPane.ERROR_MESSAGE );
					return;
				}
				if (textFieldProducto.getText().length() == 0) {
					JOptionPane.showMessageDialog( null, "Debe seleccionar un producto","Error", JOptionPane.ERROR_MESSAGE );
					return;
				}
				if (!rdbtnRetira.isSelected() && !rdbtnEnvio.isSelected() && !rdbtnAutomatico1.isSelected()) {
					JOptionPane.showMessageDialog( null, "Debe seleecionar una forma de entrega","Error", JOptionPane.ERROR_MESSAGE );
					return;
				}
				if (!rdbtnConfirma.isSelected() && !rdbtnRechazar.isSelected() && !rdbtnAutomatico2.isSelected()) {
					JOptionPane.showMessageDialog( null, "Debe seleccionar si compra sera rechazada o confirmada","Error", JOptionPane.ERROR_MESSAGE );
					return;
				}
				iniciarNuevaCompra();
			}
			
		});
		panelButtons.add(btnIniciar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cerrarFrame();
			}
		});
		panelButtons.add(btnCancelar);
	}
	
	/*
	private void recuperarListaProductos() {
		Mensaje mensaje = new Mensaje( Mensaje.Operacion.PRODUCTOS_LIST, "" );
		mensajeria.setIcompra( this );
		mensajeria.enviar( mensaje,"control.productos" );
	}
	*/
	
	private void mostrarFrameListaProductos() {
		frameSeleccionProducto = new JDialog( this );
		frameSeleccionProducto.getContentPane().setLayout( new BorderLayout() );
		frameSeleccionProducto.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		frameSeleccionProducto.setTitle( "Seleccion de Producto");
		
		String[][] datas = {};
        String[] columnNames = new String[] { "Codigo", "Descripcion","Precio", "Proveedor" }; 
        
        DefaultTableModel dtm = new DefaultTableModel( datas, columnNames ) {
			private static final long serialVersionUID = 1L;

			public boolean isCellEditable( int row, int column ) {
                return false;
            }
        };
        try {
        	dtm.setRowCount(0);
        	JsonNode node = new ObjectMapper().readTree( listaProductos );
        	for (JsonNode prod: node) {
        		String[] data = new String[6];
    			data[0] = Integer.toString( prod.get("codigo").asInt() );
    	    	data[1] = prod.get("descripcion").asText();
    	   		data[2] = Float.toString( prod.get("precio").floatValue() );
    	   		data[3] = prod.get("proveedor").asText();
    	   		dtm.addRow( data );
			}
        }
        catch(IOException e) {
        	System.out.println("[*][CONTROL] IOEXception: " + e.getMessage() );
        }
		
		tablaProductos = new JTable( dtm );
		tablaProductos.setFont( new Font("Garuda", Font.PLAIN, 16) );
		JScrollPane sp = new JScrollPane( tablaProductos );
		
		frameSeleccionProducto.getContentPane().add( sp, BorderLayout.CENTER );
		
		JButton btnAceptar = new JButton( "Aceptar" );
		btnAceptar.addActionListener( new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				seleccionarFila();
			}
		});
		frameSeleccionProducto.getContentPane().add( btnAceptar, BorderLayout.SOUTH );
		frameSeleccionProducto.setBounds( this.getBounds() );
		frameSeleccionProducto.setModal(true);
		frameSeleccionProducto.setVisible( true );
	}

	private void seleccionarFila() {
		int row = tablaProductos.getSelectedRow();
		if (row >= 0) {
			setCodigoProducto( Integer.parseInt( tablaProductos.getModel().getValueAt( row, 0 ).toString() ) ); 
			textFieldProducto.setText( getCodigoProducto() + ":" + tablaProductos.getModel().getValueAt( row, 1) + " / " + tablaProductos.getModel().getValueAt( row, 3) );
			txtPrecio.setText( "$" + tablaProductos.getModel().getValueAt( row, 2).toString() );
			frameSeleccionProducto.setVisible( false );
		}
	}
	
	private void iniciarNuevaCompra() {
		ObjectMapper mapper = new ObjectMapper(); // crea el mensaje a enviar
		ObjectNode compra = mapper.createObjectNode();
		compra.put("numero", Integer.parseInt( txtNumero.getText() ) );
		compra.put("comprador", txtComprador.getText() );
		compra.put("producto", getCodigoProducto() );

		boolean envio;
		if (rdbtnAutomatico1.isSelected())
			envio = calcularEnvio();
		else 
			envio = rdbtnRetira.isSelected() ? false:true;
		compra.put("envio",envio );
		
		boolean confirma;
		if (rdbtnAutomatico2.isSelected())
			confirma = calcularConfirmacion();
		else 
			confirma = rdbtnConfirma.isSelected() ? true:false;
		compra.put("confirma", confirma );
		
		Mensaje mensaje = new Mensaje(); // crea el mensajes
		mensaje.setIdentificador( ID.S_CONTROL );
		mensaje.setOper( Operacion.NUEVA_COMPRA ); // setea la operacion

		mensaje.setMensaje( compra.toString() );
		mensajeria.enviar( mensaje,"control.compras" );
		cerrarFrame();
		JOptionPane.showMessageDialog( null,"Compra iniciada", "Compra",JOptionPane.INFORMATION_MESSAGE );
	}
	
	private boolean calcularEnvio() {
		double randomValue = Math.random()*100;  //0.0 to 99.9
	    return randomValue <= getConfig().getProbabilidadEnvio();
	}
	
	private boolean calcularConfirmacion() {
		double randomValue = Math.random()*100;  //0.0 to 99.9
	    return randomValue <= getConfig().getProbabilidadEnvio();
	}
	
	private void cerrarFrame() {
		this.setVisible( false );
		this.dispose();
	}
	
	public ManejadorMensajesControl getMensajeria() {
		return mensajeria;
	}

	public void setMensajeria(ManejadorMensajesControl mensajeria) {
		this.mensajeria = mensajeria;
	}
	
	public JFrame getFrameListaProductos() {
		return frameListaProductos;
	}

	public void setFrameListaProductos(JFrame listaProductos) {
		this.frameListaProductos = listaProductos;
	}

	public JDialog getFrameSeleccionProducto() {
		return frameSeleccionProducto;
	}

	public void setFrameSeleccionProducto(JDialog frameSeleccionProducto) {
		this.frameSeleccionProducto = frameSeleccionProducto;
	}

	public JTable getTablaProductos() {
		return tablaProductos;
	}

	public void setTablaProductos(JTable tablaProductos) {
		this.tablaProductos = tablaProductos;
	}


	public int getCodigoProducto() {
		return codigoProducto;
	}

	public void setCodigoProducto(int codigoProducto) {
		this.codigoProducto = codigoProducto;
	}


	public Config getConfig() {
		if (config == null)
			config = new ConfigDAO().obtenerConfiguracion();
		return config;
	}


	public void setConfig(Config config) {
		this.config = config;
	}
}
