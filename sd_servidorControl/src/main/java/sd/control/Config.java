package sd.control;

public class Config {
	
	private String modo;
	private int minutosPersistencia;  // setea los minutos cada los que se persisten los datos
	private int probabilidadEnvio;
	private int probabilidadConfirmar;
	
	public Config() {}
	
	public Config( String modo, int min, int probE, int probC ) {
		this.modo = modo;
		this.minutosPersistencia = min;
		this.probabilidadEnvio = probE;
		this.probabilidadConfirmar = probC;
	}

	public String getModo() {
		return modo;
	}

	public void setModo(String modo) {
		this.modo = modo;
	}

	public int getMinutosPersistencia() {
		return minutosPersistencia;
	}

	public void setMinutosPersistencia(int minutosPersistencia) {
		this.minutosPersistencia = minutosPersistencia;
	}

	public int getProbabilidadEnvio() {
		return probabilidadEnvio;
	}

	public void setProbabilidadEnvio(int probabilidadEnvio) {
		this.probabilidadEnvio = probabilidadEnvio;
	}

	public int getProbabilidadConfirmar() {
		return probabilidadConfirmar;
	}

	public void setProbabilidadConfirmar(int probabilidadConfirmar) {
		this.probabilidadConfirmar = probabilidadConfirmar;
	}
	
}
