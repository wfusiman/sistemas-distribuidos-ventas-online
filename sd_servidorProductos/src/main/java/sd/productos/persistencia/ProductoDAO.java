package sd.productos.persistencia;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;

import sd.productos.Producto;
import sd.productos.CompraProducto;

public class ProductoDAO {

	private static final String path = System.getProperty("user.dir") + "/data/";
	private String archivoProductos;
	private String archivoCompras;
	
	public ProductoDAO() {
		this("productos.json", "compras.json");
	}
	
	public ProductoDAO( String archivo, String arch ) {
		this.setArchivoProductos(archivo);
		this.setArchivoCompras(arch);
	}
	
	public Producto obtenerProducto( int codigo ) {
		List<Producto> productos = obtenerTodosProductos();
		for (Producto prod: productos) {
			if (prod.getCodigo() == codigo)
				return prod;
		}
		return null;
	}
	
	public List<Producto> obtenerTodosProductos() {
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			if (!existeArchivo( getArchivoProductos() ))
				return null;
			Producto[] prds = mapper.readValue( new File( path+getArchivoProductos() ), Producto[].class );
			return new ArrayList<Producto>( Arrays.asList( prds ));
		} catch (IOException e) {
			System.out.println("[*][PRODUCTOS] IO Excepction: " + e.getMessage() );
			return null;
		}			
	}
	
	public int guardarProducto( Producto producto ) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			if (!existeArchivo( getArchivoProductos() ) && !crearArchivo( getArchivoProductos() ))
				return -1;
			FileOutputStream output = new FileOutputStream( path+getArchivoProductos() );
			JsonGenerator g = mapper.getFactory().createGenerator( output );
			mapper.writeValue( g,producto	 );
			return 1;
		} catch (IOException e) {
			System.out.println( "[*][PRODUCTOS] IOException: " + e.getMessage() );
			return -1;
		}
	}
	
	public int guardarProductos( List<Producto> productos ) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			if (!existeArchivo( getArchivoProductos() ) && !crearArchivo( getArchivoProductos() ))
				return -1;
			FileOutputStream output = new FileOutputStream( path+getArchivoProductos() );
			JsonGenerator g = mapper.getFactory().createGenerator( output );
			mapper.writeValue( g, productos );
			return 1;
		} catch (IOException e) {
			System.out.println( "[*][PRODUCTOS] IOException: " + e.getMessage() );
			return -1;
		}
	}
	
	public List<CompraProducto> obtenerComprasProductos() {
		ObjectMapper mapper = new ObjectMapper();
		try {
			if (!existeArchivo( getArchivoCompras() ))
				return null;
			CompraProducto[] cps = mapper.readValue( new File( path+getArchivoCompras() ), CompraProducto[].class ); 
			return new ArrayList<CompraProducto>( Arrays.asList( cps ));
		} catch (IOException ex) {
			System.out.println("[*][PRODUCTOS] Exception: " + ex.getMessage() );
			return null;
		} 
	}
	
	public int guardarComprasProductos( List<CompraProducto> comprasProductos ) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			if (!existeArchivo( getArchivoCompras() ) && !crearArchivo( getArchivoCompras() ))
				return -1;
			FileOutputStream output = new FileOutputStream( path+getArchivoCompras() );
			JsonGenerator g = mapper.getFactory().createGenerator( output );
			mapper.writeValue( g, comprasProductos );
			return 1;
		} catch (IOException e) {
			System.out.println( "[*][PRODUCTOS] Exception: " + e.getMessage() );
			return -1;
		}
	}
	
	private boolean existeArchivo( String arch ) {
		File file = new File( path+arch );
		return file.exists();
	}
	
	private boolean crearArchivo( String arch ) {
		File file = new File( path+arch );
		try {
			file.createNewFile();
			return true;
		} catch (IOException e) {
			System.out.println("[*][PRODUCTOS] IOEXception: " + e.getMessage() );
			return false;
		}
	}
	
	private String getArchivoProductos() {
		return archivoProductos;
	}

	private void setArchivoProductos(String archivo) {
		this.archivoProductos = archivo;
	}

	public String getArchivoCompras() {
		return archivoCompras;
	}

	public void setArchivoCompras(String arch) {
		this.archivoCompras = arch;
	}
	
}
