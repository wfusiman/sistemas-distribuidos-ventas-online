/**
 * ManejadorMensajes.java
 */

package sd.productos.mensajeria;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import com.rabbitmq.client.GetResponse;

import sd.productos.negocio.ServidorProductos;
import servidor.Mensaje;

public class ManejadorMensajesProductos {
	
	//private String queue;
	private Channel channel;
	private static final String EXCHANGE_NAME = "SD_COMPRASONLINE";
	private static final String HOST = "localhost";
	private static final String QUEUE_NAME = "Q_PRODUCTOS";
	
	private String consumerTag;
	private ServidorProductos servidorProductos;
	
	public ManejadorMensajesProductos( ServidorProductos sp ) {
		this.servidorProductos = sp;
	}

	/**
	 * conecta a un exchange y una cola
	 * @return
	 */
	public int conectar() {
		ConnectionFactory factory = new ConnectionFactory();
       factory.setHost( HOST );
        try {
        	Connection connection = factory.newConnection();
        	channel = connection.createChannel();
        	channel.exchangeDeclare( EXCHANGE_NAME, "topic",true );
        	channel.queueDeclare( QUEUE_NAME, true, false, false, null );
        	return 1;
        } catch (IOException | TimeoutException e) {
			System.out.println( "[*][PRODUTOS] ERROR conectando  a exchange " + EXCHANGE_NAME + " : " + e.getMessage() );
			return -1;
		}
	}
	
	/**
	 * envia el mensaje msj con topico routing key
	 * @param msj
	 * @param routingKey
	 * @return
	 */
	public int enviar( Mensaje msj, String routingKey ) {
		ObjectMapper mapper= new ObjectMapper();
		String mensaje;
		
		try {
			mensaje = mapper.writeValueAsString( msj );
		} catch (JsonProcessingException e1) {
			System.out.println("[*][PRODUCTOS] Mensajeria JSonPRocessingException...." );
			return -1;
		}
	    try {
			channel.basicPublish( EXCHANGE_NAME, routingKey, null, mensaje.getBytes("UTF-8"));
			System.out.println("[*][PRODUCTOS] --> Enviando... '" + mensaje + "' en exchange:" + EXCHANGE_NAME + ":" + routingKey );
			return 1;
		} catch (IOException e) {
			System.out.println("[*][PRODUCTOS] IOException enviar : " + e.getMessage() );
			return -1;
		}
	}
	
	/**
	 * recibe mensajes con topico *.productos
	 */
	public void recibir() {
		try {
			channel.queueBind( QUEUE_NAME, EXCHANGE_NAME, "*.productos" );
		} catch (IOException e) {
			System.out.println("[*][PRODUCTOS] IOException cola " + QUEUE_NAME + ", mensaje: " + e.getMessage() );
		}
		
		/**
		 * callback ejecutado por cada mensaje recibido
		 */
    	DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), "UTF-8");
            System.out.println("[*][PRODUCTOS] <-- Recibiendo mensaje... : " + message );
            ObjectMapper mapper = new ObjectMapper();
            Mensaje mensaje = mapper.readValue( message , Mensaje.class );
            
            servidorProductos.procesarMensaje( mensaje.getIdentificador(), mensaje.getOper(), mensaje.getMensaje(), mensaje.getTiempos() );
        };
        
        try {
        	consumerTag = channel.basicConsume( QUEUE_NAME, true, deliverCallback, consumerTag -> { });
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
	
	/**
	 * Recibe un mensaje desde la cola de Productos.
	 */
	public void recibirSingle() {
		GetResponse chResponse;
		try {
			channel.queueBind( QUEUE_NAME, EXCHANGE_NAME, "*.productos" );
		} catch (IOException e) {
			System.out.println("[*][PRODUCTOS] IOException Bind, queue: " + QUEUE_NAME + ", recibir: " + e.getMessage() );
			e.printStackTrace();
		}
        
        try {
        	chResponse = channel.basicGet( QUEUE_NAME, true );
        	if (chResponse != null) {
        		String message = new String( chResponse.getBody(), "UTF-8");
        		ObjectMapper mapper = new ObjectMapper();
                Mensaje mensaje = mapper.readValue( message , Mensaje.class );
                System.out.println("[*][PRODUCTOS] <-- Recibiendo single mensaje: " +  message );
                
                servidorProductos.procesarMensaje( mensaje.getIdentificador(),mensaje.getOper(), mensaje.getMensaje(), mensaje.getTiempos() );
        	}
		} catch (IOException e) {
			System.out.println("[*][PRODUCTOS] IOException recibirSingle: " + e.getMessage() );
		}
	}
	
	/**
	 * Detiene el hilo que procesa los mensajes entrantes
	 */
	public void detener() {
		try {
			channel.basicCancel(consumerTag);
		} catch (IOException e) {
			System.out.println("[*][PRODUCTOS] IOException : " + e.getMessage() );
		}
	}
	
	/**
	 * Cierra el exchange
	 */
	public void cerrar() {
		try {
			System.out.println("[*][PRODUCTOS] Mensajeria cerrando exchange... " );
			channel.close();
		} catch (IOException | TimeoutException e) {
			System.out.println( "[*][PRODUCTOS] Mensajeria Exception: " + e.getMessage() );
		}
	}
}
