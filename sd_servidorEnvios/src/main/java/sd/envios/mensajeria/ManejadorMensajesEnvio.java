/**
 * ManejadorMensajes.java
 */

package sd.envios.mensajeria;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import com.rabbitmq.client.GetResponse;

import sd.envios.negocio.ServidorEnvios;
import servidor.Mensaje;

public class ManejadorMensajesEnvio {
	
	private Channel channel;
	private static final String EXCHANGE_NAME = "SD_COMPRASONLINE";
	private static final String HOST = "localhost";
	private static final String QUEUE_NAME = "Q_ENVIOS";
	
	private String consumerTag;
	private ServidorEnvios servidorEnvios;
	
	public ManejadorMensajesEnvio( ServidorEnvios se ) {
		this.servidorEnvios = se;
	}

	/**
	 * Conecta a un exchange y una cola para envios
	 * @return
	 */
	public int conectar() {
		ConnectionFactory factory = new ConnectionFactory();
       factory.setHost( HOST );
        try {
        	Connection connection = factory.newConnection();
        	channel = connection.createChannel();
        	channel.exchangeDeclare( EXCHANGE_NAME, "topic",true );
        	channel.queueDeclare( QUEUE_NAME, true, false, false, null );
        	return 1;
        } catch (IOException | TimeoutException e) {
			System.out.println( "[*][ENVIOS] ERROR conectando  a exchange " + EXCHANGE_NAME + " : " + e.getMessage() );
			return -1;
		}
	}
	
	/**
	 * Envia un mensaje msj a una cola con topico routingKey
	 * @param msj
	 * @param routingKey
	 * @return
	 */
	public int enviar( Mensaje msj, String routingKey ) {
		ObjectMapper mapper= new ObjectMapper();
		String mensaje;
		
		try {
			mensaje = mapper.writeValueAsString( msj );
		} catch (JsonProcessingException e1) {
			System.out.println("[*][ENVIOS] Mensajeria JSonPRocessingException...." );
			return -1;
		}
	    try {
			channel.basicPublish( EXCHANGE_NAME, routingKey, null, mensaje.getBytes("UTF-8"));
			System.out.println("[*][ENVIOS] --> Enviado... '" + mensaje + "' en exchange:" + EXCHANGE_NAME + ":" + routingKey );
			return 1;
		} catch (IOException e) {
			System.out.println("[*][ENVIOS] IOException enviar : " + e.getMessage() );
			return -1;
		}
	}
	
	/**
	 * Recive mensajes desde la cola de pagos
	 */
	public void recibir() {
		try {
			channel.queueBind( QUEUE_NAME, EXCHANGE_NAME, "*.envios" );
		} catch (IOException e) {
			System.out.println("[*][ENVIOS] IOException cola " + QUEUE_NAME + ", recibir: " + e.getMessage() );
		}
		
		/**
		 * Funcion callback que procesa cada mensaje recibido en la cola de envios
		 */
    	DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), "UTF-8");
            System.out.println("[*][ENVIOS] <-- Recibiendo mensaje: " + message );
            ObjectMapper mapper = new ObjectMapper();
            Mensaje mensaje = mapper.readValue( message , Mensaje.class );
           
            servidorEnvios.procesarMensaje( mensaje.getIdentificador(), mensaje.getOper(), mensaje.getMensaje(), mensaje.getTiempos());
        };
        
        try {
        	consumerTag = channel.basicConsume( QUEUE_NAME, true, deliverCallback, consumerTag -> { });
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
	
	/**
	 * Recibe un mensaje desde la cola Compras.
	 */
	public void recibirSingle() {
		GetResponse chResponse;
		try {
			channel.queueBind( QUEUE_NAME, EXCHANGE_NAME, "*.envios" );
		} catch (IOException e) {
			System.out.println("[*][ENVIOS] IOException Bind, queue: " + QUEUE_NAME + ", recibir: " + e.getMessage() );
			e.printStackTrace();
		}
        
        try {
        	chResponse = channel.basicGet( QUEUE_NAME, true );
        	if (chResponse != null) {
        		String message = new String( chResponse.getBody(), "UTF-8");
        		ObjectMapper mapper = new ObjectMapper();
                Mensaje mensaje = mapper.readValue( message , Mensaje.class );
                System.out.println("[*][ENVIOS] <-- Recibiendo single mensaje: " +  message );
                
                servidorEnvios.procesarMensaje( mensaje.getIdentificador(),mensaje.getOper(), mensaje.getMensaje(), mensaje.getTiempos() );
        	}
		} catch (IOException e) {
			System.out.println("[*][ENVIOS] IOException recibirSingle: " + e.getMessage() );
		}
	}
	

	/**
	 * Detiene el proceso que recibe mensajes de la cola
	 */
	public void detener() {
		try {
			if (consumerTag != null)
				channel.basicCancel(consumerTag);
		} catch (IOException e) {
			System.out.println("[*][ENVIOS] IOException : " + e.getMessage() );
		}
	}
	
	/**
	 * Cierra el exchange
	 */
	public void cerrar() {
		try {
			System.out.println("[*][ENVIOS] Mensajeria cerrando exchange... " );
			channel.close();
		} catch (IOException | TimeoutException e) {
			System.out.println( "[*][ENVIOS] Mensajeria Exception: " + e.getMessage() );
		}
	}
	
}
