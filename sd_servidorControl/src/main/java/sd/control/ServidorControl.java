/**
 * ServidorControl.java
 */

package sd.control;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import servidor.Servidor;

import sd.control.persistencia.ConfigDAO;

public class ServidorControl {
	
	private final int PUERTO_COMPRAS = 50001; //Puerto para la conexión
	private final int PUERTO_INFRACCIONES = 50002; //Puerto para la conexión
	private final int PUERTO_PAGOS = 50003; //Puerto para la conexión
	private final int PUERTO_ENVIOS = 50004; //Puerto para la conexión
	private final int PUERTO_PRODUCTOS = 50005; //Puerto para la conexión
	
	private final String HOST = "localhost"; //Host para la conexión
	
	private Socket socketCompras; 		//Socket del servidor de compras
	private Socket socketInfracciones; 	//Socket del servidor de Infracciones
	private Socket socketPagos; 		//Socket del servidor de Pagos
	private Socket socketEnvios; 		//Socket del servidor de Envios
	private Socket socketProductos; 	//Socket del servidor de Productos
	
	private String estadoCompras;
	private String estadoInfracciones;
	private String estadoPagos;
	private String estadoEnvios;
	private String estadoProductos;

	public enum ModoServidor {AUTOMATICO, PASO_PASO};
	
	private Config config;
	
	public ServidorControl() {
		setEstadoCompras( "Desconocido");
		setEstadoEnvios("Desconocido");
		setEstadoInfracciones("Desconocido");
		setEstadoPagos("Desconocido");
		setEstadoProductos("Desconocido");
		
		ConfigDAO cdao = new ConfigDAO();
		setConfig(cdao.obtenerConfiguracion());
	}
	
	
	private String operacion( int puerto, String solicitud ) {
		try {
			System.out.println("[*][CONTROL] solicitando operacion, puerto: " + puerto + ", solicitud: " + solicitud );
			Socket sock = new Socket( HOST, puerto );
			DataOutputStream dos = new DataOutputStream( sock.getOutputStream() );
			dos.writeUTF( solicitud );
			DataInputStream dis = new DataInputStream( sock.getInputStream());
            String result =  (String) dis.readUTF();
            System.out.println("[*][CONTROL], respuesta recibida: " + result );
            sock.close();
            return result;
		} catch (IOException e) {
			System.out.println("[*][CONTROL], Excepcion Conectando a servidor ... " + e.getMessage() );
			return "sin conexion";
		}
	}
	
	public void actualizarEstados() {
		actualizarEstadoCompras();
		actualizarEstadoEnvios();
		actualizarEstadoInfracciones();
		actualizarEstadoPagos();
		actualizarEstadoProductos();
	}
	
	/* ---- SERVIDOR COMPRAS ---- */
	public void actualizarEstadoCompras() {
		setEstadoCompras( operacion( PUERTO_COMPRAS,Servidor.SERVERSTATUS ) );
	}
	
	public void iniciarServidorCompras( ModoServidor modo ) {
		if (modo.compareTo( ModoServidor.PASO_PASO) == 0)
			setEstadoCompras( operacion( PUERTO_COMPRAS, Servidor.INICIAR_PASOPASO ) );
		else 
			setEstadoCompras( operacion( PUERTO_COMPRAS, Servidor.INICIAR_AUTOMATICO ) );
	}
	
	public void detenerServidorCompras() {
		setEstadoCompras( operacion( PUERTO_COMPRAS, Servidor.DETENER ) );
	}
	
	public String obtenerEstadoCompras() {
		return operacion( PUERTO_COMPRAS, Servidor.ESTADO_ACTUAL );
	}
	
	public String obtenerUltimoEstadoCompras() {
		return operacion( PUERTO_COMPRAS, Servidor.ESTADO_ULTIMO );
	}
	
	public void persistirCompras() {
		operacion( PUERTO_COMPRAS, Servidor.PERSISTIR );
	}
	
	public void siguientePasoCompras() {
		operacion( PUERTO_COMPRAS, Servidor.SIGUIENTE);
	}
	
	public void iniciarCorteCompras() {
		operacion( PUERTO_COMPRAS, Servidor.CORTE );
	}
	/* -------------------------------*/
	
	/* ---- SERVIDOR INFRACCIONES ---- */
	public void actualizarEstadoInfracciones() {
		setEstadoInfracciones( operacion( PUERTO_INFRACCIONES, Servidor.SERVERSTATUS ) );
	}
	
	public void iniciarServidorInfracciones( ModoServidor modo ) {
		if (modo.compareTo( ModoServidor.PASO_PASO) == 0)
			setEstadoInfracciones( operacion( PUERTO_INFRACCIONES, Servidor.INICIAR_PASOPASO ) );
		else
			setEstadoInfracciones( operacion( PUERTO_INFRACCIONES, Servidor.INICIAR_AUTOMATICO ) );
	}
	
	public void detenerServidorInfracciones() {
		setEstadoInfracciones( operacion( PUERTO_INFRACCIONES, Servidor.DETENER ) );
	}
	
	public String obtenerEstadoInfracciones() {
		return operacion( PUERTO_INFRACCIONES, Servidor.ESTADO_ACTUAL );
	}
	
	public String obtenerUltimoEstadoInfracciones() {
		return operacion( PUERTO_INFRACCIONES, Servidor.ESTADO_ULTIMO );
	}
	
	public void persistirInfracciones() {
		operacion( PUERTO_INFRACCIONES, Servidor.PERSISTIR );
	}
	
	public void siguientePasoInfracciones() {
		operacion( PUERTO_INFRACCIONES, Servidor.SIGUIENTE);
	}
	
	public void iniciarCorteInfracciones() {
		operacion( PUERTO_INFRACCIONES, Servidor.CORTE );
	}
	/* -------------------------------*/

	
	/* ---- SERVIDOR ENVIOS ---- */
	public void actualizarEstadoEnvios() {
		setEstadoEnvios( operacion( PUERTO_ENVIOS,Servidor.SERVERSTATUS ) );
	}
	
	public void iniciarServidorEnvios( ModoServidor modo ) {
		if (modo.compareTo( ModoServidor.PASO_PASO) == 0)
			setEstadoEnvios( operacion( PUERTO_ENVIOS, Servidor.INICIAR_PASOPASO ) );
		else
			setEstadoEnvios( operacion( PUERTO_ENVIOS, Servidor.INICIAR_AUTOMATICO ) );
	}
	
	public void detenerServidorEnvios() {
		setEstadoEnvios( operacion( PUERTO_ENVIOS, Servidor.DETENER ) );
	}
	
	public String obtenerEstadoEnvios() {
		return operacion( PUERTO_ENVIOS, Servidor.ESTADO_ACTUAL );
	}
	
	public String obtenerUltimoEstadoEnvios() {
		return operacion( PUERTO_ENVIOS, Servidor.ESTADO_ULTIMO );
	}
	
	public void persistirEnvios() {
		operacion( PUERTO_ENVIOS, Servidor.PERSISTIR );
	}
	
	public void siguientePasoEnvios() {
		operacion( PUERTO_ENVIOS, Servidor.SIGUIENTE);
	}
	
	public void iniciarCorteEnvios() {
		operacion( PUERTO_ENVIOS, Servidor.CORTE );
	}
	/* -------------------------------*/
	
	
	/* ---- SERVIDOR PAGOS ---- */
	public void actualizarEstadoPagos() {
		setEstadoPagos( operacion( PUERTO_PAGOS,Servidor.SERVERSTATUS ) );
	}
	
	public void iniciarServidorPagos( ModoServidor modo ) {
		if (modo.compareTo( ModoServidor.PASO_PASO) == 0)
			setEstadoPagos( operacion( PUERTO_PAGOS, Servidor.INICIAR_PASOPASO ) );
		else
			setEstadoPagos( operacion( PUERTO_PAGOS, Servidor.INICIAR_AUTOMATICO ) );
	}
	
	public void detenerServidorPagos() {
		setEstadoPagos( operacion( PUERTO_PAGOS, Servidor.DETENER ) );
	}
	
	public String obtenerEstadoPagos() {
		return operacion( PUERTO_PAGOS, Servidor.ESTADO_ACTUAL );
	}
	
	public void persistirPagos() {
		operacion( PUERTO_PAGOS, Servidor.PERSISTIR );
	}
	
	public void siguientePasoPagos() {
		operacion( PUERTO_PAGOS, Servidor.SIGUIENTE);
	}
	public String obtenerUltimoEstadoPagos() {
		return operacion( PUERTO_PAGOS, Servidor.ESTADO_ULTIMO );
	}
	
	public void iniciarCortePagos() {
		operacion( PUERTO_PAGOS, Servidor.CORTE );
	}
	/* -------------------------------*/
	
	
	/* ---- SERVIDOR PRODUCTOS ---- */
	public void actualizarEstadoProductos() {
		setEstadoProductos( operacion( PUERTO_PRODUCTOS, Servidor.SERVERSTATUS ) );
	}
	
	public void iniciarServidorProductos( ModoServidor modo ) {
		if (modo.compareTo( ModoServidor.PASO_PASO) == 0)
			setEstadoProductos( operacion( PUERTO_PRODUCTOS, Servidor.INICIAR_PASOPASO ) );
		else
			setEstadoProductos( operacion( PUERTO_PRODUCTOS, Servidor.INICIAR_AUTOMATICO ) );
	}
	
	public void detenerServidorProductos() {
		setEstadoProductos( operacion( PUERTO_PRODUCTOS, Servidor.DETENER ) );
	}
	
	public String obtenerEstadoProductos() {
		return operacion( PUERTO_PRODUCTOS, Servidor.ESTADO_ACTUAL );
	}
	
	public String obtenerUltimoEstadoProductos() {
		return operacion( PUERTO_PRODUCTOS, Servidor.ESTADO_ULTIMO );
	}
	
	public String obtenerListaProductos() {
		return operacion( PUERTO_PRODUCTOS, Servidor.LISTA_PRODUCTOS );
	}
	
	public void persistirProductos() {
		operacion( PUERTO_PRODUCTOS, Servidor.PERSISTIR );
	}
	
	public void siguientePasoProductos() {
		operacion( PUERTO_PRODUCTOS, Servidor.SIGUIENTE);
	}
	
	public void iniciarCorteProductos() {
		operacion( PUERTO_PRODUCTOS, Servidor.CORTE );
	}
	/* -------------------------------*/
	
	
	public Socket getSocketProductos() {
		return socketProductos;
	}

	public void setSocketProductos(Socket socketProductos) {
		this.socketProductos = socketProductos;
	}

	public Socket getSocketCompras() {
		return socketCompras;
	}

	public void setSocketCompras(Socket socketCompras) {
		this.socketCompras = socketCompras;
	}

	public Socket getSocketInfracciones() {
		return socketInfracciones;
	}

	public void setSocketInfracciones(Socket socketInfracciones) {
		this.socketInfracciones = socketInfracciones;
	}

	public Socket getSocketPagos() {
		return socketPagos;
	}

	public void setSocketPagos(Socket socketPagos) {
		this.socketPagos = socketPagos;
	}

	public Socket getSocketEnvios() {
		return socketEnvios;
	}

	public void setSocketEnvios(Socket socketEnvios) {
		this.socketEnvios = socketEnvios;
	}

	public String getEstadoCompras() {
		return estadoCompras;
	}

	public void setEstadoCompras(String estadoCompras) {
		this.estadoCompras = estadoCompras;
	}

	public String getEstadoInfracciones() {
		return estadoInfracciones;
	}

	public void setEstadoInfracciones(String estadoInfracciones) {
		this.estadoInfracciones = estadoInfracciones;
	}

	public String getEstadoPagos() {
		return estadoPagos;
	}

	public void setEstadoPagos(String estadoPagos) {
		this.estadoPagos = estadoPagos;
	}

	public String getEstadoEnvios() {
		return estadoEnvios;
	}

	public void setEstadoEnvios(String estadoEnvios) {
		this.estadoEnvios = estadoEnvios;
	}

	public String getEstadoProductos() {
		return estadoProductos;
	}

	public void setEstadoProductos(String estadoProductos) {
		this.estadoProductos = estadoProductos;
	}


	public Config getConfig() {
		return config;
	}


	public void setConfig(Config config) {
		this.config = config;
	}

}
