/**
 * ManejadorMensajes.java
 */

package sd.infracciones.mensajeria;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import com.rabbitmq.client.GetResponse;

import sd.infracciones.negocio.ServidorInfracciones;
import servidor.Mensaje;

public class ManejadorMensajesInfraccion {
	
	private Channel channel;
	private static final String EXCHANGE_NAME = "SD_COMPRASONLINE";
	private static final String HOST = "localhost";
	private static final String QUEUE_NAME = "Q_INFRACCIONES";
	
	private String consumerTag;
	
	private ServidorInfracciones servidorInfracciones;
	
	public ManejadorMensajesInfraccion( ServidorInfracciones si ) {
		this.servidorInfracciones = si;
	}

	/**
	 * Conecta a un exchange y la cola de infracciones
	 * @return
	 */
	public int conectar() {
		ConnectionFactory factory = new ConnectionFactory();
       factory.setHost( HOST );
        try {
        	Connection connection = factory.newConnection();
        	channel = connection.createChannel();
        	channel.exchangeDeclare( EXCHANGE_NAME, "topic",true );
        	channel.queueDeclare( QUEUE_NAME, true, false, false, null );
        	return 1;
        } catch (IOException | TimeoutException e) {
			System.out.println( "[*][INFRACCIONES] ERROR conectando  a exchange " + EXCHANGE_NAME + " : " + e.getMessage() );
			e.printStackTrace();
			return -1;
		}
	}
	
	/**
	 * Envia un mensaje msj con un topico routingKey
	 * @param msj
	 * @param routingKey
	 * @return
	 */
	public int enviar( Mensaje msj, String routingKey ) {
		ObjectMapper mapper= new ObjectMapper();
		String mensaje;
		
		try {
			mensaje = mapper.writeValueAsString( msj );
		} catch (JsonProcessingException e1) {
			System.out.println("[*][INFRACCIONES] Mensajeria JSonPRocessingException...." );
			return -1;
		}
	    try {
	    	System.out.println("[*][INFRACCIONES] --> Enviando... '" + mensaje + "' en exchange:" + EXCHANGE_NAME + ":" + routingKey );
			channel.basicPublish( EXCHANGE_NAME, routingKey, null, mensaje.getBytes("UTF-8"));
			return 1;
		} catch (IOException e) {
			System.out.println("[*][INFRACCIONES] IOException enviar : " + e.getMessage() );
			return -1;
		}
	}
	
	/**
	 * Recive un mensaje desde la cola con topico *.infracciones
	 */
	public void recibir() {
		try {
			channel.queueBind( QUEUE_NAME, EXCHANGE_NAME, "*.infracciones" );
		} catch (IOException e) {
			System.out.println("[*][INFRACCIONES] IOException cola " + QUEUE_NAME + ", recibir: " + e.getMessage() );
		}
		
		/**
		 *  funcion callback que atiende los mensajes entrantes
		 */
    	DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), "UTF-8");
            System.out.println("[*][INFRACCIONES] <-- Recibiendo: " + message );
            ObjectMapper mapper = new ObjectMapper();
            Mensaje mensaje = mapper.readValue( message , Mensaje.class );
           
            servidorInfracciones.procesarMensaje( mensaje.getIdentificador(), mensaje.getOper(), mensaje.getMensaje(), mensaje.getTiempos() );
        };
        
        try {
        	consumerTag = channel.basicConsume( QUEUE_NAME, true, deliverCallback, consumerTag -> { });
		} catch (IOException e) {
			e.printStackTrace();
		}
    }

	/**
	 * Recibe un mensaje desde la cola de Infracciones.
	 */
	public void recibirSingle() {
		GetResponse chResponse;
		try {
			channel.queueBind( QUEUE_NAME, EXCHANGE_NAME, "*.infracciones" );
		} catch (IOException e) {
			System.out.println("[*][INFRACCIONES] IOException Bind, queue: " + QUEUE_NAME + ", recibir: " + e.getMessage() );
			e.printStackTrace();
		}
        
        try {
        	chResponse = channel.basicGet( QUEUE_NAME, true );
        	if (chResponse != null) {
        		String message = new String( chResponse.getBody(), "UTF-8");
        		ObjectMapper mapper = new ObjectMapper();
                Mensaje mensaje = mapper.readValue( message , Mensaje.class );
                System.out.println("[*][INFRACCIONES] <-- Recibiendo single mensaje: " +  message );
                
                servidorInfracciones.procesarMensaje( mensaje.getIdentificador(),mensaje.getOper(), mensaje.getMensaje(), mensaje.getTiempos() );
        	}
		} catch (IOException e) {
			System.out.println("[*][COMPRAS] IOException recibirSingle: " + e.getMessage() );
		}
	}
	
	/**
	 * Detiene el proceso que atiende la cola de mensajes
	 */
	public void detener() {
		try {
			channel.basicCancel(consumerTag);
		} catch (IOException e) {
			System.out.println("[*][INFRACCIONES] IOException : " + e.getMessage() );
		}
	}
	
	
	/**
	 * Cierra el exchange
	 */
	public void cerrar() {
		try {
			System.out.println("[*][INFRACCIONES] Mensajeria cerrando exchange... " );
			channel.close();
		} catch (IOException | TimeoutException e) {
			System.out.println( "[*][INFRACCIONES] Mensajeria Exception: " + e.getMessage() );
		}
	}
	
}
