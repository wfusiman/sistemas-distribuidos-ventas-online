package sd.productos.persistencia;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;


public class RelojDAO {
	private static final String path = System.getProperty("user.dir") + "/data/"; 
	private String archivo;
	
	public RelojDAO() {
		this( "relojes.json" );
	}
	
	public RelojDAO( String archivo ) {
		this.setArchivo(archivo);
	}
	
	public long[] obtenerRelojes() {
		ObjectMapper mapper = new ObjectMapper();
		try {
			if (!existeArchivo())
				guardarRelojes( new long[] {0,0,0,0,0,0} );
			return (long[])mapper.readValue( new File( getArchivo() ), long[].class );
		} catch (IOException e) {
			System.out.println( "[relojDAO] Exception " + e.getMessage() );
			return null;
		}
	}
	

	public int guardarRelojes( long[] tiempos ) {
		if (!existeArchivo() && !crearArchivo())
			return -1;
		ObjectMapper mapper = new ObjectMapper();
		try {
			mapper.writeValue(  new FileOutputStream( getArchivo() ), tiempos );
			return 1;
		} catch (IOException e) {
			System.out.println( "[config] Exception: " + e.getMessage() );
			return -1;
		}
	}
	

	private boolean crearArchivo() {
		File file = new File( getArchivo() );
		try {
			file.createNewFile();
			return true;
		} catch (IOException e) {
			System.out.println("***** IOEXception: " + e.getMessage() );
			return false;
		}
	}
	
	private boolean existeArchivo() {
		File file = new File( getArchivo() );
		return file.exists();
	}
	

	public String getArchivo() {
		return path+archivo;
	}

	public void setArchivo(String archivo) {
		this.archivo = archivo;
	}
	
	
}
