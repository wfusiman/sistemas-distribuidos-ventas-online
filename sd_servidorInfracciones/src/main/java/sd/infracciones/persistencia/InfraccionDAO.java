package sd.infracciones.persistencia;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;

import sd.infracciones.Infraccion;

public class InfraccionDAO {

	private static final String path = System.getProperty("user.dir") + "/data/";
	private String archivo;
	
	public InfraccionDAO() {
		this("data.json");
	}
	
	public InfraccionDAO( String archivo ) {
		this.setArchivo(archivo);
	}
	
	public Infraccion obtenerInfracccion( int number ) {
		List<Infraccion> infracciones = obtenerTodasInfracciones();
		for (Infraccion inf: infracciones) {
			if (inf.getNumero() == number)
				return inf;
		}
		return null;
	}
	
	public List<Infraccion> obtenerTodasInfracciones() {
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			if (!existeArchivo())
				return null;
			Infraccion[] infs = mapper.readValue( new File( path+getArchivo() ), Infraccion[].class );
			return new ArrayList<Infraccion>( Arrays.asList( infs ));
		} catch (IOException e) {
			System.out.println("[*][INFRACCION] IO Excepction:" + e.getMessage() );
			return null;
		}			
	}

	
	public int guardarInfraccion( Infraccion infraccion ) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			if (!existeArchivo())
				return -1;
			FileOutputStream output = new FileOutputStream( path+getArchivo() );
			JsonGenerator g = mapper.getFactory().createGenerator( output );
			mapper.writeValue( g,infraccion	 );
			return 1;
		} catch (IOException e) {
			System.out.println("[*][INFRACCION] IO Excepction:" + e.getMessage() );
			return -1;
		}
	}
	
	public int guardarInfracciones(List<Infraccion> infracciones ) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			if (!existeArchivo() && !crearArchivo())
				return -1;
			FileOutputStream output = new FileOutputStream( path+getArchivo() );
			JsonGenerator g = mapper.getFactory().createGenerator( output );
			mapper.writeValue( g,infracciones );
			return 1;
		} catch (IOException e) {
			System.out.println("[*][INFRACCION] IO Excepction:" + e.getMessage() );
			return -1;
		}
	}
	
	private boolean crearArchivo() {
		File file = new File( path+getArchivo() );
		try {
			file.createNewFile();
			return true;
		} catch (IOException e) {
			System.out.println("[*][INFRACCION] IOEXception: " + e.getMessage() );
			return false;
		}
	}
	
	private boolean existeArchivo() {
		File file = new File( path+getArchivo() );
		return file.exists();
	}
	
	private String getArchivo() {
		return archivo;
	}

	private void setArchivo(String archivo) {
		this.archivo = archivo;
	}
	
}
