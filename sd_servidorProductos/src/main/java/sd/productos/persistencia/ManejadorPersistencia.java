/**
 * ManejadorPersistencia.java
 */

package sd.productos.persistencia;

import sd.productos.negocio.ServidorProductos;

public class ManejadorPersistencia implements Runnable {

	private volatile Thread blinker;
	int minutos;
	ServidorProductos sp;
	
	public ManejadorPersistencia( int min, ServidorProductos sp ) {
		this.minutos = min;
		this.sp = sp;
	}
	
	public void start() {
		blinker = new Thread( this );
		System.out.println("[*][PRODUCTOS] Iniciando Manejador de persistencia..." );
		blinker.start();
	}
	
	@Override
	public void run() {
		System.out.println("[*][PRODUCTOS], Manejador de persistencia, actualizando estado cada " + minutos + " minutos" );
		Thread thisThread = Thread.currentThread();
		while (blinker == thisThread) {
			try {
				Thread.sleep( minutos * 60000 );
				sp.almacenarEstado();
			} catch (InterruptedException e) {
				System.out.println("[*][PRODUCTOS] Thread Exception " + e.getMessage() );
			}
		}
	}
	
	public void stop() {
		System.out.println("[*] Deteniendo Manejador de persistencia..." );
		blinker = null;
	}

}
