package sd.compras;

public class Config {
	
	private String modo;
	private int minutosPersistencia;  // setea los minutos cada los que se persisten los datos
	
	public Config() {}
	
	public Config( String modo, int min ) {
		this.modo = modo;
		this.minutosPersistencia = min;
	}

	public String getModo() {
		return modo;
	}

	public void setModo(String modo) {
		this.modo = modo;
	}

	public int getMinutosPersistencia() {
		return minutosPersistencia;
	}

	public void setMinutosPersistencia(int minutosPersistencia) {
		this.minutosPersistencia = minutosPersistencia;
	}
	
}
