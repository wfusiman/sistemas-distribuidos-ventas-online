/**
 * ManejadorPersistencia.java
 */

package sd.compras.persistencia;

import sd.compras.negocio.ServidorCompras;

public class ManejadorPersistencia implements Runnable {

	private volatile Thread blinker;
	int minutos;
	ServidorCompras sc;
	
	/**
	 * Crea una clase que realiza la actualizacion de la persistencia cada min minutos
	 * @param min
	 * @param sc
	 */
	public ManejadorPersistencia( int min, ServidorCompras sc ) {
		this.minutos = min;
		this.sc = sc;
	}
	
	public ManejadorPersistencia( ServidorCompras sc ) {
		this.sc = sc;
	}
	
	/**
	 * Inicia el hilo que realiza la actualizacion automatica
	 */
	public void start() {
		blinker = new Thread( this );
		System.out.println("[*][COMPRAS] Iniciando Manejador de persistencia..." );
		blinker.start();
	}
	
	@Override
	public void run() {
		System.out.println("[*][COMPRAS] Manejador de persistencia, actualizando estado cada " + minutos + " minutos" );
		Thread thisThread = Thread.currentThread();
		while (blinker == thisThread) {
			try {
				Thread.sleep( minutos * 60000 );
				sc.almacenarEstado();
			} catch (InterruptedException e) {
				System.out.println("[*][COMPRAS] Exception " + e.getMessage() );
			}	
		}
	}
	
	/**
	 * Detiene el hilo que actualiza la persistencia
	 */
	public void stop() {
		System.out.println("[*][COMPRAS] Deteniendo Manejador de persistencia..." );
		blinker = null;
	}

}
