package sd.productos;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import sd.productos.negocio.ServidorProductos;
import sd.productos.persistencia.ConfigDAO;
import sd.productos.persistencia.ManejadorPersistencia;

import servidor.Servidor;

public class ServidorControlProductos implements Servidor {
	
	private final int PUERTO = 50005;	//Puerto para la conexión
	private ServerSocket serverSocket; 	//Socket del servidor
	private Socket socket; 				//Socket 
	private DataInputStream dis;
	private DataOutputStream dos;
	
	private boolean activo;
	private boolean automatico;
	
	private Config config;
	
	ManejadorPersistencia manejadorPersistencia;
	ServidorProductos servidorProductos;
	
	public ServidorControlProductos() { 
		setActivo(false); 
		setAutomatico(true);
	}
	
	public static void main( String[] args ) {
		ServidorControlProductos sp = new ServidorControlProductos();
		sp.setup();
		sp.atender();
	}
	
	private void setup() {
		try {
			serverSocket = new ServerSocket( PUERTO );
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public int iniciar() {
		System.out.println("[*][PRODUCTOS] Iniciando Servidor ..." );
		servidorProductos = new ServidorProductos();
		if (automatico) {
			System.out.println("[*][PRODUCTOS] Modo automatico ..." );
			config = abrirConfig();
			servidorProductos.iniciarAutomatico();
			manejadorPersistencia = new ManejadorPersistencia( config.getMinutosPersistencia(), servidorProductos );
			manejadorPersistencia.start();
		}
		else { 
			System.out.println("[*][PRODUCTOS] Modo paso a paso ..." );
			config = null;
			servidorProductos.iniciarPap();
		}
		
		setActivo( true );
		return 0;
	}

	public void detener() {
        System.out.println("[*][PRODUCTOS] Deteniendo servidor ... ");
        if (automatico)
        	manejadorPersistencia.stop();
        servidorProductos.detener();
		setActivo( false );
	}

	
	public int reiniciar() {
		detener();
		iniciar();
		return 0;
	}

	public void derribar() {
		if (automatico)
			manejadorPersistencia.stop();
		
		setActivo( false );
	}

	public String estadoActual() {
		return servidorProductos.comprasProductoEstadoString();
	}

	public String estadoUltimo() {
		return servidorProductos.comprasProductoPersistidoString();
	}

	public int siguientePaso() {
		servidorProductos.recibirSiguienteMensaje();
		return 0;
	}
		
	public int persistirEstado() {
		servidorProductos.almacenarEstado();
		return 0;
	}
	
	public int iniciarCorteConsistente() {
		servidorProductos.comenzarCorteConsistente();
		return 0;
	}
	
	public String listaProductos() {
		return servidorProductos.productosToString();
	}
	
	public int setearConfig( int[] params ) {
		ConfigDAO configdao = new ConfigDAO();

		return configdao.guardarConfiguracion( new Config( (params[0] == 0) ? "automatico":"manual",params[1] ) );
	}

	
	private Config abrirConfig() {
		ConfigDAO configdao = new ConfigDAO();
		config = configdao.obtenerConfiguracion();
		return config;
	}

	public void atender() {
		while(true){
            System.out.println("[*][PRODUCTOS] Servidor ejecutando....");
            try {
				socket = serverSocket.accept();
				dis = new DataInputStream( socket.getInputStream() );
	            
	            String msjRcv = (String) dis.readUTF();
	            System.out.println("[*][PRODUCTOS] Recibido: " + msjRcv );
	           
	            String msjSnd = procesar( msjRcv );
	            
	            dos = new DataOutputStream( socket.getOutputStream());
	            dos.writeUTF( msjSnd );
	            System.out.println("[*][PRODUCTOS] Respuesta: " + msjSnd );
	            
	            dis.close();
	            dos.close();
	            socket.close();
			} catch (IOException e) {
				System.out.println("[*][PRODUCTOS], Exception atender ... " + e.getMessage() );
				e.printStackTrace();
			}
        }
	}

	/* procesar: procesa comando del que se envian mediante sockets */
	private String procesar( String msj ) {
		String []params = msj.split(" ");
		System.out.println( "[*][PRODUCTOS] Procesando pedido: " + params[0] ); 
		if (params[0].compareTo(INICIAR_AUTOMATICO) == 0) {
			automatico = true;
			iniciar();
			return "EJECUTANDO AUTOMATICO";
		}
		else if (params[0].compareTo(INICIAR_PASOPASO) == 0) {
			automatico = false;
			iniciar();
			return "EJECUTANDO PAP";
		}
		else if (params[0].compareTo(DETENER) == 0) {
			detener();
			return "DETENIDO";
		}
		else if (params[0].compareTo(REINICIAR) == 0) {
			reiniciar();
			return "REINICIADO";
		}
		else if (params[0].compareTo(DERRIBAR) == 0) {
			derribar();
			return "CAIDO";
		}
		else if (params[0].compareTo(ESTADO_ACTUAL) == 0) {
			return estadoActual();
		}
		else if (params[0].compareTo(ESTADO_ULTIMO) == 0) {
			return estadoUltimo();
		}
		else if (params[0].compareTo(LISTA_PRODUCTOS) == 0) {
			return listaProductos();
		}
		else if (params[0].compareTo(SIGUIENTE) == 0) {
			siguientePaso();
			return "OK";
		}
		else if (params[0].compareTo(PERSISTIR) == 0) {
			persistirEstado();
			return "OK";
		}
		else if (params[0].compareTo(CORTE) == 0) {
			iniciarCorteConsistente();
			return "OK";
		}
		else if (params[0].compareTo( SERVERSTATUS ) == 0) {
			return (activo) ? "EJECUTANDO " + (automatico ? "AUTOMATICO":"PASO PASO"):"DETENIDO";
		}
		else {
			// comando invalido
			return "COMANDO INVALIDO";
		}
	}
	
	
	public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}
	
	public boolean isAutomatico() {
		return automatico;
	}

	public void setAutomatico(boolean automatico) {
		this.automatico = automatico;
	}

}
