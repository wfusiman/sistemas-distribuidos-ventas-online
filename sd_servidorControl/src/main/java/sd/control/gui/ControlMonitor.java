/**
 * ControlMonitor.java
 */

package sd.control.gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.awt.event.ActionEvent;

import sd.control.Config;
import sd.control.ServidorControl;
import sd.control.mensajeria.ManejadorMensajesControl;
import sd.control.persistencia.ConfigDAO;

import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

public class ControlMonitor extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField tf_estado;
	
	private JLabel lblServidor;
	private JLabel lblEstado;
	private JLabel lblCompras;
	private JLabel lblInfracciones;
	private JLabel lblPagos;
	private JLabel lblEnvios;
	private JLabel lblProductos;
	
	private JButton btnIniciarCompras;
	private JButton btnPersistirCompras;
	private JButton btnEstadoCompras;
	private JButton btnUltimoEstadoCompras;
	private JLabel lblEstadoCompras;
	
	private JButton btnIniciarInf;
	private JButton btnPersistirInf;
	private JButton btnEstadoInf;
	private JButton btnUltimoEstadoInf;
	private JLabel lblEstadoInf;
	
	private JButton btnIniciarEnvios;
	private JButton btnPersistirEnvios;
	private JButton btnEstadoEnvios;
	private JButton btnUltimoEstadoEnvios;
	private JLabel lblEstadoEnvios;
	
	private JButton btnIniciarPagos;
	private JButton btnPersistirPagos;
	private JButton btnEstadoPagos;
	private JButton btnUltimoEstadoPagos;
	private JLabel lblEstadoPagos;
	
	private JButton btnIniciarProductos;
	private JButton btnPersistirProductos; 
	private JButton btnEstadoProductos;
	private JButton btnUltimoEstadoProductos;
	private JLabel lblEstadoProductos;
	
	private ServidorControl servControl;
	private JLabel lblModo;
	private JRadioButton rdbtnAutomatico;
	private JRadioButton rdbtnPasoAPaso;
	
	private JButton btnGenerarCompra;
	
	private ManejadorMensajesControl mensajeria;
	private Config config;
	
	private JTable tablaCompras;
	private JTable tablaEnvios;
	private JTable tablaInfracciones;
	private JTable tablaPagos;
	private JTable tablaProductos;
	
	private JDialog frameSeleccion;
	private JTable tablaServidores;
	private JButton btnSiguiente;
	
	private String listaProductos = "";
	private JButton btnCorteCompras;
	private JButton btnCorteInf;
	private JButton btnCorteEnvios;
	private JButton btnCortePagos;
	private JButton btnCorteProductos;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					System.out.println("[*][CONTROL] Iniciando control monitor....");
					ControlMonitor frame = new ControlMonitor();
					System.out.println("[*][CONTROL] Iniciando servidor de control....");
					frame.iniciarServidorControl();
					frame.setVisible(true);
					System.out.println("[*][CONTROL] Iniciando servidor de Mensajeria.....");
					frame.iniciarMensajeria();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ControlMonitor() {
		setAutoRequestFocus(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1451, 651);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		panel.setBorder(null);
		contentPane.add(panel, BorderLayout.WEST);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		JLabel lbl_estado = new JLabel("Control");
		lbl_estado.setFont(new Font("Garuda", Font.BOLD, 16));
		panel.add(lbl_estado);
		
		btnGenerarCompra = new JButton("Generar compra");
		btnGenerarCompra.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (!servControl.getEstadoCompras().contains("EJECUTANDO")) {
					JOptionPane.showMessageDialog( null, "El servidor de Compras no se esta ejecutando","Error", JOptionPane.ERROR_MESSAGE );
					return;
				}
				if (listaProductos.isEmpty())
					listaProductos = servControl.obtenerListaProductos();
				GenerarCompra nuevaCompra = new GenerarCompra( mensajeria, listaProductos );
				nuevaCompra.setModal(true);
				nuevaCompra.setVisible( true );
			}
		});
		panel.add(btnGenerarCompra);
		
		lblModo = new JLabel("Modo:");
		lblModo.setFont(new Font("Garuda", Font.BOLD, 16));
		panel.add(lblModo);
		
		rdbtnAutomatico = new JRadioButton("automatico");
		rdbtnAutomatico.addActionListener( new ActionListener() {
			public void actionPerformed( ActionEvent arg ) {
				if (rdbtnAutomatico.isSelected())
					btnSiguiente.setEnabled( false );
				else 
					btnSiguiente.setEnabled( true );
			}
		});
		rdbtnAutomatico.setSelected(true);
		rdbtnAutomatico.setFont(new Font("Garuda", Font.BOLD, 16));
		panel.add(rdbtnAutomatico);
		
		rdbtnPasoAPaso = new JRadioButton("paso a paso");
		rdbtnPasoAPaso.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (rdbtnPasoAPaso.isSelected())
					btnSiguiente.setEnabled(true);
				else 
					btnSiguiente.setEnabled(false);
			}
		});
		rdbtnPasoAPaso.setFont(new Font("Garuda", Font.BOLD, 16));
		panel.add(rdbtnPasoAPaso);
		
		ButtonGroup rbGroup = new ButtonGroup();
		rbGroup.add( rdbtnAutomatico );
		rbGroup.add( rdbtnPasoAPaso );
		
		btnSiguiente = new JButton("Siguiente paso");
		btnSiguiente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				mostrarSeleccionServidorPap();
			}
		});
		btnSiguiente.setEnabled(false);
		panel.add(btnSiguiente);
		
		tf_estado = new JTextField();
		panel.add(tf_estado);
		tf_estado.setColumns(10);
		
		
		JPanel panel_central = new JPanel();
		panel_central.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		contentPane.add( panel_central, BorderLayout.CENTER);
		GridBagLayout gbl_panel_central = new GridBagLayout();
		gbl_panel_central.columnWidths = new int[] {110, 110, 120, 120, 120};
		gbl_panel_central.rowHeights = new int[] {50, 50, 0, 50, 0, 50, 0, 50, 0, 50, 0};
		gbl_panel_central.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0};
		gbl_panel_central.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
		panel_central.setLayout(gbl_panel_central);
		
		lblServidor = new JLabel("SERVIDOR");
		GridBagConstraints gbc_lblServidor = new GridBagConstraints();
		gbc_lblServidor.anchor = GridBagConstraints.NORTH;
		gbc_lblServidor.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblServidor.insets = new Insets(0, 0, 5, 5);
		gbc_lblServidor.gridx = 0;
		gbc_lblServidor.gridy = 0;
		panel_central.add(lblServidor, gbc_lblServidor);
		
		lblEstado = new JLabel("ESTADO");
		GridBagConstraints gbc_lblEstado = new GridBagConstraints();
		gbc_lblEstado.anchor = GridBagConstraints.NORTHWEST;
		gbc_lblEstado.insets = new Insets(0, 0, 5, 5);
		gbc_lblEstado.gridx = 1;
		gbc_lblEstado.gridy = 0;
		panel_central.add(lblEstado, gbc_lblEstado);
		
		btnIniciarCompras = new JButton("iniciar");
		btnIniciarCompras.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (btnIniciarCompras.getText().compareTo("iniciar") == 0) {
					servControl.iniciarServidorCompras( getModo() );
					actualizarEstadosLabels();
				}
				else if (btnIniciarCompras.getText().compareTo("detener") == 0) {
					servControl.detenerServidorCompras();;
					actualizarEstadosLabels();
				}
			}
		});
		
		lblCompras = new JLabel("Compras");
		GridBagConstraints gbc_lblCompras = new GridBagConstraints();
		gbc_lblCompras.anchor = GridBagConstraints.WEST;
		gbc_lblCompras.insets = new Insets(0, 0, 5, 5);
		gbc_lblCompras.gridx = 0;
		gbc_lblCompras.gridy = 1;
		panel_central.add(lblCompras, gbc_lblCompras);
		
		lblEstadoCompras = new JLabel("estado");
		lblEstadoCompras.setForeground(Color.GREEN);
		GridBagConstraints gbc_lblEstadoCompras = new GridBagConstraints();
		gbc_lblEstadoCompras.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblEstadoCompras.insets = new Insets(0, 0, 5, 5);
		gbc_lblEstadoCompras.gridx = 1;
		gbc_lblEstadoCompras.gridy = 1;
		panel_central.add(lblEstadoCompras, gbc_lblEstadoCompras);
		GridBagConstraints gbc_btnIniciarCompras = new GridBagConstraints();
		gbc_btnIniciarCompras.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnIniciarCompras.insets = new Insets(0, 0, 5, 5);
		gbc_btnIniciarCompras.gridx = 2;
		gbc_btnIniciarCompras.gridy = 1;
		panel_central.add(btnIniciarCompras, gbc_btnIniciarCompras);
		
		btnPersistirCompras = new JButton("persistir");
		btnPersistirCompras.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				servControl.persistirCompras();
			}
		});
		//btnPersistirCompras.setEnabled( modo.compareTo("PASO_PASO") == 0 );
		GridBagConstraints gbc_btnPersistirCompras = new GridBagConstraints();
		gbc_btnPersistirCompras.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnPersistirCompras.insets = new Insets(0, 0, 5, 5);
		gbc_btnPersistirCompras.gridx = 3;
		gbc_btnPersistirCompras.gridy = 1;
		panel_central.add(btnPersistirCompras, gbc_btnPersistirCompras);
		
		/*
		btnMensajesCompras = new JButton("cola mensajes");
		btnMensajesCompras.setVisible(false);
		GridBagConstraints gbc_btnMensajesCompras = new GridBagConstraints();
		gbc_btnMensajesCompras.insets = new Insets(0, 0, 5, 5);
		gbc_btnMensajesCompras.gridx = 6;
		gbc_btnMensajesCompras.gridy = 1;
		panel_central.add(btnMensajesCompras, gbc_btnMensajesCompras);
		*/
		
		btnCorteCompras = new JButton("iniciar corte");
		btnCorteCompras.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				servControl.iniciarCorteCompras();
			}
		});
		GridBagConstraints gbc_btnCorteCompras = new GridBagConstraints();
		gbc_btnCorteCompras.insets = new Insets(0, 0, 5, 5);
		gbc_btnCorteCompras.gridx = 4;
		gbc_btnCorteCompras.gridy = 1;
		panel_central.add(btnCorteCompras, gbc_btnCorteCompras);
		
		btnEstadoCompras = new JButton("estado");
		btnEstadoCompras.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String estados = servControl.obtenerEstadoCompras();
				if (estados != null)
					mostrarEstadoCompras( "COMPRAS: ESTADO ACTUAL", estados ); 
			}
			
		});
		GridBagConstraints gbc_btnEstadoCompras = new GridBagConstraints();
		gbc_btnEstadoCompras.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnEstadoCompras.insets = new Insets(0, 0, 5, 5);
		gbc_btnEstadoCompras.gridx = 2;
		gbc_btnEstadoCompras.gridy = 2;
		panel_central.add(btnEstadoCompras, gbc_btnEstadoCompras);
		
		btnUltimoEstadoCompras = new JButton("ultimo estado");
		btnUltimoEstadoCompras.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String estados = servControl.obtenerUltimoEstadoCompras();
				if (estados != null)
					mostrarEstadoCompras( "COMPRAS: ESTADO PERSISTIDO", estados );
			}
		});
		GridBagConstraints gbc_btnUltimoEstadoCompras = new GridBagConstraints();
		gbc_btnUltimoEstadoCompras.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnUltimoEstadoCompras.insets = new Insets(0, 0, 5, 5);
		gbc_btnUltimoEstadoCompras.gridx = 3;
		gbc_btnUltimoEstadoCompras.gridy = 2;
		panel_central.add(btnUltimoEstadoCompras, gbc_btnUltimoEstadoCompras);
		
		lblInfracciones = new JLabel("Infracciones");
		GridBagConstraints gbc_lblInfracciones = new GridBagConstraints();
		gbc_lblInfracciones.anchor = GridBagConstraints.WEST;
		gbc_lblInfracciones.insets = new Insets(0, 0, 5, 5);
		gbc_lblInfracciones.gridx = 0;
		gbc_lblInfracciones.gridy = 3;
		panel_central.add(lblInfracciones, gbc_lblInfracciones);
		
		lblEstadoInf = new JLabel("estado");
		lblEstadoInf.setForeground(Color.GREEN);
		GridBagConstraints gbc_lblEstadoInf = new GridBagConstraints();
		gbc_lblEstadoInf.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblEstadoInf.insets = new Insets(0, 0, 5, 5);
		gbc_lblEstadoInf.gridx = 1;
		gbc_lblEstadoInf.gridy = 3;
		panel_central.add(lblEstadoInf, gbc_lblEstadoInf);
		
		btnIniciarInf = new JButton("iniciar");
		btnIniciarInf.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (btnIniciarInf.getText().compareTo("iniciar") == 0) {
					servControl.iniciarServidorInfracciones( getModo() );
					actualizarEstadosLabels();
				}
				else if (btnIniciarInf.getText().compareTo("detener") == 0) {
					servControl.detenerServidorInfracciones();
					actualizarEstadosLabels();
				}
			}
		});
		GridBagConstraints gbc_btnIniciarInf = new GridBagConstraints();
		gbc_btnIniciarInf.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnIniciarInf.insets = new Insets(0, 0, 5, 5);
		gbc_btnIniciarInf.gridx = 2;
		gbc_btnIniciarInf.gridy = 3;
		panel_central.add(btnIniciarInf, gbc_btnIniciarInf);
		
		btnPersistirInf = new JButton("persistir");
		btnPersistirInf.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				servControl.persistirInfracciones();
			}
		});
		btnPersistirInf.setEnabled(false);
		GridBagConstraints gbc_btnPersistirInf = new GridBagConstraints();
		gbc_btnPersistirInf.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnPersistirInf.insets = new Insets(0, 0, 5, 5);
		gbc_btnPersistirInf.gridx = 3;
		gbc_btnPersistirInf.gridy = 3;
		panel_central.add(btnPersistirInf, gbc_btnPersistirInf);
		
		/*
		btnMensajesInf = new JButton("cola mensajes");
		btnMensajesInf.setVisible(false);
		GridBagConstraints gbc_btnMensajesInf = new GridBagConstraints();
		gbc_btnMensajesInf.insets = new Insets(0, 0, 5, 5);
		gbc_btnMensajesInf.gridx = 6;
		gbc_btnMensajesInf.gridy = 2;
		panel_central.add(btnMensajesInf, gbc_btnMensajesInf);
		*/
		
		btnUltimoEstadoInf = new JButton("ultimo estado");
		btnUltimoEstadoInf.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//solicitarEstado("control.infracciones");
				String infs = servControl.obtenerUltimoEstadoInfracciones();
				mostrarEstadoInfracciones( "INFRACCIONES: ULTIMO ESTADO PERSISTIDO", infs );
			}
		});
		GridBagConstraints gbc_btnUltimoEstadoInf = new GridBagConstraints();
		gbc_btnUltimoEstadoInf.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnUltimoEstadoInf.insets = new Insets(0, 0, 5, 5);
		gbc_btnUltimoEstadoInf.gridx = 3;
		gbc_btnUltimoEstadoInf.gridy = 4;
		panel_central.add(btnUltimoEstadoInf, gbc_btnUltimoEstadoInf);
		
		btnCorteInf = new JButton("iniciar corte");
		btnCorteInf.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				servControl.iniciarCorteInfracciones();
			}
		});
		GridBagConstraints gbc_btnCorteInf = new GridBagConstraints();
		gbc_btnCorteInf.insets = new Insets(0, 0, 5, 5);
		gbc_btnCorteInf.gridx = 4;
		gbc_btnCorteInf.gridy = 3;
		panel_central.add(btnCorteInf, gbc_btnCorteInf);
		
		btnEstadoInf = new JButton("estado");
		btnEstadoInf.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String infs = servControl.obtenerEstadoInfracciones();
				//solicitarEstado("control.infracciones");
				mostrarEstadoInfracciones( "INFRACCINOES: ESTADO ACTUAL", infs );
			}
		});
		GridBagConstraints gbc_btnEstadoInf = new GridBagConstraints();
		gbc_btnEstadoInf.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnEstadoInf.insets = new Insets(0, 0, 5, 5);
		gbc_btnEstadoInf.gridx = 2;
		gbc_btnEstadoInf.gridy = 4;
		panel_central.add(btnEstadoInf, gbc_btnEstadoInf);
		
		lblEnvios = new JLabel("Envios");
		GridBagConstraints gbc_lblEnvios = new GridBagConstraints();
		gbc_lblEnvios.anchor = GridBagConstraints.WEST;
		gbc_lblEnvios.insets = new Insets(0, 0, 5, 5);
		gbc_lblEnvios.gridx = 0;
		gbc_lblEnvios.gridy = 5;
		panel_central.add(lblEnvios, gbc_lblEnvios);
		
		btnIniciarEnvios = new JButton("iniciar");
		btnIniciarEnvios.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (btnIniciarEnvios.getText().compareTo("iniciar") == 0) {
					servControl.iniciarServidorEnvios( getModo() );
					actualizarEstadosLabels();
				}
				else if (btnIniciarEnvios.getText().compareTo("detener") == 0) {
					servControl.detenerServidorEnvios();
					actualizarEstadosLabels();
				}
			}
		});
		
		lblEstadoEnvios = new JLabel("estado");
		lblEstadoEnvios.setForeground(Color.GREEN);
		GridBagConstraints gbc_lblEstadoEnvios = new GridBagConstraints();
		gbc_lblEstadoEnvios.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblEstadoEnvios.insets = new Insets(0, 0, 5, 5);
		gbc_lblEstadoEnvios.gridx = 1;
		gbc_lblEstadoEnvios.gridy = 5;
		panel_central.add(lblEstadoEnvios, gbc_lblEstadoEnvios);
		GridBagConstraints gbc_btnIniciarEnvios = new GridBagConstraints();
		gbc_btnIniciarEnvios.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnIniciarEnvios.insets = new Insets(0, 0, 5, 5);
		gbc_btnIniciarEnvios.gridx = 2;
		gbc_btnIniciarEnvios.gridy = 5;
		panel_central.add(btnIniciarEnvios, gbc_btnIniciarEnvios);
		
		btnPersistirEnvios = new JButton("persistir");
		btnPersistirEnvios.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				servControl.persistirEnvios();
			}
		});
		btnPersistirEnvios.setEnabled(false);
		GridBagConstraints gbc_btnPersistirEnvios = new GridBagConstraints();
		gbc_btnPersistirEnvios.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnPersistirEnvios.insets = new Insets(0, 0, 5, 5);
		gbc_btnPersistirEnvios.gridx = 3;
		gbc_btnPersistirEnvios.gridy = 5;
		panel_central.add(btnPersistirEnvios, gbc_btnPersistirEnvios);
		
		/*
		btnMensajesEnvios = new JButton("cola mensajes");
		btnMensajesEnvios.setVisible(false);
		GridBagConstraints gbc_btnMensajesEnvios = new GridBagConstraints();
		gbc_btnMensajesEnvios.insets = new Insets(0, 0, 5, 5);
		gbc_btnMensajesEnvios.gridx = 6;
		gbc_btnMensajesEnvios.gridy = 3;
		panel_central.add(btnMensajesEnvios, gbc_btnMensajesEnvios);
		*/
		
		btnUltimoEstadoEnvios = new JButton("ultimo estado");
		btnUltimoEstadoEnvios.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//solicitarEstado("control.envios");
				String envios = servControl.obtenerUltimoEstadoEnvios();
				mostrarEstadoEnvios( "ENVIOS: ULTIMO ESTADO PERSISTIDO",envios );
			}
		});
		GridBagConstraints gbc_btnUltimoEstadoEnvios = new GridBagConstraints();
		gbc_btnUltimoEstadoEnvios.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnUltimoEstadoEnvios.insets = new Insets(0, 0, 5, 5);
		gbc_btnUltimoEstadoEnvios.gridx = 3;
		gbc_btnUltimoEstadoEnvios.gridy = 6;
		panel_central.add(btnUltimoEstadoEnvios, gbc_btnUltimoEstadoEnvios);
		
		btnCorteEnvios = new JButton("iniciar corte");
		btnCorteEnvios.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				servControl.iniciarCorteEnvios();
			}
		});
		GridBagConstraints gbc_btnCorteEnvios = new GridBagConstraints();
		gbc_btnCorteEnvios.insets = new Insets(0, 0, 5, 5);
		gbc_btnCorteEnvios.gridx = 4;
		gbc_btnCorteEnvios.gridy = 5;
		panel_central.add(btnCorteEnvios, gbc_btnCorteEnvios);
		
		btnEstadoEnvios = new JButton("estado");
		btnEstadoEnvios.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//solicitarEstado("control.envios");
				String envios = servControl.obtenerEstadoEnvios();
				mostrarEstadoEnvios( "ENVIOS: ESTADO ACTUAL",envios );
			}
		});
		GridBagConstraints gbc_btnEstadoEnvios = new GridBagConstraints();
		gbc_btnEstadoEnvios.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnEstadoEnvios.insets = new Insets(0, 0, 5, 5);
		gbc_btnEstadoEnvios.gridx = 2;
		gbc_btnEstadoEnvios.gridy = 6;
		panel_central.add(btnEstadoEnvios, gbc_btnEstadoEnvios);
		
		lblPagos = new JLabel("Pagos");
		GridBagConstraints gbc_lblPagos = new GridBagConstraints();
		gbc_lblPagos.anchor = GridBagConstraints.WEST;
		gbc_lblPagos.insets = new Insets(0, 0, 5, 5);
		gbc_lblPagos.gridx = 0;
		gbc_lblPagos.gridy = 7;
		panel_central.add(lblPagos, gbc_lblPagos);
		
		lblEstadoPagos = new JLabel("estado");
		lblEstadoPagos.setForeground(Color.GREEN);
		GridBagConstraints gbc_lblEstadoPagos = new GridBagConstraints();
		gbc_lblEstadoPagos.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblEstadoPagos.insets = new Insets(0, 0, 5, 5);
		gbc_lblEstadoPagos.gridx = 1;
		gbc_lblEstadoPagos.gridy = 7;
		panel_central.add(lblEstadoPagos, gbc_lblEstadoPagos);
		
		btnIniciarPagos = new JButton("iniciar");
		btnIniciarPagos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (btnIniciarPagos.getText().compareTo("iniciar") == 0) {
					servControl.iniciarServidorPagos( getModo() );
					actualizarEstadosLabels();
				}
				else if (btnIniciarPagos.getText().compareTo("detener") == 0) {
					servControl.detenerServidorPagos();
					actualizarEstadosLabels();
				}
			}
		});
		GridBagConstraints gbc_btnIniciarPagos = new GridBagConstraints();
		gbc_btnIniciarPagos.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnIniciarPagos.insets = new Insets(0, 0, 5, 5);
		gbc_btnIniciarPagos.gridx = 2;
		gbc_btnIniciarPagos.gridy = 7;
		panel_central.add(btnIniciarPagos, gbc_btnIniciarPagos);
		
		btnPersistirPagos = new JButton("persistir");
		btnPersistirPagos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				servControl.persistirPagos();
			}
		});
		btnPersistirPagos.setEnabled(false);
		GridBagConstraints gbc_btnPersistirPagos = new GridBagConstraints();
		gbc_btnPersistirPagos.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnPersistirPagos.insets = new Insets(0, 0, 5, 5);
		gbc_btnPersistirPagos.gridx = 3;
		gbc_btnPersistirPagos.gridy = 7;
		panel_central.add(btnPersistirPagos, gbc_btnPersistirPagos);
		
		/*
		btnMensajesPagos = new JButton("cola mensajes");
		btnMensajesPagos.setVisible(false);
		GridBagConstraints gbc_btnMensajesPagos = new GridBagConstraints();
		gbc_btnMensajesPagos.insets = new Insets(0, 0, 5, 5);
		gbc_btnMensajesPagos.gridx = 6;
		gbc_btnMensajesPagos.gridy = 4;
		panel_central.add(btnMensajesPagos, gbc_btnMensajesPagos);
		*/
		
		btnUltimoEstadoPagos = new JButton("ultimo estado");
		btnUltimoEstadoPagos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//solicitarEstado("control.pagos");
				String pagos = servControl.obtenerUltimoEstadoPagos();
				mostrarEstadoPagos( "PAGOS: ULTIMO ESTADO PERSISTIDO",pagos );
			}
		});
		GridBagConstraints gbc_btnUltimoEstadoPagos = new GridBagConstraints();
		gbc_btnUltimoEstadoPagos.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnUltimoEstadoPagos.insets = new Insets(0, 0, 5, 5);
		gbc_btnUltimoEstadoPagos.gridx = 3;
		gbc_btnUltimoEstadoPagos.gridy = 8;
		panel_central.add(btnUltimoEstadoPagos, gbc_btnUltimoEstadoPagos);
		
		btnCortePagos = new JButton("iniciar corte");
		btnCortePagos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				servControl.iniciarCortePagos();
			}
		});
		GridBagConstraints gbc_btnCortePagos = new GridBagConstraints();
		gbc_btnCortePagos.insets = new Insets(0, 0, 5, 5);
		gbc_btnCortePagos.gridx = 4;
		gbc_btnCortePagos.gridy = 7;
		panel_central.add(btnCortePagos, gbc_btnCortePagos);
		
		btnEstadoPagos = new JButton("estado");
		btnEstadoPagos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//solicitarEstado("control.pagos");
				String pagos = servControl.obtenerEstadoPagos();
				mostrarEstadoPagos( "PAGOS: ESTADO ACTUAL", pagos );
			}
		});
		GridBagConstraints gbc_btnEstadoPagos = new GridBagConstraints();
		gbc_btnEstadoPagos.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnEstadoPagos.insets = new Insets(0, 0, 5, 5);
		gbc_btnEstadoPagos.gridx = 2;
		gbc_btnEstadoPagos.gridy = 8;
		panel_central.add(btnEstadoPagos, gbc_btnEstadoPagos);
		
		lblProductos = new JLabel("Productos");
		GridBagConstraints gbc_lblProductos = new GridBagConstraints();
		gbc_lblProductos.anchor = GridBagConstraints.WEST;
		gbc_lblProductos.insets = new Insets(0, 0, 5, 5);
		gbc_lblProductos.gridx = 0;
		gbc_lblProductos.gridy = 9;
		panel_central.add(lblProductos, gbc_lblProductos);
		
		btnIniciarProductos = new JButton("iniciar");
		btnIniciarProductos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (btnIniciarProductos.getText().compareTo("iniciar") == 0) {
					servControl.iniciarServidorProductos( getModo() );
					actualizarEstadosLabels();
				}
				else if (btnIniciarProductos.getText().compareTo("detener") == 0) {
					servControl.detenerServidorProductos();
					actualizarEstadosLabels();
				}
			}
		});
		
		lblEstadoProductos = new JLabel("estado");
		lblEstadoProductos.setForeground(Color.GREEN);
		GridBagConstraints gbc_lblEstadoProductos = new GridBagConstraints();
		gbc_lblEstadoProductos.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblEstadoProductos.insets = new Insets(0, 0, 5, 5);
		gbc_lblEstadoProductos.gridx = 1;
		gbc_lblEstadoProductos.gridy = 9;
		panel_central.add(lblEstadoProductos, gbc_lblEstadoProductos);
		GridBagConstraints gbc_btnIniciarProductos = new GridBagConstraints();
		gbc_btnIniciarProductos.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnIniciarProductos.insets = new Insets(0, 0, 5, 5);
		gbc_btnIniciarProductos.gridx = 2;
		gbc_btnIniciarProductos.gridy = 9;
		panel_central.add(btnIniciarProductos, gbc_btnIniciarProductos);
		
		btnPersistirProductos = new JButton("persistir");
		btnPersistirProductos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				servControl.persistirProductos();
			}
		});
		btnPersistirProductos.setEnabled(false);
		GridBagConstraints gbc_btnPersistirProductos = new GridBagConstraints();
		gbc_btnPersistirProductos.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnPersistirProductos.insets = new Insets(0, 0, 5, 5);
		gbc_btnPersistirProductos.gridx = 3;
		gbc_btnPersistirProductos.gridy = 9;
		panel_central.add(btnPersistirProductos, gbc_btnPersistirProductos);
		
		/*
		btnMensajesProductos = new JButton("cola mensajes");
		btnMensajesProductos.setVisible(false);
		GridBagConstraints gbc_btnMensajesProductos = new GridBagConstraints();
		gbc_btnMensajesProductos.insets = new Insets(0, 0, 0, 5);
		gbc_btnMensajesProductos.gridx = 6;
		gbc_btnMensajesProductos.gridy = 5;
		panel_central.add(btnMensajesProductos, gbc_btnMensajesProductos);
		*/
		
		btnUltimoEstadoProductos = new JButton("ultimo estado");
		btnUltimoEstadoProductos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//solicitarEstado("control.productos");
				String comprasProds = servControl.obtenerUltimoEstadoProductos();
				mostrarEstadoProductos( "PRODUCTOS: ULTIMO ESTADO PERSISTIDO",comprasProds );
			}
		});
		GridBagConstraints gbc_btnUltimoEstadoProductos = new GridBagConstraints();
		gbc_btnUltimoEstadoProductos.insets = new Insets(0, 0, 5, 5);
		gbc_btnUltimoEstadoProductos.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnUltimoEstadoProductos.gridx = 3;
		gbc_btnUltimoEstadoProductos.gridy = 10;
		panel_central.add(btnUltimoEstadoProductos, gbc_btnUltimoEstadoProductos);
		
		btnCorteProductos = new JButton("iniciar corte");
		btnCorteProductos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				servControl.iniciarCorteProductos();
			}
		});
		GridBagConstraints gbc_btnCorteProductos = new GridBagConstraints();
		gbc_btnCorteProductos.insets = new Insets(0, 0, 5, 5);
		gbc_btnCorteProductos.gridx = 4;
		gbc_btnCorteProductos.gridy = 9;
		panel_central.add(btnCorteProductos, gbc_btnCorteProductos);
		
		btnEstadoProductos = new JButton("estado");
		btnEstadoProductos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//solicitarEstado("control.productos");
				String comprasProds = servControl.obtenerEstadoProductos();
				mostrarEstadoProductos( "PRODUCTOS: ESTADO ACTUAL",comprasProds );
			}
		});
		GridBagConstraints gbc_btnEstadoProductos = new GridBagConstraints();
		gbc_btnEstadoProductos.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnEstadoProductos.insets = new Insets(0, 0, 0, 5);
		gbc_btnEstadoProductos.gridx = 2;
		gbc_btnEstadoProductos.gridy = 10;
		panel_central.add(btnEstadoProductos, gbc_btnEstadoProductos);
		
		JLabel lblNewLabel = new JLabel("Control y Monitor Sistemas de compras online");
		lblNewLabel.setFont(new Font("Garuda", Font.BOLD, 24));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lblNewLabel, BorderLayout.NORTH);
	}
	
	public ServidorControl.ModoServidor getModo() {
		if (rdbtnAutomatico.isSelected())
			return ServidorControl.ModoServidor.AUTOMATICO;
		return ServidorControl.ModoServidor.PASO_PASO;
	}
	
	public void iniciarServidorControl() {
		servControl = new ServidorControl();
		servControl.actualizarEstados();
		actualizarEstadosLabels();
	}
	
	private void actualizarEstadosLabels() {
		lblEstadoCompras.setText( servControl.getEstadoCompras() ); 
		if (lblEstadoCompras.getText().contains( "EJECUTANDO" )) {
			lblEstadoCompras.setForeground( Color.green );
			btnIniciarCompras.setText("detener");
			if (lblEstadoCompras.getText().contains("AUTOMATICO")) 
				btnPersistirCompras.setEnabled( false );
			else
				btnPersistirCompras.setEnabled( true );
			btnEstadoCompras.setEnabled(true);
			//btnMensajesCompras.setEnabled(true);
			btnUltimoEstadoCompras.setEnabled(true);
		}
		else if (lblEstadoCompras.getText().compareToIgnoreCase( "detenido" ) == 0) {
			lblEstadoCompras.setForeground( Color.BLUE);
			btnIniciarCompras.setText("iniciar");
			btnPersistirCompras.setEnabled( false );
			btnEstadoCompras.setEnabled(false);
			//btnMensajesCompras.setEnabled(false);
			btnUltimoEstadoCompras.setEnabled(false);
		}
		else {
			lblEstadoCompras.setForeground( Color.RED);
			btnIniciarCompras.setText("iniciar");
			btnPersistirCompras.setEnabled( false );
			btnEstadoCompras.setEnabled(false);
			//btnMensajesCompras.setEnabled(false);
			btnUltimoEstadoCompras.setEnabled(false);
		}
			
		lblEstadoInf.setText( servControl.getEstadoInfracciones() );
		if (lblEstadoInf.getText().contains( "EJECUTANDO" )) {
			lblEstadoInf.setForeground( Color.GREEN );
			btnIniciarInf.setText("detener");
			if (lblEstadoInf.getText().contains( "AUTOMATICO" ))
				btnPersistirInf.setEnabled(false);
			else
				btnPersistirInf.setEnabled(true);
			btnEstadoInf.setEnabled(true);
			//btnMensajesInf.setEnabled(true);
			btnUltimoEstadoInf.setEnabled(true);
		}
		else if (lblEstadoInf.getText().compareToIgnoreCase( "detenido" ) == 0) {
			lblEstadoInf.setForeground( Color.BLUE );
			btnIniciarInf.setText("iniciar");
			btnPersistirInf.setEnabled(false);
			btnEstadoInf.setEnabled(false);
			//btnMensajesInf.setEnabled(false);
			btnUltimoEstadoInf.setEnabled(false);
		}
		else {
			lblEstadoInf.setForeground( Color.RED);
			btnIniciarInf.setText("iniciar");
			btnPersistirInf.setEnabled(false);
			btnEstadoInf.setEnabled(false);
			// btnMensajesInf.setEnabled(false);
			btnUltimoEstadoInf.setEnabled(false);
		}
		
		lblEstadoEnvios.setText( servControl.getEstadoEnvios() );
		if (lblEstadoEnvios.getText().contains( "EJECUTANDO" )) {
			lblEstadoEnvios.setForeground( Color.GREEN );
			btnIniciarEnvios.setText("detener");
			if (lblEstadoEnvios.getText().contains( "AUTOMATICO"))
				btnPersistirEnvios.setEnabled( false );
			else
				btnPersistirEnvios.setEnabled( true );
			btnEstadoEnvios.setEnabled( true );
			// btnMensajesEnvios.setEnabled(true);
			btnUltimoEstadoEnvios.setEnabled(true);
		}
		else if ( lblEstadoEnvios.getText().compareToIgnoreCase( "detenido" ) == 0) {
			lblEstadoEnvios.setForeground( Color.BLUE );
			btnIniciarEnvios.setText("iniciar");
			btnPersistirEnvios.setEnabled( false );
			btnEstadoEnvios.setEnabled( false );
			// btnMensajesEnvios.setEnabled(false);
			btnUltimoEstadoEnvios.setEnabled(false);
		}
		else {
			lblEstadoEnvios.setForeground( Color.RED);
			btnIniciarEnvios.setText("iniciar");
			btnPersistirEnvios.setEnabled( false );
			btnEstadoEnvios.setEnabled( false );
			// btnMensajesEnvios.setEnabled(false);
			btnUltimoEstadoEnvios.setEnabled(false);
		}
		
		lblEstadoPagos.setText( servControl.getEstadoPagos() );
		if (lblEstadoPagos.getText().contains( "EJECUTANDO" )) {
			lblEstadoPagos.setForeground( Color.GREEN );
			btnIniciarPagos.setText("detener");
			if (lblEstadoPagos.getText().contains( "AUTOMATICO" )) 
				btnPersistirPagos.setEnabled(false);
			else
				btnPersistirPagos.setEnabled(true);
			btnEstadoPagos.setEnabled(true);
			// btnMensajesPagos.setEnabled(true);
			btnUltimoEstadoPagos.setEnabled(true);
		}
		else if (lblEstadoPagos.getText().compareToIgnoreCase( "detenido" ) == 0) {
			lblEstadoPagos.setForeground( Color.BLUE );
			btnIniciarPagos.setText("iniciar");
			btnPersistirPagos.setEnabled(false);
			btnEstadoPagos.setEnabled(false);
			// btnMensajesPagos.setEnabled(false);
			btnUltimoEstadoPagos.setEnabled(false);
		}
		else { 
			lblEstadoPagos.setForeground( Color.RED);
			btnIniciarPagos.setText("iniciar");
			btnPersistirPagos.setEnabled(false);
			btnEstadoPagos.setEnabled(false);
			// btnMensajesPagos.setEnabled(false);
			btnUltimoEstadoPagos.setEnabled(false);
		}
		
		lblEstadoProductos.setText( servControl.getEstadoProductos() );
		if (lblEstadoProductos.getText().contains( "EJECUTANDO" )) {
			lblEstadoProductos.setForeground( Color.GREEN );
			btnIniciarProductos.setText("detener");
			if (lblEstadoProductos.getText().contains( "AUTOMATICO" ))
				btnPersistirProductos.setEnabled( false);
			else
				btnPersistirProductos.setEnabled( true);
			btnEstadoProductos.setEnabled(true);
			// btnMensajesProductos.setEnabled(true);
			btnUltimoEstadoProductos.setEnabled(true);
		}
		else if (lblEstadoProductos.getText().compareToIgnoreCase( "detenido" ) == 0) {
			lblEstadoProductos.setForeground( Color.BLUE );
			btnIniciarProductos.setText("iniciar");
			btnPersistirProductos.setEnabled(false);
			btnEstadoProductos.setEnabled(false);
			// btnMensajesProductos.setEnabled(false);
			btnUltimoEstadoProductos.setEnabled(false);
		}
		else { 
			lblEstadoProductos.setForeground( Color.RED);
			btnIniciarProductos.setText("iniciar");
			btnPersistirProductos.setEnabled(false);
			btnEstadoProductos.setEnabled(false);
			// btnMensajesProductos.setEnabled(false);
			btnUltimoEstadoProductos.setEnabled(false);
		}
		
	}
	
	/*
	private void solicitarEstado( String routing ) {
		mensajeria.setControlm( this );
		Mensaje msj = new Mensaje( Mensaje.Operacion.ESTADO_ACTUAL, "" );
		mensajeria.enviar(msj,routing );
	}
	*/
	
	/*
	private void solicitarUltimoEstado( String routing ) {
		mensajeria.setControlm( this );
		Mensaje msj = new Mensaje( Mensaje.Operacion.ESTADO_ULTIMO, "" );
		mensajeria.enviar(msj,routing );
	}
	*/
	
	
	
	/**
	 * Muesta pantalla con estado de Servidor Compras
	 * @param titulo
	 * @param compras
	 */
	private void mostrarEstadoCompras( String titulo, String compras ) {
		JDialog frameEstadoCompras = new JDialog( this );
		frameEstadoCompras.getContentPane().setLayout( new BorderLayout() );
		frameEstadoCompras.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		frameEstadoCompras.setTitle( titulo );
		
		String[][] datas = {};
        String[] columnNames = new String[] { "numero","producto","comprador","estado", "envio","confirmada" }; 
        
        DefaultTableModel dtm = new DefaultTableModel( datas, columnNames ) {
			private static final long serialVersionUID = 1L;

			public boolean isCellEditable( int row, int column ) {
                return false;
            }
        };    
        try {
        	dtm.setRowCount(0);
        	JsonNode node = new ObjectMapper().readTree( compras );
        	for (JsonNode compra: node) {
        		String[] data = new String[6];
    			data[0] = Integer.toString( compra.get("numero").asInt() );
    	    	data[1] = Integer.toString( compra.get("producto").asInt() );
    	   		data[2] = compra.get("comprador").asText();
    	   		data[3] = compra.get("estado").asText();
    	   		data[4] = (compra.get("envio").asBoolean() ? "ENVIAR":"RETIRA");
    	   		data[5] = (compra.get("confirmada").asBoolean() ? "CONFIRMADA":"RECHAZADA" );
    	   		dtm.addRow( data );
			}
        }
        catch(IOException e) {
        	System.out.println("[*][CONTROL] IOEXception: " + e.getMessage() );
        }
        
		tablaCompras = new JTable( dtm );
		tablaCompras.setFont( new Font("Garuda", Font.PLAIN, 16) );
		JScrollPane sp = new JScrollPane( tablaCompras );
		
		frameEstadoCompras.getContentPane().add( sp, BorderLayout.CENTER );
		frameEstadoCompras.setBounds( this.getBounds() );
		frameEstadoCompras.setModal(true);
		frameEstadoCompras.setVisible( true );
	}
	
	/**
	 * Muestra pantalla con estado de servidor Infracciones
	 * @param titulo
	 * @param infracciones
	 */
	private void mostrarEstadoInfracciones( String titulo, String infracciones ) {
		JDialog frameEstadoInfr = new JDialog( this );
		frameEstadoInfr.getContentPane().setLayout( new BorderLayout() );
		frameEstadoInfr.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		frameEstadoInfr.setTitle( titulo );
		
		String[][] datas = {};
        String[] columnNames = new String[] { "numero","compra","estado"}; 
        
        DefaultTableModel dtm = new DefaultTableModel( datas, columnNames ) {
			private static final long serialVersionUID = 1L;

			public boolean isCellEditable( int row, int column ) {
                return false;
            }
        };
        try {
        	dtm.setRowCount(0);
        	JsonNode node = new ObjectMapper().readTree( infracciones );
        	for (JsonNode inf: node) {
        		String[] data = new String[6];
    			data[0] = Integer.toString( inf.get("numero").asInt() );
    	    	data[1] = Integer.toString( inf.get("compra").asInt() );
    	   		data[2] = inf.get("estado").asText();
    	   		dtm.addRow( data );
			}
        }
        catch(IOException e) {
        	System.out.println("[*][CONTROL] IOEXception: " + e.getMessage() );
        }
        
		tablaInfracciones = new JTable( dtm );
		tablaInfracciones.setFont( new Font("Garuda", Font.PLAIN, 16) );
		JScrollPane sp = new JScrollPane( tablaInfracciones );
		
		frameEstadoInfr.getContentPane().add( sp, BorderLayout.CENTER );
		frameEstadoInfr.setBounds( this.getBounds() );
		frameEstadoInfr.setModal(true);
		frameEstadoInfr.setVisible( true );
	}
	
	/**
	 * Muesta pantalla con estado de servidor Envios
	 * @param titulo
	 */
	private void mostrarEstadoEnvios( String titulo, String envios ) {
		JDialog frameEstadoEnvios = new JDialog( this );
		frameEstadoEnvios.getContentPane().setLayout( new BorderLayout() );
		frameEstadoEnvios.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		frameEstadoEnvios.setTitle( titulo );
		
		String[][] datas = {};
        String[] columnNames = new String[] { "numero","compra","estado" }; 
        
        DefaultTableModel dtm = new DefaultTableModel( datas, columnNames ) {
			private static final long serialVersionUID = 1L;

			public boolean isCellEditable( int row, int column ) {
                return false;
            }
        };
        try {
        	dtm.setRowCount(0);
        	JsonNode node = new ObjectMapper().readTree( envios );
        	for (JsonNode envio: node) {
        		String[] data = new String[6];
    			data[0] = Integer.toString( envio.get("numero").asInt() );
    	    	data[1] = Integer.toString( envio.get("compra").asInt() );
    	   		data[2] = envio.get("estado").asText();
    	   		dtm.addRow( data );
			}
        }
        catch(IOException e) {
        	System.out.println("[*][CONTROL] IOEXception: " + e.getMessage() );
        }
		tablaEnvios = new JTable( dtm );
		tablaEnvios.setFont( new Font("Garuda", Font.PLAIN, 16) );
		JScrollPane sp = new JScrollPane( tablaEnvios );
		
		frameEstadoEnvios.getContentPane().add( sp, BorderLayout.CENTER );
		frameEstadoEnvios.setBounds( this.getBounds() );
		frameEstadoEnvios.setModal(true);
		frameEstadoEnvios.setVisible( true );
	}
	
	/**
	 * Muesta pantalla con estado de servidor Pagos
	 * @param titulo
	 */
	private void mostrarEstadoPagos( String titulo, String pagos ) {
		JDialog frameEstadoPagos = new JDialog( this );
		frameEstadoPagos.getContentPane().setLayout( new BorderLayout() );
		frameEstadoPagos.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		frameEstadoPagos.setTitle( titulo );
		
		String[][] datas = {};
        String[] columnNames = new String[] { "numero","compra","autorizado" }; 
        
        DefaultTableModel dtm = new DefaultTableModel( datas, columnNames ) {
			private static final long serialVersionUID = 1L;

			public boolean isCellEditable( int row, int column ) {
                return false;
            }
        };
        try {
        	dtm.setRowCount(0);
        	JsonNode node = new ObjectMapper().readTree( pagos );
        	for (JsonNode pago: node) {
        		String[] data = new String[6];
    			data[0] = Integer.toString( pago.get("numero").asInt() );
    	    	data[1] = Integer.toString( pago.get("compra").asInt() );
    	   		data[2] = (pago.get("autorizado").asBoolean() ? "AUTORIZADO":"NO AUTORIZADO") ;
    	   		dtm.addRow( data );
			}
        }
        catch(IOException e) {
        	System.out.println("[*][CONTROL] IOEXception: " + e.getMessage() );
        }
        
		tablaPagos = new JTable( dtm );
		tablaPagos.setFont( new Font("Garuda", Font.PLAIN, 16) );
		JScrollPane sp = new JScrollPane( tablaPagos );
		
		frameEstadoPagos.getContentPane().add( sp, BorderLayout.CENTER );
		frameEstadoPagos.setBounds( this.getBounds() );
		frameEstadoPagos.setModal(true);
		frameEstadoPagos.setVisible( true );
	}
	
	private void mostrarEstadoProductos( String titulo, String prods ) {
		JDialog frameEstadoProductos = new JDialog( this );
		frameEstadoProductos.getContentPane().setLayout( new BorderLayout() );
		frameEstadoProductos.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		frameEstadoProductos.setTitle( titulo );
		
		String[][] datas = {};
        String[] columnNames = new String[] { "compra","producto","estado" }; 
        
        DefaultTableModel dtm = new DefaultTableModel( datas, columnNames ) {
			private static final long serialVersionUID = 1L;

			public boolean isCellEditable( int row, int column ) {
                return false;
            }
        };
        try {
        	dtm.setRowCount(0);
        	JsonNode node = new ObjectMapper().readTree( prods );
        	for (JsonNode prod: node) {
        		String[] data = new String[6];
    			data[0] = Integer.toString( prod.get("compra").asInt() );
    	    	data[1] = Integer.toString( prod.get("producto").asInt() );
    	   		data[2] = prod.get("estado").asText();
    	   		dtm.addRow( data );
			}
        }
        catch(IOException e) {
        	System.out.println("[*][CONTROL] IOEXception: " + e.getMessage() );
        }
		tablaProductos = new JTable( dtm );
		tablaProductos.setFont( new Font("Garuda", Font.PLAIN, 16) );
		JScrollPane sp = new JScrollPane( tablaProductos );
		
		frameEstadoProductos.getContentPane().add( sp, BorderLayout.CENTER );
		frameEstadoProductos.setBounds( this.getBounds() );
		frameEstadoProductos.setModal(true);
		frameEstadoProductos.setVisible( true );
	}
	
	private void mostrarSeleccionServidorPap() {
		frameSeleccion = new JDialog( this );
		frameSeleccion.getContentPane().setLayout( new BorderLayout() );
		frameSeleccion.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		frameSeleccion.setTitle( "Seleccion de servidor");
		
		String[][] datas = {};
        String[] columnNames = new String[] { "SERVIDOR" }; 
        
        DefaultTableModel dtm = new DefaultTableModel( datas, columnNames ) {
			private static final long serialVersionUID = 1L;

			public boolean isCellEditable( int row, int column ) {
                return false;
            }
        };
       
       	dtm.setRowCount(0);
        if (lblEstadoCompras.getText().contains("PAP"))
        	dtm.addRow( new String[] {"COMPRAS"});
        if (lblEstadoEnvios.getText().contains("PAP"))
        	dtm.addRow( new String[] {"ENVIOS"});
        if (lblEstadoInf.getText().contains("PAP"))
        	dtm.addRow( new String[] {"INFRACCIONES"});
        if (lblEstadoPagos.getText().contains("PAP"))
        	dtm.addRow( new String[] {"PAGOS"});
        if (lblEstadoProductos.getText().contains("PAP"))
        	dtm.addRow( new String[] {"PRODUCTOS"});
		
		tablaServidores = new JTable( dtm );
		tablaServidores.setFont( new Font("Garuda", Font.PLAIN, 16) );
		JScrollPane sp = new JScrollPane( tablaServidores );
		
		frameSeleccion.getContentPane().add( sp, BorderLayout.CENTER );
		
		JButton btnAceptar = new JButton( "Aceptar" );
		btnAceptar.addActionListener( new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				seleccionarServidor();
			}
		});
		frameSeleccion.getContentPane().add( btnAceptar, BorderLayout.SOUTH );
		frameSeleccion.setBounds( this.getBounds() );
		frameSeleccion.setModal(true);
		frameSeleccion.setVisible( true );
	}
	
	private void seleccionarServidor() {
		int row = tablaServidores.getSelectedRow();
		if (row >= 0) {
			String sel = tablaServidores.getModel().getValueAt(row, 0).toString();
			if (sel.compareTo("COMPRAS") == 0)
				servControl.siguientePasoCompras();
			else if (sel.compareTo("INFRACCIONES") == 0)
				servControl.siguientePasoInfracciones();
			else if (sel.compareTo("PAGOS") == 0)
				servControl.siguientePasoPagos();
			else if (sel.compareTo("ENVIOS") == 0)
				servControl.siguientePasoEnvios();
			else if (sel.compareTo("PRODUCTOS") == 0)
				servControl.siguientePasoProductos();
			
			frameSeleccion.setVisible( false );
		}
	}
	
	public void iniciarMensajeria() {
		mensajeria = new ManejadorMensajesControl();
		if (mensajeria.conectar() == 1)
			System.out.println("[*][CONTROL] Exchange creado...");
		mensajeria.recibir();
	}

	public ManejadorMensajesControl getMensajeria() {
		return mensajeria;
	}

	public void setMensajeria(ManejadorMensajesControl mensajeria) {
		this.mensajeria = mensajeria;
	}

	public Config getConfig() {
		if (config == null)
			config = new ConfigDAO().obtenerConfiguracion();
		return config;
	}

	public void setConfig(Config config) {
		this.config = config;
	}

	public JTable getTablaCompras() {
		return tablaCompras;
	}

	public void setTablaCompras(JTable tablaCompras) {
		this.tablaCompras = tablaCompras;
	}

	public JTable getTablaEnvios() {
		return tablaEnvios;
	}

	public void setTablaEnvios(JTable tablaEnvios) {
		this.tablaEnvios = tablaEnvios;
	}

	public JTable getTablaInfracciones() {
		return tablaInfracciones;
	}

	public void setTablaInfracciones(JTable tablaInfracciones) {
		this.tablaInfracciones = tablaInfracciones;
	}

	public JTable getTablaPagos() {
		return tablaPagos;
	}

	public void setTablaPagos(JTable tablaPagos) {
		this.tablaPagos = tablaPagos;
	}

	public JTable getTablaProductos() {
		return tablaProductos;
	}

	public void setTablaProductos(JTable tablaProductos) {
		this.tablaProductos = tablaProductos;
	}

	
}
