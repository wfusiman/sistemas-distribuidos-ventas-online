package sd.productos;

public class Producto {

	private int codigo;
	private String descripcion;
	private float precio;
	private int stock;
	
	private int proveedorCodigo;
	private String proveedorDenominacion;
	
	
	public Producto() {}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public float getPrecio() {
		return precio;
	}

	public void setPrecio(float precio) {
		this.precio = precio;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public int getProveedorCodigo() {
		return proveedorCodigo;
	}

	public void setProveedorCodigo(int proveedorCodigo) {
		this.proveedorCodigo = proveedorCodigo;
	}

	public String getProveedorDenominacion() {
		return proveedorDenominacion;
	}

	public void setProveedorDenominacion(String proveedorDenominacion) {
		this.proveedorDenominacion = proveedorDenominacion;
	}
	
	
}
