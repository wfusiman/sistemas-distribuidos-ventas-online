package servidor;

import java.util.concurrent.atomic.AtomicReferenceArray;

public class RelojVectorial {
	
	public final static int INDEX_CONTROL = 0;
    public final static int INDEX_COMPRAS = 1;
    public final static int INDEX_PRODUCTOS = 2;
    public final static int INDEX_ENVIOS = 3;
    public final static int INDEX_INFRACCIONES = 4;
    public final static int INDEX_PAGOS = 5;
    

    private AtomicReferenceArray<Long> times = new AtomicReferenceArray<Long>(6);

    /**
     * Crea un Reloj vectorial con sus relojes a 0
     */
    public RelojVectorial() {
        this( 0L );
    }

    /**
     * Crea un reloj vectorial con sus relojes seteados a tiempo 
     * @param tiempo
     */
    public RelojVectorial( long tiempo ) {
        for (int i=0; i < times.length(); i++)
            times.set( i,tiempo );
    }

    /**
     * Crea un reloj vectorial con sus relojes seteados para cada servidor
     * @param timesServers
     */
    public RelojVectorial( long[] timesServers ) {
        for (int i=0; i < times.length(); i++)
            times.set( i, timesServers[i] );
    }

    /**
    * Retorna el valor actual del reloj del servidor en indexServer
    */
    public long time( int indexServer ) {
        return times.get( indexServer );
    }

    /**
     * Incrementa en 1 y retorn el valor del reloj del servidor en indexServer
     */
    public long tick( int indexServer ) {
        times.set( indexServer, times.get(indexServer) + 1);
        return times.get(indexServer);
    }

    /**
     * Incremente los relojes del servidor en indexServer, de acuerdo al valor del tiempoAnterior
     * @param indexServer
     * @param tiempoAnterior
     * @return nuevo valor del reloj en indexServer
     */
    public long tick( int indexServer, long tiempoNuevo ) {
        long tiempoActual = times.get( indexServer );
        if (tiempoNuevo > tiempoActual) 
            times.set( indexServer, tiempoNuevo );
        return times.get( indexServer );
    }

    /**
     * Retorna verdadero si el tiempo en indexServer es anterior al valor actual
     */
    public boolean ocurrioAntes( int indexServer, long tiempo ) {
        return (Long.compare( times.get(indexServer), tiempo) > 0);
    }

    /**
     * Retorna verdadero si el tiempo en indexServer es posterior al valor actual
     * @param indexServer
     * @param tiempo
     * @return
     */
    public boolean ocurrioDespues( int indexServer, long tiempo ) {
        return (Long.compare( times.get(indexServer), tiempo) < 0);
    }

    /**
     * Retorna los valores de los relojes de los servidores en un arreglo de longs
     * @return arreglo de valores de relojes
     */
    public long[] toLongArray() {
        long[] tms = new long[ times.length() ];
        for (int i=0; i < times.length(); i++)
            tms[i] = times.get(i);

        return tms;
    }
    
    /**
     * Incrementa el reloj de indexServer y ajustar los otros relojes de acuerdo al arreglo tiempos
     * @param indexServer
     * @param tiempos
     */
    public void ajustarTiempos( int indexServer, long[] tiempos ) {
    	long tiempo;
    	tick( indexServer );
    	if (tiempos != null)
    		for (int i=0; i < times.length();i++) {
    			tiempo = tiempos[i];
    			if (i != indexServer)
    				tick( i, tiempo );
    		}
    }
    
    public String toString() {
    	return times.get(0).toString() + times.get(1).toString() + times.get(2).toString() + times.get(3).toString() + times.get(4).toString() + times.get(5).toString();
    }

}
