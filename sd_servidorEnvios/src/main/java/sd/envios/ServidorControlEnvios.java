package sd.envios;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import sd.envios.persistencia.ManejadorPersistencia;
import sd.envios.negocio.ServidorEnvios;
import sd.envios.persistencia.ConfigDAO;

import servidor.Servidor;

public class ServidorControlEnvios implements Servidor {
	
	private final int PUERTO = 50004; //Puerto para la conexión
	private ServerSocket serverSocket; 	//Socket del servidor
	private Socket socket; 		//Socket 
	private DataInputStream dis;
	private DataOutputStream dos;
	
	private boolean activo;
	private boolean automatico;
	private Config config;
	
	ManejadorPersistencia manejadorPersistencia;
	ServidorEnvios servidorEnvios;
	
	public ServidorControlEnvios() { 
		setActivo(false); 
		this.automatico = true;
	}
	
	public static void main( String[] args ) {
		ServidorControlEnvios se = new ServidorControlEnvios();
		se.setup();
		se.atender();
	}
	
	private void setup() {
		try {
			serverSocket = new ServerSocket( PUERTO );
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public int iniciar() {
		System.out.println("[*][ENVIOS] Iniciando Servidor... ");
		servidorEnvios = new ServidorEnvios();
		if (automatico) {
			System.out.println("[*][ENVIOS] Modo automatico... ");
			servidorEnvios.iniciarAutomatico();
			config = abrirConfig();
			manejadorPersistencia = new ManejadorPersistencia( config.getMinutosPersistencia(), servidorEnvios );
			manejadorPersistencia.start();
		}
		else { 
			System.out.println("[*][ENVIOS] Iniciando Paso a paso... ");
			servidorEnvios.iniciarPap();
			config = null;
		}
	
		setActivo( true );
		return 0;
	}

	public void detener() {
        System.out.println("[*][ENVIOS] Deteniendo servidor Envios... ");
        if (automatico) 
        	manejadorPersistencia.stop();
        servidorEnvios.detener();
		setActivo( false );
	}

	public int reiniciar() {
		detener();
		iniciar();
		return 0;
	}

	public void derribar() {
		
	}

	public String estadoActual() {
		return servidorEnvios.listaEnviosString();
	}

	public String estadoUltimo() {
		return servidorEnvios.listaEnviosPerisitidosString();
	}

	public int siguientePaso() {
		servidorEnvios.recibirSiguienteMensaje();
		return 0;
	}
	
	
	public int persistirEstado() {
		servidorEnvios.almacenarEstado();
		return 0;
	}
	
	public int iniciarCorteConsistente() {
		servidorEnvios.comenzarCorteConsistente();
		return 0;
	}
	
	public int setearConfig( int[] params ) {
		ConfigDAO configdao = new ConfigDAO();
		
		return configdao.guardarConfiguracion( new Config( (params[0] == 0) ? "automatico":"manual",params[1] ) );
	}

	private Config abrirConfig() {
		ConfigDAO configdao = new ConfigDAO();
		config = configdao.obtenerConfiguracion();
		return config;
	}

	public void atender() {
		while(true){
            System.out.println("[*][ENVIOS] ejecutando.... Esperando cliente");
            try {
				socket = serverSocket.accept();
				dis = new DataInputStream( socket.getInputStream() );
	            
	            String msjRcv = (String) dis.readUTF();
	            System.out.println("[*][ENVIOS] Mensaje recibido: " + msjRcv );
	           
	            String msjSnd = procesar( msjRcv );
	            
	            dos = new DataOutputStream( socket.getOutputStream());
	            dos.writeUTF( msjSnd );
	            System.out.println("[*][ENVIOS], Enviando respuesta: " + msjSnd );
	            
	            dis.close();
	            dos.close();
	            socket.close();
			} catch (IOException e) {
				System.out.println("[ENVIOS] Exception atender ... " + e.getMessage() );
				e.printStackTrace();
			}
        }
	}

	/* procesar: procesa comando del que se envian mediante sockets */
	private String procesar( String msj ) {
		String []params = msj.split(" ");
		System.out.println( "[*][ENVIOS] procesando pedido: " + params[0] ); 
		if (params[0].compareTo(INICIAR_AUTOMATICO) == 0) {
			automatico = true;
			iniciar();
			return "EJECUTANDO AUTOMATICO";
		}
		else if (params[0].compareTo(INICIAR_PASOPASO) == 0) {
			automatico = false;
			iniciar();
			return "EJECUTANDO PAP";
		}
		else if (params[0].compareTo(DETENER) == 0) {
			detener();
			return "DETENIDO";
		}
		else if (params[0].compareTo(REINICIAR) == 0) {
			reiniciar();
			return "REINICIADO";
		}
		else if (params[0].compareTo(DERRIBAR) == 0) {
			derribar();
			return "REINICIADO";
		}
		else if (params[0].compareTo(ESTADO_ACTUAL) == 0) {
			return estadoActual();
		}
		else if (params[0].compareTo(ESTADO_ULTIMO) == 0) {
			return estadoUltimo();
		}
		else if (params[0].compareTo(SIGUIENTE) == 0) {
			siguientePaso();
			return "OK";
		}
		else if (params[0].compareTo(PERSISTIR) == 0) {
			persistirEstado();
			return "OK";
		}
		else if (params[0].compareTo(CORTE) == 0) {
			iniciarCorteConsistente();
			return "OK";
		}
		else if (params[0].compareTo( SERVERSTATUS ) == 0) {
			return (activo) ? "EJECUTANDO " + (automatico ? "AUTOMATICO":"PASO PASO"):"DETENIDO";
		}
		else {
			// comando invalido
			return "COMANDO INVALIDO";
		}
	}
	
	public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}


}
