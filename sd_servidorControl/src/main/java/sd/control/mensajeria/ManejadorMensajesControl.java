/**
 * ManejadorMensajes.java
 */

package sd.control.mensajeria;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

import sd.control.gui.ControlMonitor;
import sd.control.gui.GenerarCompra;

import servidor.Mensaje;

public class ManejadorMensajesControl {
	
	//private String queue;
	private Channel channel;
	private static final String EXCHANGE_NAME = "SD_COMPRASONLINE";
	private static final String HOST = "localhost";
	private static final String QUEUE_NAME = "Q_CONTROL";

	private GenerarCompra icompra;
	private ControlMonitor controlm;

	public ManejadorMensajesControl() {
	}

	/**
	 * conecta al servidor de mensajeria RabbitMQ
	 * @return
	 */
	public int conectar() {
		ConnectionFactory factory = new ConnectionFactory();
       factory.setHost( HOST );
        try {
        	Connection connection = factory.newConnection();
        	channel = connection.createChannel();
        	channel.exchangeDeclare( EXCHANGE_NAME, "topic",true );
        	channel.queueDeclare( QUEUE_NAME, true, false, false, null );
        	return 1;
        } catch (IOException | TimeoutException e) {
			System.out.println( "[*][CONTROL] ERROR conectando  a exchange " + EXCHANGE_NAME + " : " + e.getMessage() );
			return -1;
		}
	}
	
	/**
	 * Envia un mensaje, al servidor de mensajeria con el topico routingkey
	 * @param mensaje
	 * @param routingKey
	 * @return
	 */
	public int enviar( Mensaje msj, String routingKey ) {
		ObjectMapper mapper= new ObjectMapper();
		String mensaje;
		try {
			mensaje = mapper.writeValueAsString( msj );
		} catch (JsonProcessingException e1) {
			System.out.println("[*][CONTROL] Mensajeria JSonPRocessingException: " + e1.getMessage() );
			return -1;
		}
	    try {
	    	System.out.println("[*][CONTROL] --> enviando mensaje: " + mensaje + " - routingkey: " + routingKey );
			channel.basicPublish( EXCHANGE_NAME, routingKey, null, mensaje.getBytes("UTF-8"));
			return 1;
		} catch (IOException e) {
			System.out.println("[*][CONTROL] IOException enviar : " + e.getMessage() );
			return -1;
		} 
	}
	
	/**
	 * Lee del servidor de mensajeria.
	 */
	public void recibir() {
    	String queueName = null;
		try {
			channel.queueBind( QUEUE_NAME, EXCHANGE_NAME, "*.control" );
		} catch (IOException e) {
			System.out.println("[*][CONTROL] IOException cola " + queueName + ", recibir: " + e.getMessage() );
		}
			
		DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), "UTF-8");
            System.out.println("[*][CONTROL] <-- Recibiendo mensaje: " + message );
        };
        
        try {
        	channel.basicConsume( QUEUE_NAME, true, deliverCallback, consumerTag -> { });
		} catch (IOException e) {
			System.out.println("[*][CONTROL] IOException basicConsume: " + e.getMessage() );
		}
    }
	
	public GenerarCompra getIcompra() {
		return icompra;
	}

	public void setIcompra( GenerarCompra icompra) {
		this.icompra = icompra;
	}

	public ControlMonitor getControlm() {
		return controlm;
	}

	public void setControlm(ControlMonitor controlm) {
		this.controlm = controlm;
	}
}
