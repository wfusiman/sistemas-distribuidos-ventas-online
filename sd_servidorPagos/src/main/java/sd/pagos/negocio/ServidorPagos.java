package sd.pagos.negocio;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import sd.pagos.Pago;
import sd.pagos.mensajeria.ManejadorMensajesPago;
import sd.pagos.persistencia.ConfigDAO;
import sd.pagos.persistencia.PagoDAO;
import sd.pagos.persistencia.RelojDAO;
import servidor.Mensaje.ID;
import servidor.Mensaje.Operacion;
import servidor.Canal;
import servidor.Mensaje;
import servidor.RelojVectorial;

public class ServidorPagos {

	private List<Pago> pagos;
	private ManejadorMensajesPago manejadorMensajes;
	private RelojVectorial relojes;
	private RelojDAO relojDAO;
	
	private boolean snapshot;
	 
	Canal canalProductos;
	Canal canalCompras;
	List<Pago> pagosCorte;
	
	public void iniciarAutomatico() {
		iniciar();
		manejadorMensajes.recibir();
	}
	
	public void iniciarPap() {
		iniciar();
	}
	
	private void iniciar() {
		relojDAO = new RelojDAO();
		relojes = new RelojVectorial( relojDAO.obtenerRelojes() );
		pagos = new PagoDAO().obtenerTodosPagos();
		if (pagos == null)
			pagos = new ArrayList<Pago>();
		manejadorMensajes = new ManejadorMensajesPago( this );
		manejadorMensajes.conectar();
		
		// Inicializar los canales
		canalProductos = new Canal( ID.S_PRODUCTOS.toString(), new ArrayList<Mensaje>(), servidor.Canal.Estado.DESABILITADO );
		canalCompras = new Canal( ID.S_COMPRAS.toString(), new ArrayList<Mensaje>(), servidor.Canal.Estado.DESABILITADO );
	}

	public void detener() {
		new PagoDAO().guardarPagos( pagos );
		manejadorMensajes.detener();
	}

	public void recibirSiguienteMensaje() {
		manejadorMensajes.recibirSingle();
	}
	

	public void comenzarCorteConsistente() {
		
		canalProductos.setEstado( servidor.Canal.Estado.LOGGING );
		canalCompras.setEstado( servidor.Canal.Estado.LOGGING );
		
		almacenarEstadoCorte();
		
		Mensaje mensaje = new Mensaje();
		mensaje.setIdentificador( ID.S_PAGOS );
		mensaje.setOper( Operacion.SNAPSHOT );
		
		relojes.tick( RelojVectorial.INDEX_PAGOS ); // incrementa reloj
    	mensaje.setTiempos( relojes.toLongArray() );
		manejadorMensajes.enviar( mensaje, "pagos.compras" );
		
		relojes.tick( RelojVectorial.INDEX_PAGOS ); // incrementa reloj
    	mensaje.setTiempos( relojes.toLongArray() );
		manejadorMensajes.enviar( mensaje, "pagos.productos" );
		
		setSnapshot( true );
	}
	
	
	public void procesarMensaje( ID origen, Operacion operacion, String mensaje, long[] tiempos) {
		procesarCorte( origen , operacion, mensaje );
		// Incrementa tiempo reloj propio, y ajusta los tiempos de los otros relojes
		relojes.ajustarTiempos( RelojVectorial.INDEX_PAGOS, tiempos );
  
		if (operacion == Operacion.AUTORIZAR_PAGO) {      	
    		JsonNode node;
			try {
				ObjectMapper mapper = new ObjectMapper();
				node = mapper.readTree( mensaje );
				int compra = node.get("compra").asInt();
	        	
	        	boolean aut = autorizarPago(compra, true );
	        	
	        	/*
	        	ObjectNode node1 = mapper.createObjectNode();
	        	node1.put("compra", compra );
	        	node1.put("autorizado", aut );
	        	*/
	        	Mensaje msj = new Mensaje();
	        	msj.setIdentificador( ID.S_PAGOS );
	        	msj.setOper( Operacion.AUTORIZAR_PAGO );
	        	msj.setMensaje( "{\"compra\":" + compra + ",\"autorizado\":" + aut + "}" );
	        	//msj.setMensaje( node1.toString() );
	        	
	        	relojes.tick( RelojVectorial.INDEX_PAGOS );
	        	msj.setTiempos( relojes.toLongArray() );
	        	manejadorMensajes.enviar( msj,"pagos.compras" );
	        	
	        	relojes.tick( RelojVectorial.INDEX_PAGOS );
	        	msj.setTiempos( relojes.toLongArray() );
	        	manejadorMensajes.enviar( msj,"pagos.productos" );
			} catch (IOException e) {
				System.out.println("[*][PAGOS] IOException: " + e.getMessage() );
			}
        	
        }
        else if (operacion == Operacion.SNAPSHOT) {
   		 	if (origen == ID.S_PRODUCTOS) {
   		 		if (canalProductos.getEstado() == servidor.Canal.Estado.DESABILITADO) {
   		 			
   		 			canalProductos.setEstado( servidor.Canal.Estado.COMPLETO );
   		 			canalCompras.setEstado( servidor.Canal.Estado.LOGGING );
   				
   		 			almacenarEstadoCorte();
   		
   		 			Mensaje msj = new Mensaje();
   		 			msj.setIdentificador( ID.S_PAGOS );
   		 			msj.setOper( Operacion.SNAPSHOT );
	   		 		
   		 			relojes.tick( RelojVectorial.INDEX_PAGOS );
		        	msj.setTiempos( relojes.toLongArray() );
   		 			manejadorMensajes.enviar( msj, "pagos.compras" );
   		 		
	   		 		relojes.tick( RelojVectorial.INDEX_PAGOS );
		        	msj.setTiempos( relojes.toLongArray() );
   		 			manejadorMensajes.enviar( msj, "pagos.productos" );
   		 		}
   		 		else if (canalProductos.getEstado() == servidor.Canal.Estado.LOGGING) {
	   		 		canalProductos.setEstado( servidor.Canal.Estado.COMPLETO );
   		 			cerrarCorte();
   		 		}
   		 	}
   		 	else if (origen == ID.S_COMPRAS) {
   		 		if (canalCompras.getEstado() == servidor.Canal.Estado.DESABILITADO) {
   		 			
   		 			canalProductos.setEstado( servidor.Canal.Estado.LOGGING );
   		 			canalCompras.setEstado( servidor.Canal.Estado.COMPLETO );
   		 		
   		 			almacenarEstadoCorte();
   		
   		 			Mensaje msj = new Mensaje();
   		 			msj.setIdentificador( ID.S_PAGOS );
   		 			msj.setOper( Operacion.SNAPSHOT );
	   		 			
	   		 		relojes.tick( RelojVectorial.INDEX_PAGOS );
		        	msj.setTiempos( relojes.toLongArray() );
   		 			manejadorMensajes.enviar( msj, "pagos.productos" );
   		 			
   		 			relojes.tick( RelojVectorial.INDEX_PAGOS );
		        	msj.setTiempos( relojes.toLongArray() );
   		 			manejadorMensajes.enviar( msj, "pagos.compras" );
   		 		
   		 		}
   		 		else if (canalCompras.getEstado() == servidor.Canal.Estado.LOGGING) {
   		 			canalCompras.setEstado( servidor.Canal.Estado.COMPLETO );
   		 			cerrarCorte();
   		 		}
		 	}
        }
		// Guarda el reloj actual en archivo.
	 	relojDAO.guardarRelojes( relojes.toLongArray() );
	}

	
	public List<Pago> listaPagos() {
		return pagos;
	}

	public boolean autorizarPago( int compra, boolean aut ) {
		boolean autorizado;
		Pago pago = new Pago();
		pago.setNumero( pagos.size() + 1 );
		pago.setCompra(compra);
		
		double randomValue = Math.random()*100;  //0.0 to 99.9
		autorizado = randomValue <= new ConfigDAO().obtenerConfiguracion().getProbabilidadAutorizacion();
		
		pago.setAutorizado(autorizado);
		pagos.add(pago);
		return autorizado;
	}
	
	public String pagosEstadoString() {
		return pagosToString( pagos );
	}
	
	public String pagosPersistidosString() {
		PagoDAO pdao = new PagoDAO();
		List<Pago> listPagos = pdao.obtenerTodosPagos();
		return pagosToString( listPagos );
	}
	
	private String pagosToString( List<Pago> listPagos ) {
		ObjectMapper mapper = new ObjectMapper();
		ArrayNode arrayNode = mapper.createArrayNode();
		if (listPagos != null)
			for (Pago pago: listPagos) {
				ObjectNode node = mapper.createObjectNode();
				node.put("numero", pago.getNumero() );
				node.put("compra", pago.getCompra() );
				node.put("autorizado", pago.isAutorizado() );
				arrayNode.add( node );
			}
		return arrayNode.toString();
	}
	
	public void almacenarEstado() {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		PagoDAO edao = new PagoDAO();
		System.out.println("[*][PAGOs] almacenando estado de envios...  " + pagos.size() + " registros, " + dateFormat.format( new Date() ) );
		if (pagos.size() > 0)
			edao.guardarPagos(pagos);
	}

	
	private void procesarCorte( ID origen, Operacion operacion, String mensaje ) {
		if (operacion == Operacion.SNAPSHOT)
			return;
		if ((canalCompras.getNombre().compareToIgnoreCase( origen.toString())  == 0) && canalCompras.getEstado() == servidor.Canal.Estado.LOGGING) {
			Mensaje m = new Mensaje();
			m.setIdentificador( origen );
			m.setOper( operacion );
			m.setMensaje(mensaje);
			canalCompras.agregarMensaje( m );
		}
		else if ((canalProductos.getNombre().compareToIgnoreCase( origen.toString()) == 0) && canalProductos.getEstado() == servidor.Canal.Estado.LOGGING) {
			Mensaje m = new Mensaje();
			m.setIdentificador( origen );
			m.setOper( operacion );
			m.setMensaje(mensaje);
			canalProductos.agregarMensaje( m );
		}
		
	}
	
	private void almacenarEstadoCorte() {
		pagosCorte = new ArrayList<Pago>( pagos );
	}
	
	private void cerrarCorte() {
		if (canalCompras.getEstado() == servidor.Canal.Estado.COMPLETO && canalProductos.getEstado() == servidor.Canal.Estado.COMPLETO ) {
			
			almacenarCanalesCorte();
			
			canalCompras.setEstado( servidor.Canal.Estado.DESABILITADO );
			canalCompras.setMensajes( new ArrayList<Mensaje>() );
			
			canalProductos.setEstado( servidor.Canal.Estado.DESABILITADO );
			canalProductos.setMensajes( new ArrayList<Mensaje>() );
		}
	}
	
	private void almacenarCanalesCorte() {
		ObjectMapper mapper = new ObjectMapper();
	
		// Nodo raiz
		ObjectNode nodeCorte = mapper.createObjectNode();
		
		// Nodo estado; array de compras
		ArrayNode nodePagos = mapper.createArrayNode();
		for (Pago p: pagosCorte) {
			ObjectNode nodePago = mapper.createObjectNode();
			nodePago.put("numero", p.getNumero() );
			nodePago.put("compra", p.getCompra() );
			nodePago.put("importe", p.getImporte() );
			nodePago.put("formaPago", p.getFormaPago() );
			nodePago.put("fecha", (p.getFecha() == null) ? "": p.getFecha().toString()  );
			nodePagos.add( nodePago );
		}
		nodeCorte.put("estado", nodePagos.toString() );
		
		// Nodo canales: array de mensajes
		ArrayNode nodeCanales = mapper.createArrayNode();
		for (Mensaje m: canalCompras.getMensajes()) {
			ObjectNode nodeCanal = mapper.createObjectNode();
			nodeCanal.put("operacion", m.getOper().toString() );
			nodeCanal.put("mensaje", m.getMensaje().toString() );
			nodeCanales.add( nodeCanal );
		}
		for (Mensaje m: canalProductos.getMensajes()) {
			ObjectNode nodeCanal = mapper.createObjectNode();
			nodeCanal.put("operacion", m.getOper().toString() );
			nodeCanal.put("mensaje", m.getMensaje().toString() );
			nodeCanales.add( nodeCanal );
		}
		nodeCorte.put( "canales", nodeCanales.toString() );
		
		try {
			String archivo = System.getProperty("user.dir") + "/data/" + "corte" + relojes.toString() + ".json";
			File file = new File( archivo );
			file.createNewFile();
			FileOutputStream output = new FileOutputStream( archivo );
			JsonGenerator g = mapper.getFactory().createGenerator( output );
			mapper.writeValue( g, nodeCorte );
		} catch (IOException e) {
			System.out.println( "[*][COMPRAS] IOException guardando Corte: " + e.getMessage() );
		}
		
	}
	

	public boolean isSnapshot() {
		return snapshot;
	}

	public void setSnapshot(boolean snapshot) {
		this.snapshot = snapshot;
	}
}
