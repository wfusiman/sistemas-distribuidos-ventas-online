package servidor;

public class Mensaje {
	
	public enum Operacion {RESPUESTA, PRODUCTOS_LIST, TOTAL_COMPRAS, NUEVA_COMPRA, CONFIRMAR_COMPRA, RESERVAR_PRODUCTO, INFORMAR_INFRACCION, 
							INFORMAR_PAGO, ENVIAR, DETECTAR_INFRACCION, CONTROL_INFRACCION, CALCULAR_ENVIO, AGENDAR_ENVIO, AUTORIZAR_PAGO,
							ESTADO_ACTUAL, ESTADO_ULTIMO, COLA_MENSAJES,
							SNAPSHOT};
	public enum ID {S_CONTROL,S_COMPRAS,S_INFRACCIONES, S_ENVIOS, S_PAGOS, S_PRODUCTOS};
	
	private ID identificador;
	private Operacion oper;
	private String mensaje;
	private long[] tiempos;
	
	public Mensaje() {}
	
	public Mensaje( Operacion oper, String msj ) {
		this.oper = oper;
		this.mensaje = msj;
	}

	public Operacion getOper() {
		return oper;
	}

	public void setOper(Operacion oper) {
		this.oper = oper;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public long[] getTiempos() {
		return tiempos;
	}

	public void setTiempos(long[] tiempos) {
		this.tiempos = tiempos;
	}

	public ID getIdentificador() {
		return identificador;
	}

	public void setIdentificador(ID identificador) {
		this.identificador = identificador;
	}
}
