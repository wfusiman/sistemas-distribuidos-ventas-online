/**
 * ManejadorPersistencia.java
 */

package sd.pagos.persistencia;

import sd.pagos.negocio.ServidorPagos;

public class ManejadorPersistencia implements Runnable {

	private volatile Thread blinker;
	int minutos;
	ServidorPagos sp;
	
	public ManejadorPersistencia( ServidorPagos sp ) {
		this.sp = sp;
	}
	
	public ManejadorPersistencia( int min, ServidorPagos sp ) {
		this.minutos = min;
		this.sp = sp;
	}
	
	public void start() {
		blinker = new Thread( this );
		System.out.println("[*][PAGOS] Iniciando Manejador de persistencia..." );
		blinker.start();
	}
	
	@Override
	public void run() {
		System.out.println("[*][PAGOS]  Manejador de persistencia, actualizando estado cada " + minutos + " minutos" );
		Thread thisThread = Thread.currentThread();
		while (blinker == thisThread) {
			try {
				Thread.sleep( minutos * 60000 );
				sp.almacenarEstado();
			} catch (InterruptedException e) {
				System.out.println("[*][PAGOS] , InterruptedException " + e.getMessage() );
			}
		}
	}
	
	public void stop() {
		System.out.println("[*][PAGOS] Deteniendo Manejador de persistencia..." );
		blinker = null;
	}

}
