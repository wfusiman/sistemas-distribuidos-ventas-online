package sd.pagos;

public class Config {
	
	private String modo;
	private int probabilidadAutorizacion; 
	private int minutosPersistencia; 
	
	public Config() {}
	
	public Config( String modo, int prob, int min ) {
		this.modo = modo;
		this.setProbabilidadAutorizacion(prob);
		this.setMinutosPersistencia(min);
	}

	public String getModo() {
		return modo;
	}

	public void setModo(String modo) {
		this.modo = modo;
	}

	public int getProbabilidadAutorizacion() {
		return probabilidadAutorizacion;
	}

	public void setProbabilidadAutorizacion(int probabilidadAutorizacion) {
		this.probabilidadAutorizacion = probabilidadAutorizacion;
	}

	public int getMinutosPersistencia() {
		return minutosPersistencia;
	}

	public void setMinutosPersistencia(int minutosPersistencia) {
		this.minutosPersistencia = minutosPersistencia;
	}
	
}
