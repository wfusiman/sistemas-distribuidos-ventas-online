package servidor;

import java.util.List;

public class Canal {

	public enum Estado {DESABILITADO,LOGGING,COMPLETO};
	
	private String nombre;
	private List<Mensaje> mensajes;
	private Estado estado;
	
	public Canal() {}
	
	public Canal( String nombre, List<Mensaje> msjs, Estado est ) {
		this.setNombre(nombre);
		this.mensajes = msjs;
		this.estado = est;
	}
	
	public void vaciar() {
		mensajes.clear();
	}
	
	public void agregarMensaje( Mensaje mensaje ) {
		if (mensajes != null)
			mensajes.add( mensaje );
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<Mensaje> getMensajes() {
		return mensajes;
	}

	public void setMensajes( List<Mensaje> mensajes) {
		this.mensajes = mensajes;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}
	
	
}
