package sd.infracciones;


public class Config {
	
	private String modo;
	private int probabilidadInfraccion; // setea la probabilidad de que se detecte una infraccion para una compra
	private int minutosPersistencia;  // setea los minutos cada los que se persisten los datos
	
	public Config() {}
	
	public Config( String modo, int prob, int min ) {
		this.modo = modo;
		this.probabilidadInfraccion = prob;
		this.minutosPersistencia = min;
	}

	public int getMinutosPersistencia() {
		return minutosPersistencia;
	}

	public void setMinutosPersistencia(int minutosAct) {
		this.minutosPersistencia = minutosAct;
	}

	public String getModo() {
		return modo;
	}

	public void setModo(String modo) {
		this.modo = modo;
	}

	public int getProbabilidadInfraccion() {
		return probabilidadInfraccion;
	}

	public void setProbabilidadInfraccion(int probInfraccion) {
		this.probabilidadInfraccion = probInfraccion;
	}

	
}
