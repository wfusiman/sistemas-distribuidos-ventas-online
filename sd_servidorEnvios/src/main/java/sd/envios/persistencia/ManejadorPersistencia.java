/**
 * ManejadorPersistencia.java
 */

package sd.envios.persistencia;

import sd.envios.negocio.ServidorEnvios;

public class ManejadorPersistencia implements Runnable {

	private volatile Thread blinker;
	int minutos;
	ServidorEnvios se;
	
	public ManejadorPersistencia( ServidorEnvios se ) {
		this.se = se;
	}
	
	public ManejadorPersistencia( int min, ServidorEnvios se ) {
		this.minutos = min;
		this.se = se;
	}
	
	public void start() {
		blinker = new Thread( this );
		System.out.println("[*][ENVIOS] Iniciando Manejador de persistencia..." );
		blinker.start();
	}
	
	public void run() {
		System.out.println("[*][ENVIOS]  Manejador de persistencia, actualizando estado cada " + minutos + " minutos" );
		Thread thisThread = Thread.currentThread();
		while (blinker == thisThread) {
			try {
				Thread.sleep( minutos * 60000 );
				se.almacenarEstado();
			} catch (InterruptedException e) {
				System.out.println("[*][ENVIOS] , InterruptedException " + e.getMessage() );
			}
		}
	}
	
	public void stop() {
		System.out.println("[*][ENVIOS] Deteniendo Manejador de persistencia..." );
		blinker = null;
	}

}
