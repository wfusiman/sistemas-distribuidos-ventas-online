package sd.compras.persistencia;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;

import sd.compras.Compra;

public class ComprasDAO {
	
	private static final String path = System.getProperty("user.dir") + "/data/";
	private String archivo;
	
	public ComprasDAO() {
		this("data.json");
	}
	
	public ComprasDAO( String archivo ) {
		this.setArchivo(archivo);
	}
	
	public Compra obtenerCompra( int number ) {
		List<Compra> compras = obtenerTodasCompras();
		for (Compra compra: compras) {
			if (compra.getNumero() == number)
				return compra;
		}
		return null;
	}
	
	public List<Compra> obtenerTodasCompras() {
		ObjectMapper mapper = new ObjectMapper();
		try {
			if (!existeArchivo())
				return null;
			Compra[] comps = mapper.readValue( new File( path+getArchivo() ), Compra[].class );
			if (comps != null)
				return new ArrayList<Compra>( Arrays.asList( comps ) );
			return null;
		} catch (IOException e) {
			System.out.println("[*][COMPRAS] IO Excepction: " + e.getMessage() );
			return null;
		}
	}
	
	public int guardarCompra( Compra compra ) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			if (!existeArchivo() && !crearArchivo())
				return -1;
			FileOutputStream output = new FileOutputStream( path+getArchivo() );
			JsonGenerator g = mapper.getFactory().createGenerator( output );
			mapper.writeValue( g,compra	 );
			return 1;
		} catch (IOException e) {
			System.out.println( "[*][COMPRAS] IOException: " + e.getMessage() );
			return -1;
		}
	}
	
	public int guardarCompras( List<Compra> compras ) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			if (!existeArchivo() && !crearArchivo())
				return -1;
			FileOutputStream output = new FileOutputStream( path+getArchivo() );
			JsonGenerator g = mapper.getFactory().createGenerator( output );
			mapper.writeValue( g, compras );
			return 1;
		} catch (IOException e) {
			System.out.println( "[*][COMPRAS] IOException: " + e.getMessage() );
			return -1;
		}
	}
	
	private boolean existeArchivo() {
		File file = new File( path+getArchivo() );
		return file.exists();
	}
	
	private boolean crearArchivo() {
		File file = new File( path+getArchivo() );
		try {
			file.createNewFile();
			return true;
		} catch (IOException e) {
			System.out.println("[*][COMPRAS] IOEXception: " + e.getMessage() );
			return false;
		}
	}
	
	private String getArchivo() {
		return archivo;
	}

	private void setArchivo(String archivo) {
		this.archivo = archivo;
	}

}
