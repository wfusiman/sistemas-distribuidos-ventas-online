package sd.envios.negocio;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import sd.envios.persistencia.EnvioDAO;
import sd.envios.persistencia.RelojDAO;
import sd.envios.Envio;
import sd.envios.mensajeria.ManejadorMensajesEnvio;
import servidor.Canal;
import servidor.Mensaje;
import servidor.RelojVectorial;
import servidor.Canal.Estado;
import servidor.Mensaje.ID;
import servidor.Mensaje.Operacion;

public class ServidorEnvios  {
	
	private List<Envio> envios;
	private RelojVectorial relojes;
	private ManejadorMensajesEnvio manejadorMensajes;
	private RelojDAO relojDAO;

	private boolean snapshot;
	 
	Canal canalProductos;
	Canal canalCompras;
	List<Envio> enviosCorte;
	
	public ServidorEnvios() {
	}
	
	public void iniciarAutomatico() {
		iniciar();
		manejadorMensajes.recibir();
	}
	
	public void iniciarPap() {
		iniciar();
	}
	
	private void iniciar() {
		relojDAO = new RelojDAO();
		relojes = new RelojVectorial( relojDAO.obtenerRelojes() );
		envios = new EnvioDAO().obtenerTodosEnvios();
		if (envios == null)
			 envios = new ArrayList<Envio>();
		manejadorMensajes = new ManejadorMensajesEnvio( this );
		manejadorMensajes.conectar();
		
		// Inicializar los canales
		canalProductos = new Canal( ID.S_PRODUCTOS.toString(), new ArrayList<Mensaje>(), Estado.DESABILITADO );
		canalCompras = new Canal( ID.S_COMPRAS.toString(), new ArrayList<Mensaje>(), Estado.DESABILITADO );
	}

	
	public void detener() {
		new EnvioDAO().guardarEnvios( envios );
		manejadorMensajes.detener();
	}

	public void recibirSiguienteMensaje() {
		manejadorMensajes.recibirSingle();
	}
	
	
	public void comenzarCorteConsistente() {
		
		canalProductos.setEstado( Estado.LOGGING );
		canalCompras.setEstado( Estado.LOGGING );
		
		almacenarEstadoCorte();
		
		Mensaje mensaje = new Mensaje();
		mensaje.setIdentificador( ID.S_ENVIOS );
		mensaje.setOper( Operacion.SNAPSHOT );
		
		relojes.tick( RelojVectorial.INDEX_ENVIOS ); // Incrementa el reloj
		mensaje.setTiempos( relojes.toLongArray() );
		manejadorMensajes.enviar( mensaje, "envios.compras" );
		
		relojes.tick( RelojVectorial.INDEX_ENVIOS ); // Incrementa el reloj
		mensaje.setTiempos( relojes.toLongArray() );
		manejadorMensajes.enviar( mensaje, "envios.productos" );
		
		relojes.tick( RelojVectorial.INDEX_ENVIOS ); // Incrementa el reloj
		mensaje.setTiempos( relojes.toLongArray() );
		manejadorMensajes.enviar( mensaje, "envios.pagos" );
		
		setSnapshot( true );
	}
	
	public void procesarMensaje( ID origen, Operacion operacion, String mensaje, long[] tiempos ) {
		 procesarCorte( origen , operacion, mensaje );
		// Incrementa tiempo reloj propio, y ajusta los tiempos de los otros relojes
		 relojes.ajustarTiempos( RelojVectorial.INDEX_ENVIOS, tiempos );
		 
		 if (operacion == Operacion.CALCULAR_ENVIO) {
        	JsonNode node;
			try {
				ObjectMapper mapper = new ObjectMapper();
				node = mapper.readTree( mensaje );
				int compra = node.get("compra").asInt();
				
	        	float costo = calularCostoEnvio( compra );
	        	
	        	/*
	        	ObjectNode node1 = mapper.createObjectNode();
	        	node1.put("conpra", compra );
	        	node1.put("costo", costo );
	        	*/
	        	
	        	Mensaje msj = new Mensaje();
	        	msj.setIdentificador( ID.S_ENVIOS);
	        	msj.setOper( Operacion.CALCULAR_ENVIO );
	        	String respuesta = "{\"compra\":" + compra + ",\"costo\":" + costo + "}";
	        	msj.setMensaje( respuesta );
	        	//msj.setMensaje( node1.toString() );
	        	
	        	relojes.tick( RelojVectorial.INDEX_ENVIOS ); // Incrementa el reloj
	        	msj.setTiempos( relojes.toLongArray() );
	        	manejadorMensajes.enviar( msj, "envios.compras" );
			} catch (IOException e) {
				System.out.println( "[*][ENVIOS]  IOEXception: " + e.getMessage() );
			}
        }
        else if (operacion == Operacion.AGENDAR_ENVIO) {
        	JsonNode node;
			try {
				ObjectMapper mapper = new ObjectMapper();
				node = mapper.readTree( mensaje );
				int compra = node.get("compra").asInt();
	        	
	        	agendarEnvio(compra);
	        	
	        	/*
	        	ObjectNode node1 = mapper.createObjectNode();
	        	node1.put("compra", compra );
	        	*/
	        	
	        	Mensaje msj = new Mensaje();
	        	msj.setIdentificador( ID.S_ENVIOS);
	        	msj.setOper( Operacion.ENVIAR );
	        	msj.setMensaje( "{\"compra\":" + compra + "}" );
	        	//msj.setMensaje( node1.toString() );
	        	
	        	relojes.tick( RelojVectorial.INDEX_ENVIOS );
	        	msj.setTiempos( relojes.toLongArray() );
	        	manejadorMensajes.enviar( msj, "envios.productos" );
			} catch (IOException e) {
				System.out.println( "[*][ENVIOS]  IOEXception: " + e.getMessage() );
			}
        }
        else if (operacion == Operacion.SNAPSHOT) {
   		 	if (origen == ID.S_PRODUCTOS) {
   		 		if (canalProductos.getEstado() == Estado.DESABILITADO) {
   		 			
   		 			canalProductos.setEstado( Estado.COMPLETO );
   		 			canalCompras.setEstado( Estado.LOGGING );
   				
   		 			almacenarEstadoCorte();
   		
   		 			Mensaje msj = new Mensaje();
   		 			msj.setIdentificador( ID.S_ENVIOS );
   		 			msj.setOper( Operacion.SNAPSHOT );
	   		 			
	   		 		relojes.tick( RelojVectorial.INDEX_ENVIOS ); // Incrementa el reloj
	   				msj.setTiempos( relojes.toLongArray() );
   		 			manejadorMensajes.enviar( msj, "envios.compras" );
	   		 			
	   		 		relojes.tick( RelojVectorial.INDEX_ENVIOS ); // Incrementa el reloj
	   				msj.setTiempos( relojes.toLongArray() );
		 			manejadorMensajes.enviar( msj, "envios.productos" );
   		 		}
   		 		else if (canalProductos.getEstado() == Estado.LOGGING) {
	   		 		canalProductos.setEstado( Estado.COMPLETO );
   		 			cerrarCorte();
   		 		}
   		 	}
	   		 else if (origen == ID.S_COMPRAS) {
			 		if (canalCompras.getEstado() == Estado.DESABILITADO) {
			 			
		   		 		canalCompras.setEstado( Estado.COMPLETO );
			 			canalProductos.setEstado( Estado.LOGGING );
					
			 			almacenarEstadoCorte();
			
			 			Mensaje msj = new Mensaje();
			 			msj.setIdentificador( ID.S_ENVIOS);
			 			msj.setOper( Operacion.SNAPSHOT );
			 			
			 			relojes.tick( RelojVectorial.INDEX_ENVIOS ); // Incrementa el reloj
		   				msj.setTiempos( relojes.toLongArray() );
			 			manejadorMensajes.enviar( msj, "envios.productos" );
			 		
			 			relojes.tick( RelojVectorial.INDEX_ENVIOS ); // Incrementa el reloj
		   				msj.setTiempos( relojes.toLongArray() );
	   		 			manejadorMensajes.enviar( msj, "envios.compras" );
		   		 		
			 		}
			 		else if (canalCompras.getEstado() == Estado.LOGGING) {
			 			canalCompras.setEstado( Estado.COMPLETO );
			 			cerrarCorte();
			 		}
			 	}
        }
		// Guarda el reloj actual en archivo.
		relojDAO.guardarRelojes( relojes.toLongArray() );
	}

	public List<Envio> listaEnvios() {
		return envios;
	}

	public float calularCostoEnvio( int compra ) {
		int min = 300;
		int max = 800;
		Random sr = new Random( System.currentTimeMillis() );
		float costo = sr.nextInt( max-min+1 ) + min ;
		
		Envio envio = new Envio();
		envio.setNumero( envios.size() + 1 );
		envio.setCompra( compra );
		envio.setEstado( "CALCULADO");
		envio.setCosto(costo);
		
		envios.add( envio );
		return costo;
	}
	
	public void agendarEnvio( int compra ) {
		for (Envio env: envios) {
			if (env.getCompra() == compra) {
				env.setDespacho( new Date() );
				env.setEstado("DESPACHADO");
				return;
			}
		}
		
	}
	
	/**
	 * Retorna la lista de envios en memoria en formato cadena Json
	 */
	public String listaEnviosString() {
		return enviosToString( envios ); 
	}

	/**
	 * Retorna la lista de envios persistida en formato cadena Json
	 */

	public String listaEnviosPerisitidosString() {
		List<Envio> result = new EnvioDAO().obtenerTodosEnvios();
		return enviosToString( result );
	}
	
	/**
	 * Tranforma una lista de compras en una cadena con formato json.
	 * @param listCompras
	 * @return
	 */
	private String enviosToString( List<Envio> listEnvios) {
		ObjectMapper mapper = new ObjectMapper();
		ArrayNode arrayNode = mapper.createArrayNode();
		if (listEnvios != null)
			for (Envio envio: listEnvios) {
				ObjectNode node = mapper.createObjectNode();
				node.put("numero", envio.getCompra() );
				node.put("compra", envio.getCompra() );
				node.put("estado", envio.getEstado() );
				arrayNode.add( node );
			}
		return arrayNode.toString();
	}

	
	public void almacenarEstado() {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		EnvioDAO edao = new EnvioDAO();
		System.out.println("[*][ENVIOS] almacenando estado de envios...  " + envios.size() + " registros, " + dateFormat.format( new Date() ) );
		if (envios.size() > 0)
			edao.guardarEnvios(envios);
	}
	
	private void procesarCorte( ID origen, Operacion operacion, String mensaje ) {
		if (operacion == Operacion.SNAPSHOT)
			return;
		if ((canalCompras.getNombre().compareToIgnoreCase( origen.toString())  == 0) && canalCompras.getEstado() == Estado.LOGGING) {
			Mensaje m = new Mensaje();
			m.setIdentificador( origen );
			m.setOper( operacion );
			m.setMensaje(mensaje);
			canalCompras.agregarMensaje( m );
		}
		else if ((canalProductos.getNombre().compareToIgnoreCase( origen.toString()) == 0) && canalProductos.getEstado() == Estado.LOGGING) {
			Mensaje m = new Mensaje();
			m.setIdentificador( origen );
			m.setOper( operacion );
			m.setMensaje(mensaje);
			canalProductos.agregarMensaje( m );
		}
	}
	
	private void cerrarCorte() {
		if (canalCompras.getEstado() == Estado.COMPLETO && canalProductos.getEstado() == Estado.COMPLETO) {
			
			almacenarCanalesCorte();
			
			canalCompras.setEstado( Estado.DESABILITADO );
			canalCompras.setMensajes( new ArrayList<Mensaje>() );
			
			canalProductos.setEstado( Estado.DESABILITADO );
			canalProductos.setMensajes( new ArrayList<Mensaje>() );
		}
	}
	
	private void almacenarEstadoCorte() {
		enviosCorte = new ArrayList<Envio>( envios );
	}
	
	private void almacenarCanalesCorte() {
		ObjectMapper mapper = new ObjectMapper();
	
		// Nodo raiz
		ObjectNode nodeCorte = mapper.createObjectNode();
		
		// Nodo estado; array de envios
		ArrayNode nodeEnvios = mapper.createArrayNode();
		for (Envio envio: enviosCorte) {
			ObjectNode nodeEnvio = mapper.createObjectNode();
			nodeEnvio.put("numero", envio.getNumero() );
			nodeEnvio.put("compra", envio.getCompra() );
			nodeEnvio.put("costo", envio.getCosto() );
			nodeEnvio.put("estado", envio.getEstado() );
			nodeEnvio.put("despacho", (envio.getDespacho() == null ? "": envio.getDespacho().toString())  );
			nodeEnvio.put("entrega", (envio.getEntrega() == null ? "": envio.getEntrega().toString()) );
			nodeEnvios.add( nodeEnvio );
		}
		nodeCorte.put("estado", nodeEnvios.toString() );
		
		// Nodo canales: array de mensajes
		ArrayNode nodeCanales = mapper.createArrayNode();
		for (Mensaje m: canalCompras.getMensajes()) {
			ObjectNode nodeCanal = mapper.createObjectNode();
			nodeCanal.put("operacion", m.getOper().toString() );
			nodeCanal.put("mensaje", m.getMensaje().toString() );
			nodeCanales.add( nodeCanal );
		}
		for (Mensaje m: canalProductos.getMensajes()) {
			ObjectNode nodeCanal = mapper.createObjectNode();
			nodeCanal.put("operacion", m.getOper().toString() );
			nodeCanal.put("mensaje", m.getMensaje().toString() );
			nodeCanales.add( nodeCanal );
		}		
		nodeCorte.put( "canales", nodeCanales.toString() );
		
		try {
			String archivo = System.getProperty("user.dir") + "/data/" + "corte" + relojes.toString() + ".json";
			File file = new File( archivo );
			file.createNewFile();
			FileOutputStream output = new FileOutputStream( archivo );
			JsonGenerator g = mapper.getFactory().createGenerator( output );
			mapper.writeValue( g, nodeCorte );
		} catch (IOException e) {
			System.out.println( "[*][COMPRAS] IOException guardando Corte: " + e.getMessage() );
		}
		
	}
	
	public boolean isSnapshot() {
		return snapshot;
	}

	public void setSnapshot(boolean snapshot) {
		this.snapshot = snapshot;
	}
}
