/**
 * ManejadorMensajes.java
 */

package sd.pagos.mensajeria;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import com.rabbitmq.client.GetResponse;

import sd.pagos.negocio.ServidorPagos;
import servidor.Mensaje;

public class ManejadorMensajesPago {
	
	private Channel channel;
	private static final String EXCHANGE_NAME = "SD_COMPRASONLINE";
	private static final String HOST = "localhost";
	private static final String QUEUE_NAME = "Q_PAGOS";
	
	private String consumerTag;
	
	private ServidorPagos servidorPagos;
	
	public ManejadorMensajesPago( ServidorPagos sp ) {
		this.servidorPagos = sp;
	}

	/**
	 * Conecta a un exchange y declara una cola para pagos
	 * @return
	 */
	public int conectar() {
		ConnectionFactory factory = new ConnectionFactory();
       factory.setHost( HOST );
        try {
        	Connection connection = factory.newConnection();
        	channel = connection.createChannel();
        	channel.exchangeDeclare( EXCHANGE_NAME, "topic",true );
        	channel.queueDeclare( QUEUE_NAME, true, false, false, null );
        	return 1;
        } catch (IOException | TimeoutException e) {
			System.out.println( "[*][PAGOS] ERROR conectando  a exchange " + EXCHANGE_NAME + " : " + e.getMessage() );
			return -1;
		}
	}
	
	/**
	 * Envia un mensaje msj hacia una cola con topico rountingKey
	 * @param msj
	 * @param routingKey
	 * @return
	 */
	public int enviar( Mensaje msj, String routingKey ) {
		ObjectMapper mapper= new ObjectMapper();
		String mensaje;
		
		try {
			mensaje = mapper.writeValueAsString( msj );
		} catch (JsonProcessingException e1) {
			System.out.println("[*][PAGOS] Mensajeria JSonPRocessingException...." );
			return -1;
		}
	    try {
	    	System.out.println("[*][PAGOS] --> Enviando... '" + mensaje + "' en exchange:" + EXCHANGE_NAME + ":" + routingKey );
			channel.basicPublish( EXCHANGE_NAME, routingKey, null, mensaje.getBytes("UTF-8"));
			return 1;
		} catch (IOException e) {
			System.out.println("[*][PAGOS] IOException enviar : " + e.getMessage() );
			return -1;
		}
	}
	
	/**
	 * Recive mensajes desde una cola con topico *.pagos
	 */
	public void recibir() {
		try {
			channel.queueBind( QUEUE_NAME, EXCHANGE_NAME, "*.pagos" );
		} catch (IOException e) {
			System.out.println("[*][PAGOS] IOException cola " + QUEUE_NAME + ", recibir: " + e.getMessage() );
		}
		
		/**
		 * funcion callback que procesa cada mensaje recibido en la cola
		 */
    	DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), "UTF-8");
            System.out.println("[*][PAGOS] <-- Recibiendo: " +  message );
            ObjectMapper mapper = new ObjectMapper();
            Mensaje mensaje = mapper.readValue( message , Mensaje.class );
           
            servidorPagos.procesarMensaje( mensaje.getIdentificador(), mensaje.getOper(), mensaje.getMensaje(), mensaje.getTiempos() );
        };
        
        try {
        	consumerTag = channel.basicConsume( QUEUE_NAME, true, deliverCallback, consumerTag -> { });
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
	
	/**
	 * Recibe un mensaje desde la cola de Pagos.
	 */
	public void recibirSingle() {
		GetResponse chResponse;
		try {
			channel.queueBind( QUEUE_NAME, EXCHANGE_NAME, "*.pagos" );
		} catch (IOException e) {
			System.out.println("[*][PAGOS] IOException Bind, queue: " + QUEUE_NAME + ", recibir: " + e.getMessage() );
			e.printStackTrace();
		}
        
        try {
        	chResponse = channel.basicGet( QUEUE_NAME, true );
        	if (chResponse != null) {
        		String message = new String( chResponse.getBody(), "UTF-8");
        		ObjectMapper mapper = new ObjectMapper();
                Mensaje mensaje = mapper.readValue( message , Mensaje.class );
                System.out.println("[*][PAGOS] <-- Recibiendo single mensaje: " +  message );
                
                servidorPagos.procesarMensaje( mensaje.getIdentificador(),mensaje.getOper(), mensaje.getMensaje(), mensaje.getTiempos() );
        	}
		} catch (IOException e) {
			System.out.println("[*][COMPRAS] IOException recibirSingle: " + e.getMessage() );
		}
	}

	/**
	 * Detiene el proceso que recibe mensajes de la cola
	 */
	public void detener() {
		try {
			channel.basicCancel(consumerTag);
		} catch (IOException e) {
			System.out.println("[*][PAGOS] IOException : " + e.getMessage() );
		}
	}
	
	/**
	 * Cierra el exchange
	 */
	public void cerrar() {
		try {
			System.out.println("[*][PAGOS] Mensajeria cerrando exchange... " );
			channel.close();
		} catch (IOException | TimeoutException e) {
			System.out.println( "[*][PAGOS] Mensajeria Exception: " + e.getMessage() );
		}
	}
	
}
